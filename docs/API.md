#NuBot HTTP API

NuBot allows external interaction over HTTP. The feature is still experimental and poorly documented.
Launch the bot server using the -GUI flag.  Api base is http://localhost:<port> (default 8889)


Success response: 
```
{
 "success":true ,
 "error":""
}
```

Error response example: 
```
{
 "success":false ,
 "error":"err description "
}
```

## Entry points 

### /config , POST

### /partialconfig , POST

### /config , GET

### /configreset , POST

### /configfile , POST

### /opstatus , POST

### /startstop , POST

### /stopserver , POST

### /logdump , POST

### /balances , GET

### /orders , GET

