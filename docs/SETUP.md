# Setting up NuBot
A tutorial for operators of NuBot.

##Disclaimer . Use NuBot at your own risk

*PLEASE BE AWARE THAT AUTOMATED TRADING WITH NUBOT MAY BE RISKY, ADDICTIVE, UNETHICAL OR ILLEGAL. ITS MISUSE MAY ALSO CAUSE FINANCIAL LOSS. NONE OF THE AUTHORS, CONTRIBUTORS, ADMINISTRATORS, OR ANYONE ELSE CONNECTED WITH NUBITS, IN ANY WAY WHATSOEVER, CAN BE RESPONSIBLE FOR THE USE YOU MAKE OF NUBOT*

By using NuBot you declare to have accepted the afore-mentioned risks. See the DISCLAIMER.md for detailed terms.

##Requirements for a correct functioning 

* The operator must provide the bot with access to market exchanges where NuBits are traded
* The operator must avoid any kind of interaction with the exchange while the automated bot is operating. Do not try to trade, do not try to deposit/withdraw funds. 
* The operator must run a single instance of NuBot for each market, without overlapping currencies. Its good practice to create a brand new exchange account and keypair for each market he works with.
* To broadcast *liquidityinfo*, the operator should either possess an unlocked NuBit client which controls the custodial grant address or connect to a liquidity pool.
* The operator must apply for receiving a valid authorization token that will grant authorization to full services. Apply for auth key by filling [this form](http://goo.gl/forms/nPjiDdsIUb)

### Setup overview.
TL; DR  Make sure you run both the NuBits client and NuBot  on a machine connected to the internet 24/7, configure NuBits client to accept RPC commands, configure NuBot, launch it and monitor it.

Detailed tutorial below : 

###0) Setup the machine

NuBot can run on a computer, on a raspberry pi, or a remote VPS, as long as it has a permanent connection. The choice is up to you. It only requires a machine with 24/7 connection and recent Java Runtime Environment to be installed.
NuBot comes as a cross platform executable jar file to run from command line or by double clicking the server launcher executable jar. 

Type `java -version` in your terminal to make sure you have JRE >= 1.8 installed on your machine, otherwise download Java JRE:1.8 [from oracle's download page](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)

Or install via apt-get 
```
$ sudo add-apt-repository ppa:webupd8team/java
$ sudo apt-get update
$ sudo apt-get install oracle-java8-installer
```

Starting from v0.3.0,  NuBot can be run as a service and controlled via web interface. That implies you can run the service on a remote machine with permanent connection and control it via browser from another machine.

#### For Raspberry pi users :

**A RaspberryPi 2 seems to be the minimum requirement for running NuBot on the RaspberryPi platform especially if running nud on the same machine is intended**

There are no trusted apt repositories for Java 8 installation packets available. You need to download and install manually  : 

[Download page for Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html), look for ARM.
**Remark**: wget will not work with the direct link, because an authentication token is missing; start the download on a regular browser and use the download link that include the authentication token.

```
$ wget http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-arm32-vfp-hflt.tar.gz?AuthParam=<obtain-auth-from-browser>  
$ sudo tar zxvf jdk-8-linux-arm-vfp-hflt.tar.gz -C /opt
$ sudo update-alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_60/bin/javac 1
$ sudo update-alternatives --install /usr/bin/java java /opt/jdk1.8.0_60/bin/java 1
$ sudo update-alternatives --config javac #confirm with enter
$ sudo update-alternatives --config java #confirm with enter
```


###1) Prepare the NuBits client 

**If you plan to operate NuBot with a [liquidity pool](https://docs.nubits.com/liquidity-pools/), the server will take care of submitting *liquidityinfo*. Skip this section of the tutorial and go next step.**
To function correctly, NuBot needs to communicate with the NuBit client of the custodian. The bot needs to broadcast liquidity notification to NuNet. By doing so, it allow shareholders to have real-time information about active orders in the network across custodians, and react to it.  It does so by interacting via the *NuBits client of the custodian*. 

Install the latest version of the NuBit client [from the official website](https://nubits.com/download) and let the client sync with the network (downloading the whole blockchain from scratch can take a while). 

*NOTE: Is it also possible to test NuBot without being a custodian and setting the `"submit-liquidity":false` in the configuration file. In this case you can ignore this section.*

To allow the bot to communicate with the Nu client via RPC over HTTP, open your *nu.conf* and add make sure you have it properly configured: [Read this bitcoin tutorial to locate the data directory](https://en.bitcoin.it/wiki/Data_directory#Default_Location).  

*nu.conf*  sample configuration
```
server=1
rpcuser=<choose-a-username>
rpcpassword=<choose-a-password>
port=9090
rpcport=9091
rpcallowip=<ip_of_machine_running_the_bot>*
```

* NOTE: if you plan using NuBot and NuBits on the same machine, the rpcallowip parameter can be omitted. If you plan to run the bot on a different machine, you must authorise remote calls to your Nu client.

Restart the NuBits client to make changes effective. Make sure that the NuBit client controls a public address which received a custodial grant.  If the NuBits wallet is encrypted with a passphrase, make sure to unlock it, otherwise it won't accept RPC calls to *liquidityinfo*. Unlocking the wallet *for minting only* is not enough.  

The two Nu wallets are locked/unlocked separately. To unlock the NuBits wallets, make sure to execute the command in the NuBits context; type:

```
nud -unit=B walletpassphrase <your passphrase> 9999999999 true
```

The command above will unlock the NBT wallet (for minting only) for 9999999999 seconds, ~ 300 years. That should be enough time for your bot to keep the peg!

###2) Prepare NuBot

*Download and unzip the latest stable build from the [download page](https://bitbucket.org/JordanLeePeershares/nubottrading/downloads).*

Apply for a credential token : since the bot uses external services that require authentication, you'll need to apply for a valid auth-token file that is not distributed with the bundle for obvious reasons.
If you are applying as operator, use [this form](http://goo.gl/forms/nPjiDdsIUb) to request a valid key .

In the spirit of OSS, you are also allowed to bypass the official credentials and use your own. 
 - First edit the source of [CredentialManager.java](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/src/main/java/com/nubits/nubot/global/) and add your own credentials (see comments in source)
 - Build the project using NuBotDist gradle task
 - Run the bot from CLI using the *-skipCredentials* flag
 

#### Launch NuBot as a service (web GUI)

If you want to launch the bot via CLI, you can skip this section.

Double click the provided *NuBot-Server-launcher.jar* executable or add the *-server* flag to command line arguments. A new webservice will be started at your localhost on port specified via configuration file or CLI parameter (default 8889).
You can navigate to http://localhost:port from the local machine or properly configure firewall/port-forwarding to access the web-interface from a remote machine or smartphone.

On startup you will be given the option of selecting an existing *.json* configuration file or start from scratch. 
If you chose the latter, a new empty configuration file will be created in *config/nubot-config.json* and you can setup the bot using the web-interface. Remember to save your changes or they will not be effective.
The next chapter of this tutorial provides a detailed explanation of each launch parameter you can change in the web interface. 

In the web-interface you are presented with a simple dashboard that lets you control the bot (start/stop, change configuration, access documentation).  

#### Launch configuration tutorial

The bot reads configuration options from one *.json* file (also when configured via web-interface although it is transparent to the user).  Below you can find a list of all the available options parameter along a brief description. 
*We suggest omitting most most non-essential parameters : when possible they will be automatically filled with their default values.*
Please make sure you fully understand in which way changing a setting will affect the bot before doing anything that can lead to losses. 

**If you want to quickly run NuBot because you know what you are doing, you can skip this section and take a look at the sample configuration files distributed under *config/* directory**

In this tutorial we will use the following grouping : 

 * market options: exchange keys and market-related settings ;  
 * misc options: miscellaneous configuration parameters ;
 * liquidity-info options: define nubits client communication ;
 * price-tracking options: for non-USD pairs, define price feeds ;
 * orderbook options: define how liquidity is distributed across orders and tiers;
 * liquidity pool options: configure liquidity pool client;


Refer to the file `sample-config.json` or its essential form `sample-config-mini.json` provided with the bot. You can edit the provided sample file or create a new configuration file.

Sample `sample-config.json`:
```json
{
  "exchangename":"poloniex",
  "apikey": "xxx",
  "apisecret": "xxx",
  "txfee": 0.2,
  "pair":"nbt_btc",

  "dualside": true,
  "multipleoperators":false,
  "executeorders":true,
  "vebosity":"normal",
  "bypassStreaming": false,
  "streamingServer" : "stream.tradingbot.nu:5556",
  "gitter":true,
  "mailnotifications":"severe",
  "mailrecipient":"xxx@xxx.xxx",
  "emergencytimeout":60,
  "keepproceeds":0,
  "priceincrement": 0.1,
  "webport": 8889,
  "submitliquidity":true,
  "nubitaddress": "xxx",
  "nudip": "127.0.0.1",
  "nudport": 9091,
  "rpcpass": "xxx",
  "rpcuser": "xxx",

  "wallchangeThreshold": 0.1,
  "mainfeed":"blockchain",
  "backupfeeds": ["coinbase", "btce"],
  
  "bookDisabletier2": false,
  
  "bookSellWall": 1000.0,
  "bookSellOffset": 0.002,
  "bookSellInterval": 0.015,
  "bookSellMaxVolumeCumulative" : 0,

  "bookSellType": "exp",
  "bookSellSteepness": "low",

  "bookBuywall": 1000.0,
  "bookBuyOffset": 0.048,
  "bookBuyInterval": 0.015,
  "bookBuyMaxVolumeCumulative" : 0,  
  "bookBuyType": "log",
  "bookBuySteepness": "low",

  "poolMode" : false,
  "poolURI" : "",
  "poolPayoutAddress" : "xxx",
  "poolSubmitInterval" : 60
}

```
---
#### Market options 

Parameters : 

| Parameter      |  Default value  |  Description  |   Admitted values  | 
| ------------- |:-------------:| -------------:| -------------:| 
| exchangename     | / | Name of the exchange where the bots operates |  **see list of accepted exchange names |
| apikey      |  / | Operator's public key to access the exchange . *this param is optional for ccex*     |  String |
| apisecret |  / |  Operator's secret key to access the exchange    | String |
| txfee    | 0.2  |  If transaction fee not available from the exchange via api, this value will be used  |  double. Expressed in absolute percentage. 10 = 10% , 0.5 = 0.5% |
| pair | "nbt_btc"  |  The currency pair where the bot operates     |   valid currency pair for the specified  eg. "nbt_usd" |

See EXCHANGES.md for an updated list of valid exchange names.

---

#### Misc options

Parameters : 

| Parameter      |  Default value  |  Description  |   Admitted values  | 
| ------------- |:-------------:| -------------:| -------------:| 
| dualside | true |  If set to true, the bot will behave as a dual side custodian, if false as a sell side custodian.     | true,false |
| multipleoperators    | false |  if set to true (and streaming bypassed or offline) bot will sync with remote NPT and reset orders often.  | boolean |
| executeorders    | true |  if set to false the bot will print a warning instead of executing orders  | boolean |
| verbosity    | "normal" |  set logging verbosity level | String ("high", "normal", "low") |
| gitter(*)    | true |  if set to false will disable gitter notifications | boolean |
| mailnotifications    | "severe" |  set notification level: none, all: including non-critical, severe: only critical | String ("none", "all", "severe") |
| mailrecipient | / |  the email to which emergency email are sent  |  String  |
| emergencytimeout    | 60 | max amount of minutes of consecutive failure. After those minute elapse, emergency procedure starts |  int (minutes) |
| keepproceeds    | 0 |  Specific setting for KTm's proposal. Will keep the specified proceeds from sales apart instead of putting 100% of balance on buy . |  double. Expressed in absolute percentage. 10 = 10% , 0.5 = 0.5%|
| priceincrement    | 0.0003 |  if working in sell-side mode, this value (considered USD) will be added to the sell price | double , price increment in expressed USD |
| webport    | 8889 |  the port for http server and GUI. Can be overridden via CLI launch argument *-port*   | int |

(*) Join these two gitter rooms to read public notifications : [Normal Notification](https://gitter.im/NuNetwork/nubot-notifications?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
 and [Severe Notifications](https://gitter.im/NuNetwork/nubot-important-notifications?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
---

#### Liquidity-info options

Necessary only if you need your bot to submit liquidityinfo. Will be ignored if bot is set to operate in pool mode (see below).
Parameters : 

| Parameter      |  Default value  |  Description  |   Admitted values  | 
| ------------- |:-------------:| -------------:| -------------:| 
| submitliquidity    | false  |  if set to false, the bot will not try to submit liquidity info. If set to false, it will also allow the operator to omit the declaration of *nubitaddress* ,  *nudport* , *rpcuser* and *rpcpass*  | boolean |
| nubitaddress | / | The public address where the custodial grant has been received    |   valid custodian NBT addresses (String) |
| nudip    | "127.0.0.1"  |  The IP address of the machine that hosts the Nu Client |  IP address (String) |
| nudport | / |The RPC port of the Nu daemon |   1024...65535 |
| rpcpass | / |  The RPC password of the Nu daemon    |  String |
| rpcuser | / |  The RPC username  of the Nu daemon    |    String |


---

#### Price-tracking options 
When running the bot against a currency pair different from NBT/USD, the bot can be configured with additional parameters. 
**The options below are not required and we highly suggest to omit them. NuBot will use the default options automatically.**

For testing purposes the operator can overwrite the default price feeds that the bot must use to track the price of a currency (both fiat and crypto) different from USD.   
The price feed will track the live rate between the *secondary peg* currency and USD. 

Parameters : 

| Parameter      |  Default value  |  Description  |   Admitted values  | 
| ------------- |:-------------:| -------------:| -------------:|
| bypassStreaming    | false |  if set to true will autonomously read peg price directly from pricefeeds. See [STREAMER.MD](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/docs/STREAMER.md)  | boolean |
| streamingServer    | "stream.tradingbot.nu:5556" |  hostname:port for Streaming service.  See [STREAMER.MD](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/docs/STREAMER.md)   | String host:ip |
| wallchangeThreshold | 0.15 | how much the price needs to change to trigger a wall-shift.    | double. Expressed in absolute percentage. 10 = 10% , 0.5 = 0.5%   |
| mainfeed  | depends on currency. See [currencies.csv](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/res/currencies.csv) | the name of the main price feed that has priority over the others | See [FEEDS.md](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/docs/FEEDS.md) |
| backupfeeds  | depends on currency. See [currencies.csv](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/currencies.csv) |  a json array containing an arbitrary number (>2) of backup price feed names    |   See [FEEDS.md](https://bitbucket.org/JordanLeePeershares/nubottrading/src/master/docs/FEEDS.md) |

Note: Streaming server uses default recommended values for spread and treshold. even while reading prices from streaming server, the local spread setting will be enforced if different from default. 

#### Orderbook options
---
Parameters : The operator can control the orderbook that nubot places by controlling parameters below . 
If you launch NuBot with the GUI, you will be able to change parameters and see a real time preview of how changing parameters affects the shape of your orderbook.
**The options below are not required and we highly suggest to omit them. NuBot will use the default options automatically.**

| Parameter      |  Default value  |  Description  |   Admitted values  | 
| ------------- |:-------------:| -------------:| -------------:| 
| bookDisabletier2 | false | if set to true, will only place the tier1 walls  |  boolean |
| bookSellwall | 500 | maximum volume to put on sell walls at best price (tier1) |  double , expressed in NBT ; 0 allowed |
| bookBuywall | 500 | maximum volume to put on buy walls at best price (tier1) |  double , expressed in NBT ; 0 allowed |
| bookSellOffset | depends on currency (0;0.01;0.025) | offset from target price  |  double , expressed USD.  |
| bookBuyOffset | depends on currency (0;0.01;0.025) |  offset from target price  |  double , expressed USD.  |
| bookSellInterval | 0.008 |  interval price between two sell orders  |  double , expressed USD. (0.000001,0.3)  |
| bookBuyInterval | 0.008 |  interval price between two buy orders  |  double , expressed USD. (0.000001,0.3) |
| bookSellMaxVolumeCumulative | 0 (unlimited) |  maximum cumulative volume of open sell orders  |  double , expressed USD.   |
| bookBuyMaxVolumeCumulative | 0 (unlimited) |  maximum cumulative volume of open buy orders  |  double , expressed USD. |
| bookSellType | exp | shape of the curve  |  String , **lin**ear; **exp**onential; **log**arithmic;  |
| bookBuyType | exp | shape of the curve  |  String , **lin**ear; **exp**onential; **log**arithmic; |
| bookSellSteepness | mid | steepness of the curve  |  String , flat;low;mid;high |
| bookBuySteepness | mid | steepness of the curve  |  String , flat;low;mid;high  |

![parametricbook](https://bytebucket.org/JordanLeePeershares/nubottrading/raw/b7dfd492e808e0a92081dcfcfc749ec3022e12f7/res/readme-assets/parametric-book.png "Parametrized orderbook")

For efficient order handling, the bot will limit the number of orders to 12 per side. 
Moreover, to optimize tier2 liquidity, any amounts left in balance will be placed on additional order at the borders of the orderbook defined by the model.  

---
#### Liquidity Pool options

NuBot can be configured to participate to [liquidity pools](https://nubits.com/liquidity-pools) using the following options.
Parameters :

| Parameter      |  Default value  |  Description  |   Admitted values  |
| ------------- |:-------------:| -------------:| -------------:|
| poolModeActive     | false | if set to true will make NuBot behave as a client of liquidity pool |  boolean |
| poolURI     | / | URI of the pool | http://hostname:port [official list](https://nubits.com/liquidity-pools) |
| poolPayoutAddress     | / | NBT address for receiving the payout |  String |
| poolSubmitInterval     | 50 | Interval expressed in seconds between submissions to liquidity pool server |  int  |


---
 
###3) Run NuBot via command line

Open a terminal, navigate to the folder of NuBot and execute the jar, specifying the path to the *.json* file(s) you want to use as configuration.

Example : 

```
$ cd /path/to/NuBot
$ java -jar NuBot.jar -cfg=<path/to/config.json> [-server] [-skipCredentials] [-port=<port>] [-noBrowser]
```

The flags in brackets are optional and false by default.
You can also use nohup in *nix system to redirect the output, and run it in background with the `&` char. For example you could run nubot in background using :

```
$ nohup java -jar NuBot.jar sample-config.json >/dev/null 2>&1 &
```

The -port flag will be considered only when the *-server* flag is active, and it will override the value declared in configuration file.

The [optional] flag *-server* will spin up the UI and automatically launch the browser, when available. 

The [optional] flag *-noBrowser* will skip opening the browser in server mode.

The *-skipCredentials* flag will force NuBot to use user-defined credentials for external services, as explained in section 2 (Prepare NuBot) of this document.

The bot will start and write output in the */logs* folder. 

To terminate the bot, exit the process with "Ctrl+C" : the bot will clear out *liquidityinfo* and cancel open orders.

###3) Running multiple instances of NuBot

Provided you've got enough computational resources, you can run multiple instances of NuBot on the same machine. If you run them in server mode, then you must set a different webport for each new instance.
We also suggest to have a separate API pair and exchange account for each instance to ensure a correct functioning.