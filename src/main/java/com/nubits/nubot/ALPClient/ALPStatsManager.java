/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.ALPClient;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.models.*;
import com.nubits.nubot.utils.TimeUtils;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.util.Date;

public class ALPStatsManager {
    private static final Logger LOG = LoggerFactory.getLogger(ALPStatsManager.class.getName());

    private ALPService alpService;

    private ALPStats latestStats;
    private Date lastFetchedDate;

    public ALPStatsManager(ALPService alpService) {
        this.alpService = alpService;
    }

    public void fetchStats() {

        ApiResponse serverStatusResponse = alpService.getServerStatus();

        JSONParser parser = new JSONParser();

        boolean soFarSoGood = true;


        //init vars
        boolean connected = false;
        String serverAddress = "";
        Amount roundReward = null;
        Amount totalReward = null;
        String poolUserId = "";

        Date nextPayoutDate = null;
        Date lastPayoutDate = null;
        double askPayoutRank1 = 0;
        double askPayoutRank2 = 0;
        double bidPayoutRank1 = 0;
        double bidPayoutRank2 = 0;
        String serverUpTime = "";

        Date serverTime;

        //Get server status
        if (serverStatusResponse.isPositive()) {
            String serverStatsString = serverStatusResponse.getResponseObject().toString();
            JSONObject responseServerJSON = null;
            try {
                responseServerJSON = (JSONObject) parser.parse(serverStatsString);
                    /*sample :
                        {"message": {"meta": {"last-credit-time": 1453066114, "next_payout_time": 1453115839, "number-of-orders": 0, "number-of-users": 0, "number-of-users-active": 0}, "prices": {"btc": 380.1, "ppc": 0.393, "usd": 1.0}, "rewards": {"bter": {"btc": {"ask": {"rank_1": 0.0, "rank_2": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0}}}, "test_exchange": {"btc": {"ask": {"rank_1": 0.0, "rank_2": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0}}, "ppc": {"ask": {"rank_1": 0.0, "rank_2": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0}}}, "test_exchange_2": {"usd": {"ask": {"rank_1": 0.0, "rank_2": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0}}}}, "totals": {"bter": {"btc": {"ask": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "total": 0.0}}, "test_exchange": {"btc": {"ask": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "total": 0.0}, "ppc": {"ask": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "total": 0.0}}, "test_exchange_2": {"usd": {"ask": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "bid": {"rank_1": 0.0, "rank_2": 0.0, "total": 0.0}, "total": 0.0}}}}, "server_time": 1453284400, "server_up_time": 379, "status": true}
                    */
                JSONObject serverMessageJSON = (JSONObject) (responseServerJSON.get("message"));
                JSONObject metaJSON = (JSONObject) (serverMessageJSON.get("meta"));
                JSONObject rewardsJSON = (JSONObject) (serverMessageJSON.get("rewards"));

                //Fill objects
                nextPayoutDate = new Date((Long) metaJSON.get("next-payout-time") * 1000);
                lastPayoutDate = new Date((Long) metaJSON.get("last-credit-time") * 1000);
                String currencyStr = null;
                try {
                    currencyStr = Currency.getCurrencyToTrack(Global.options.getPair()).getCode().toLowerCase();
                    JSONObject rewards_exchangeJSON = (JSONObject) (rewardsJSON.get(Global.options.exchangeName.toLowerCase()));
                    JSONObject rewards_exchange_currencyJSON = (JSONObject) (rewards_exchangeJSON.get(currencyStr));
                    JSONObject rewards_exchange_currency_ask_JSON = (JSONObject) (rewards_exchange_currencyJSON.get("ask"));
                    JSONObject rewards_exchange_currency_bid_JSON = (JSONObject) (rewards_exchange_currencyJSON.get("bid"));


                    askPayoutRank1 = Utils.getDouble(rewards_exchange_currency_ask_JSON.get("rank_1"));
                    askPayoutRank2 = Utils.getDouble(rewards_exchange_currency_ask_JSON.get("rank_2"));
                    bidPayoutRank1 = Utils.getDouble(rewards_exchange_currency_bid_JSON.get("rank_1"));
                    bidPayoutRank2 = Utils.getDouble(rewards_exchange_currency_bid_JSON.get("rank_2"));

                    long secondsUptime = (long) responseServerJSON.get("server_up_time");
                    serverUpTime = TimeUtils.formatDuration(secondsUptime);

                } catch (CurrencyToTrackNotAvailableException e) {
                    LOG.error(e.getMessage());
                    soFarSoGood = false;
                }

            } catch (ParseException e) {
                LOG.error(e.getMessage());
                soFarSoGood = false;

            }
            if (soFarSoGood) {
                ApiResponse userStatsResponse = alpService.getUserStats(Global.options.apiKey);
                if (userStatsResponse.isPositive()) {
                    String userStatsString = userStatsResponse.getResponseObject().toString();
                    JSONObject responseUserJSON = null;
                    try {
                        responseUserJSON = (JSONObject) parser.parse(userStatsString);
                        /*sample :
                            {"server_time": 1453285272, "message": {"total_reward": 0.0, "current_reward": 0.0, "history": []}, "success": true}
                        */
                        JSONObject userMessageJSON = (JSONObject) (responseUserJSON.get("message"));

                        //Fill objects
                        connected = true;
                        serverAddress = alpService.getBase();
                        roundReward = new Amount(Utils.getDouble(userMessageJSON.get("current_reward")), CurrencyList.NBT);
                        totalReward = new Amount(Utils.getDouble(userMessageJSON.get("total_reward")), CurrencyList.NBT);
                        poolUserId = Global.options.getApiKey();

                    } catch (ParseException e) {
                        LOG.error(e.getMessage());
                        soFarSoGood = false;
                    }
                    if (soFarSoGood) {

                        try {
                            latestStats = new ALPStats(connected, serverAddress, roundReward, totalReward,
                                    nextPayoutDate, lastPayoutDate,
                                    askPayoutRank1, askPayoutRank2, bidPayoutRank1, bidPayoutRank2, serverUpTime, poolUserId);
                            lastFetchedDate = new Date();

                        } catch (MalformedURLException e) {
                            LOG.error(e.getMessage());
                            soFarSoGood = false;
                        }

                    } else {
                        LOG.error("Pool : Generic Error fetching user stats");
                        soFarSoGood = false;
                    }
                } else {
                    LOG.error("Pool : Error fetching user stats : " + serverStatusResponse.getError().toString());
                    soFarSoGood = false;
                }
            }

        } else {
            LOG.error("Pool :Error fetching server stats : " + serverStatusResponse.getError().toString());
            soFarSoGood = false;
        }


        if (!soFarSoGood) {
            latestStats = ALPStats.generateEmptyOfflineStats(); //Return an empty stats for offline
            lastFetchedDate = new Date();
        }


    }


    public ALPStats getStats() {

        if (latestStats == null) {
            fetchStats();
        }
        return latestStats;
    }

    public ALPStats fetchAndGetStats() {
        fetchStats();
        return getStats();
    }

}
