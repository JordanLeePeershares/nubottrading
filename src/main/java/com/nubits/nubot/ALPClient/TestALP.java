/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.ALPClient;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.SignedRequest;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.testsmanual.WrapperTestUtils;
import com.nubits.nubot.trading.TradeUtils;
import com.nubits.nubot.utils.InitTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.rmi.runtime.Log;

public class TestALP {
    NuBotOptions opt;
    String unit;

    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TestALP.class.getName());

    private static final String TEST_OPTIONS_PATH = "config/myconfig/latest/bter.json";


    public static void main(String[] args) {
        InitTests.setLoggingFilename(TestALP.class.getSimpleName());
        InitTests.loadConfig(TEST_OPTIONS_PATH);

        TestALP test = new TestALP();
        test.init(Global.options); //Pass an empty string to avoid placing the orders, or exchange name
        test.exec(true); //!! Use true to execute ALL methods

    }

    private void exec(boolean execAll) {
//Setup the ALPService object
        ALPService alpService = new ALPService(opt.poolURI);

        if (execAll) {

            //Try to register --------------------------------------------
            tryToRegister(alpService);

            //Generate the signed request with list of orders ------------
            trySubmitSignedRequest(alpService);

            //Try to get user stats
            tryToGetUserStats(alpService);

            //Try to get user orders
            tryToGetUserOrders(alpService);

            //Try to get server status
            tryGetServerStatus(alpService);

            //Try to get exchanges
            tryToGetExchanges(alpService);
        } else {

            //only executes few methods

            //Try to get user stats
            //tryToGetUserStats(alpService);

            //Generate the signed request with list of orders ------------
            trySubmitSignedRequest(alpService);


        }
    }

    private void tryToGetExchanges(ALPService alpService) {
        ApiResponse exchangesResponse = alpService.getExchanges();

        if (exchangesResponse.isPositive()) {
            LOG.info("Positive answer from alpService.getExchanges: " + exchangesResponse.getResponseObject().toString());
        } else {
            LOG.error("Error from alpService.getExchanges: " + exchangesResponse.getError().toString());
        }
    }

    private void tryGetServerStatus(ALPService alpService) {
        ApiResponse serverStatusResponse = alpService.getServerStatus();

        if (serverStatusResponse.isPositive()) {
            LOG.info("Positive answer from alpService.getServerStatus: " + serverStatusResponse.getResponseObject().toString());
        } else {
            LOG.error("Error from alpService.getServerStatus: " + serverStatusResponse.getError().toString());
        }
    }

    private void tryToGetUserOrders(ALPService alpService) {
        ApiResponse userOrdersResponse = alpService.getUserOrders(opt.apiKey);

        if (userOrdersResponse.isPositive()) {
            LOG.info("Positive answer from alpService.getUserOrders: " + userOrdersResponse.getResponseObject().toString());
        } else {
            LOG.error("Error from alpService.getUserOrders: " + userOrdersResponse.getError().toString());
        }
    }

    private void tryToGetUserStats(ALPService alpService) {
        ApiResponse userStatsResponse = alpService.getUserStats(opt.apiKey);

        if (userStatsResponse.isPositive()) {
            LOG.info("Positive answer from alpService.getUserStats: " + userStatsResponse.getResponseObject().toString());
        } else {
            LOG.error("Error from alpService.getUserStats: " + userStatsResponse.getError().toString());
        }
    }

    private void trySubmitSignedRequest(ALPService alpService) {
        SignedRequest openOrdersRequest = null;
        try {
            openOrdersRequest = Global.trade.getOpenOrdersRequest(opt.getPair());
            LOG.debug("req:" + openOrdersRequest.getReqJSON() + " \n sign:" + openOrdersRequest.getSign());
            ApiResponse signedOrderResponse = alpService.submitOpenOrdersRequest(opt.apiKey, openOrdersRequest.getReqJSON(), openOrdersRequest.getSign(), opt.exchangeName, unit);

            if (signedOrderResponse.isPositive()) {
                LOG.info("Positive answer from alpService.submitOpenOrdersRequest: " + signedOrderResponse.getResponseObject().toString());
            } else {
                LOG.error("Error from alpService.submitOpenOrdersRequest: " + signedOrderResponse.getError().toString());
            }
        } catch (ErrorGeneratingALPRequestException e) {
            LOG.error(e.getMessage());
        }
    }

    private void tryToRegister(ALPService alpService) {
        ApiResponse registerResponse = alpService.register(opt.apiKey, opt.poolPayoutAddress, opt.exchangeName, unit);

        if (registerResponse.isPositive()) {
            LOG.info("Positive answer from alpService.register: " + registerResponse.getResponseObject().toString());
        } else {
            LOG.error("Error from alpService.register: " + registerResponse.getError().toString());
        }
    }

    private void init(NuBotOptions options) {
        opt = options;
        unit = opt.getPair().getPaymentCurrency().getCode().toLowerCase();

        SessionManager.setModeRunning();

        //Setup the exchange
        try {
            WrapperTestUtils.configureExchange(opt.exchangeName);
        } catch (NuBotConfigException e) {
            LOG.error(e.getMessage());
        }

        LOG.info("Executing ALP test on exchange: " + opt.exchangeName + "\n" +
                "poolURI: " + opt.poolURI + "\n" +
                "apiKey: " + opt.apiKey + "\n" +
                "poolPayoutAddress: " + opt.poolPayoutAddress + "\n" +
                "unit: " + unit);

        //Delete all orders
        //LOG.info("Placing some random orders");

        //WrapperTestUtils.testClearAllOrders(opt.getPair());


        //Place some random orders for the sake of it
        //TradeUtils.placeSomeRandomOrders();
        //        LOG.info("Placing some random orders");


    }


}
