/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.ALPClient;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.utils.TimeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class ALPStats {
    private static final Logger LOG = LoggerFactory.getLogger(ALPStats.class.getName());

    private boolean connected;
    private URL serverAddress;

    //user info
    private Amount roundReward;
    private Amount totalReward;
    private String poolUserID;

    //server stats
    private Date nextPayoutDate;
    private Date lastPayoutDate;

    private double askPayoutRank1;
    private double askPayoutRank2;
    private double bidPayoutRank1;
    private double bidPayoutRank2;
    private String serverUpTime;


    private final Date created; //Creation of the object


    public ALPStats(boolean connected, String serverAddress, Amount roundReward, Amount totalReward, Date nextPayoutDate, Date lastPayoutDate, double askPayoutRank1, double askPayoutRank2, double bidPayoutRank1, double bidPayoutRank2, String serverUpTime, String poolUserID) throws MalformedURLException {
        this.connected = connected;
        this.serverAddress = new URL(serverAddress);
        this.roundReward = roundReward;
        this.totalReward = totalReward;
        this.nextPayoutDate = nextPayoutDate;
        this.lastPayoutDate = lastPayoutDate;
        this.askPayoutRank1 = askPayoutRank1;
        this.askPayoutRank2 = askPayoutRank2;
        this.bidPayoutRank1 = bidPayoutRank1;
        this.bidPayoutRank2 = bidPayoutRank2;
        this.serverUpTime = serverUpTime;
        this.poolUserID = poolUserID;
        this.created = new Date();
    }

    public String getRawHTMLfooter() {
        return "<a href=\"" + serverAddress.toString() + "status\" target=\"_blank\">ServerStats.json</a> | " +
                "<a href=\"" + serverAddress.toString() + poolUserID + "/stats\" target=\"_blank\">UserStats.json</a> | " +
                "<a href=\"" + serverAddress.toString() + poolUserID + "/orders\" target=\"_blank\">UserOrders.json</a> | " +
                "<a href=\"" + serverAddress.toString() + "exchanges\" target=\"_blank\">Exchanges.json</a> | ";
    }

    public static ALPStats generateEmptyOfflineStats() {
        try {
            return new ALPStats(false, Global.options.poolURI,
                    new Amount(0, CurrencyList.NBT), new Amount(0, CurrencyList.NBT),
                    new Date(), new Date(),
                    0, 0, 0, 0, "0", "");
        } catch (MalformedURLException e) {
            LOG.error(e.getMessage());
            return null;
        }

    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public URL getServerAddress() {
        return serverAddress;
    }


    public String getRoundReward() {
        return Double.toString(roundReward.getQuantity());
    }

    public String getTotalReward() {
        return Double.toString(totalReward.getQuantity());

    }


    public String getNextPayoutDate() {
        return TimeUtils.formatDate(nextPayoutDate, true);
    }

    public double getAskPayoutRank1() {
        return askPayoutRank1;
    }

    public double getAskPayoutRank2() {
        return askPayoutRank2;
    }

    public double getBidPayoutRank1() {
        return bidPayoutRank1;
    }

    public double getBidPayoutRank2() {
        return bidPayoutRank2;
    }

    public String getServerUpTime() {
        return serverUpTime;
    }

    public String getPoolUserID() {
        return poolUserID;
    }

    public String getCreated() {
        return TimeUtils.formatDate(created, true);
    }

    public String getLastPayoutDate() {
        return TimeUtils.formatDate(lastPayoutDate, true);
    }

    @Override
    public String toString() {
        return "ALPStats{" +
                "connected=" + connected +
                ", serverAddress=" + serverAddress +
                ", roundReward=" + roundReward +
                ", totalReward=" + totalReward +
                ", poolUserID='" + poolUserID + '\'' +
                ", nextPayoutDate=" + nextPayoutDate +
                ", lastPayoutDate=" + lastPayoutDate +
                ", askPayoutRank1=" + askPayoutRank1 +
                ", askPayoutRank2=" + askPayoutRank2 +
                ", bidPayoutRank1=" + bidPayoutRank1 +
                ", bidPayoutRank2=" + bidPayoutRank2 +
                ", serverUpTime='" + serverUpTime + '\'' +
                ", created=" + created +
                '}';
    }
}
