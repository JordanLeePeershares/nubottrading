/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.ALPClient;


import com.nubits.nubot.models.ApiError;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.trading.TradeUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.URL;
import java.util.*;
import java.util.logging.ErrorManager;

public class ALPService {

    //Wrapper for Nu ALP server as documented on https://github.com/inuitwallet/ALP-Server
    private static final Logger LOG = LoggerFactory.getLogger(ALPService.class.getName());

    private String base;

    public ALPService(String base) {
        this.base = base;
    }

    /*
    POST /register

    Allows a client to register a new user for the exchange/unit combination supplied.
    The exchange API public key serves as a user identifier.

        "user": "Exchange API public Key",
        "address": "A valid NBT payout address",
        "exchange": "A supported exchange",
        "unit": "A supported currency code"

    Sample error response :   {"message": "500 error", "success": false}
    Attention: the response object will be valid also for "user already registered" messages
    */
    public ApiResponse register(String user, String address, String exchange, String unit) {
        ApiResponse resp = new ApiResponse();

        String method = "register";
        boolean isGet = false;
        HashMap<String, String> args = new HashMap<>();

        args.put("user", user);
        args.put("address", address);
        args.put("exchange", exchange);
        args.put("unit", unit);

        String httpAnswer = executeQuery(method, args, isGet);
        JSONParser parser = new JSONParser();

        boolean success = false;
        boolean alreadyRegistered = false;
        String errMessage = "Error while communicating with ALP server : ";
        try {
            JSONObject httpAnswerJSON = (JSONObject) (parser.parse(httpAnswer));
            success = (boolean) httpAnswerJSON.get("success");
            if (!success) {
                String errString = (String) httpAnswerJSON.get("message");
                if (errString.contains("user is already registered")) {
                    alreadyRegistered = true;
                } else {
                    errMessage += errString;
                }
            }
        } catch (ParseException e) {
            errMessage += e.getMessage();
        }

        if (httpAnswer != null && success) {
            resp.setResponseObject(httpAnswer);
        } else if (alreadyRegistered) { //Valid if already registered
            String message = "ALP user " + user + " already registered!";
            LOG.debug(message);
            resp.setResponseObject(message);
        } else {
            resp.setError(new ApiError(-1, errMessage));
        }

        return resp;
    }


    /*
    POST /liquidity

    Allows a client to submit a signed 'get_orders' request to the server.
    The server will use that to collect orders on behalf of the specified user and add them to the pool to be credited.

       "user": "The exchange API public Key used to 'register'",
       "req": "A JSON dictionary containing the parameters required by the exchange API",
       "sign": "The result of signing req with the exchange API private key",
       "exchange": "The target exchange",
       "unit": The target currency"
    */
    public ApiResponse submitOpenOrdersRequest(String user, String req, String sign, String exchange, String unit) {
        ApiResponse resp = new ApiResponse();

        String method = "liquidity";
        boolean isGet = false;
        HashMap<String, String> args = new HashMap<>();

        args.put("user", user);
        args.put("req", req);
        args.put("sign", sign);
        args.put("exchange", exchange);
        args.put("unit", unit);

        String httpAnswer = executeQuery(method, args, isGet);

        JSONParser parser = new JSONParser();
        boolean success = false;
        String errString = "";
        try {
            JSONObject httpAnswerJSON = (JSONObject) (parser.parse(httpAnswer));

            success = (Boolean) httpAnswerJSON.get("success");
            if (!success) {
                errString = (String) httpAnswerJSON.get("message");
            } else {
                success = true;
            }
        } catch (ParseException e) {
            resp.setError(new ApiError(-1, "Parse error: " + e.toString()));
        }

        if (httpAnswer != null && success) {
            resp.setResponseObject(httpAnswer);
        } else if (!success) {
            resp.setError(new ApiError(-1, errString));
        } else {
            resp.setError(new ApiError(-1, "Error while communicating with ALP server"));
        }

        return resp;
    }


    /*
        GET /exchanges

        Shows an object containing the exchanges supported by this ALP and the parameters of each of them.
    */
    public ApiResponse getExchanges() {
        ApiResponse resp = new ApiResponse();

        String method = "exchanges";
        boolean isGet = true;
        AbstractMap<String, String> args = null;

        String httpAnswer = executeQuery(method, args, isGet);

        if (httpAnswer != null) {
            resp.setResponseObject(httpAnswer);
        } else {
            resp.setError(new ApiError(-1, "Error while communicating with ALP server"));
        }

        return resp;
    }


    /*
        GET /status

        Shows a large data object containing lots of data about the performance of the pool and the distribution of
        liquidity on it. For more information and a break down of the data shown, see the Stats section.
    */
    public ApiResponse getServerStatus() {
        ApiResponse resp = new ApiResponse();

        String method = "status";
        boolean isGet = true;
        AbstractMap<String, String> args = null;

        String httpAnswer = executeQuery(method, args, isGet);

        if (httpAnswer != null) {
            resp.setResponseObject(httpAnswer);
        } else {
            resp.setError(new ApiError(-1, "Error while communicating with ALP server"));
        }

        return resp;
    }


    /*
        GET /<user>/orders

        Shows the orders on record for the given user. This includes details of any credits associated with the order.
    */
    public ApiResponse getUserOrders(String user) {
        ApiResponse resp = new ApiResponse();

        String method = user + "/orders";
        boolean isGet = true;
        AbstractMap<String, String> args = null;

        String httpAnswer = executeQuery(method, args, isGet);

        if (httpAnswer != null) {
            resp.setResponseObject(httpAnswer);
        } else {
            resp.setError(new ApiError(-1, "Error while communicating with ALP server"));
        }

        return resp;
    }


    /*
         GET /<user>/stats

         Shows the statistics for the given user. This includes a history of the users net worth to allow for
         easier tracking of profit/loss.
    */
    public ApiResponse getUserStats(String user) {
        ApiResponse resp = new ApiResponse();

        String method = user + "/stats";
        boolean isGet = true;
        AbstractMap<String, String> args = null;

        String httpAnswer = executeQuery(method, args, isGet);

        if (httpAnswer != null) {
            resp.setResponseObject(httpAnswer);
        } else {
            resp.setError(new ApiError(-1, "Error while communicating with ALP server"));
        }

        return resp;
    }


    /**
     * @param method
     * @param args
     * @param isGet
     * @return
     */

    private String executeQuery(String method, AbstractMap<String, String> args, boolean isGet) {
        String answer = null;
        String argsString = "";
        String url = base + method;

        String postJSONString = "{";

        int paramCount = 0;
        if (args != null) {
            for (Iterator<Map.Entry<String, String>> argumentIterator = args.entrySet().iterator(); argumentIterator.hasNext(); ) {
                paramCount++;
                Map.Entry<String, String> argument = argumentIterator.next();

                String k = argument.getKey().toString();
                String v = argument.getValue().toString();

                String newJSONelem = k.equals("req") ? "\"" + k + "\":" + v : "\"" + k + "\":" + "\"" + v + "\"";

                postJSONString += newJSONelem;
                if (argumentIterator.hasNext())
                    postJSONString += ",";


                if (paramCount > 1) {
                    argsString += "&";
                }
                argsString += argument.getKey() + "=" + argument.getValue();
            }
        }
        postJSONString += "}";

        if (isGet) {
            url = argsString.length() > 0 ? url + "?" + argsString : url;
        }

        LOG.debug("url = " + url +
                ", postData = " + postJSONString +
                ", argsString = " + argsString +
                ", isGet = " + isGet);

        // add header
        Header[] headers = new Header[1];
        headers[0] = new BasicHeader("Content-Type", "application/json");


        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = null;
        HttpGet get = null;
        HttpResponse response = null;

        try {
            if (!isGet) {
                post = new HttpPost(url);
                StringEntity params = new StringEntity(postJSONString);
                post.setEntity(params);
                post.setHeaders(headers);
                response = client.execute(post);
            } else {
                get = new HttpGet(url);
                get.setHeaders(headers);
                response = client.execute(get);
            }
        } catch (NoRouteToHostException e) {
            if (!isGet) {
                post.abort();
            } else {
                get.abort();
            }
            LOG.error(e.toString());
            return null;
        } catch (SocketException e) {
            if (!isGet) {
                post.abort();
            } else {
                get.abort();
            }
            LOG.error(e.toString());
            return null;
        } catch (Exception e) {
            if (!isGet) {
                post.abort();
            } else {
                get.abort();
            }
            LOG.error(e.toString());
            return e.toString();
        }
        BufferedReader rd;


        try {
            rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));


            StringBuffer buffer = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                buffer.append(line);
            }

            answer = buffer.toString();
        } catch (IOException ex) {

            LOG.error(ex.toString());
            return null;
        } catch (IllegalStateException ex) {

            LOG.error(ex.toString());
            return null;
        }

        LOG.trace("\nSending request to URL : " + url + " ; get = " + isGet);
        LOG.trace("Response Code : " + response.getStatusLine().getStatusCode());
        LOG.trace("Response :" + response);

        return answer;
    }


    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }
}
