package com.nubits.nubot.launch.toolkit;

import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.pricefeeds.feedservices.AbstractPriceFeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * a simple launch of pricefeeder
 */
public class LaunchFeed {
    private static final Logger LOG = LoggerFactory.getLogger(LaunchFeed.class.getName());

    public static void example() {
        CurrencyPair testPair = CurrencyList.BTC_USD;
        ArrayList<AbstractPriceFeed> allfeeds = FeedFacade.getAllExistingFeeds();

        int fn = allfeeds.size();

        Iterator<AbstractPriceFeed> it = allfeeds.iterator();
        System.out.println("query feeds " + allfeeds.size());
        assert (allfeeds.size() > 0);

        double sum = 0.0;
        int n = 0;
        int fails = 0;

        while (it.hasNext()) {

            AbstractPriceFeed feed = it.next();
            System.out.println("query feed " + feed);
            LastPrice lastprice = null;
            try {
                lastprice = feed.getLastPrice(testPair);
            } catch (FeedCallException e) {
                LOG.error(e.toString());
            }


            try {
                double ld = lastprice.getPrice().getQuantity();
                System.out.println("price: " + ld);

                sum += ld;

                n++;
            } catch (Exception e) {
                fails++;
            }
        }

        System.out.println("feed n: " + fn);
        System.out.println("successes: " + n);
        System.out.println("fails: " + fails);

    }

    public static void main(String... args) {
        for (int i = 0; i < 100; i++) {
            example();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {


            }
        }
    }
}
