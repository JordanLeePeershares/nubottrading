/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.launch;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.NuBotRunException;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.options.SaveOptions;
import com.nubits.nubot.utils.FilesystemUtils;
import com.nubits.nubot.utils.VersionInfo;
import com.nubits.nubot.webui.UiServer;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


/**
 * the main launcher class. either start bot through commandline
 * or a GUI is launched where user starts the Bot himself
 */
public class MainLaunch {

    static {
        System.setProperty("logback.configurationFile", Settings.LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(MainLaunch.class.getName());


    /**
     * Start the NuBot. start if config is valid and other instance is running
     *
     * @param args a list of valid arguments
     */
    public static void main(String args[]) {

        Global.logFolder = "" + System.currentTimeMillis();

        Global.defineLogPath(Settings.ORPHANS_LOGFOLDER + "/" + Global.logFolder);

        boolean isLatestOrDevelop = true;
        String latestWarningMessage = "";
        LOG.debug("checking latest version");
        if (!VersionInfo.isCurrentVersionOk()) {
            String latest = VersionInfo.getRemoteVersionString();
            String current = VersionInfo.getVersionName();
            isLatestOrDevelop = false;
            latestWarningMessage = "Your version of " + Settings.APP_NAME + " (" + current + ") is not updated." +
                    " A newer version (" + latest + ") is available for download at " + Settings.OFFICIAL_DOWNLOADS_URL;
            LOG.warn(latestWarningMessage);
        }
        CommandLine cli = parseArgs(args);

        boolean byPassNuAuth = false;
        if (cli.hasOption(CLIOptions.BYPASS_NUDEV_AUTH)) { //if set to true will use custom defined keys to third party services
            LOG.warn("Official credential skipped. Will use user-defined credentials defined in source of CredentialManager.java");
            byPassNuAuth = true;
        }

        //Try to setup the access manager
        try {
            Global.credentialManager = CredentialManager.createCredentialManager(byPassNuAuth);
        } catch (Exception e) {
            LOG.error(e.toString());
            MainLaunch.exitWithNotice("Problems while creating CredentialManager.");
        }


        boolean runGUI = false;
        String configFile;

        boolean defaultCfg = false;
        if (cli.hasOption(CLIOptions.SERVER)) {
            runGUI = true;
            LOG.info("Running " + Settings.APP_NAME + " with server");

            if (cli.hasOption(CLIOptions.WEBPORT)) {
                Global.webPort = Integer.parseInt(cli.getOptionValue(CLIOptions.WEBPORT));
            }
            if (!cli.hasOption(CLIOptions.CFG)) {
                LOG.info("Setting default config file location :" + Settings.DEFAULT_CONFIG_FILE_PATH);
                //Cancel any previously existing file, if any
                File f = new File(Settings.DEFAULT_CONFIG_FILE_PATH);
                if (f.exists() && !f.isDirectory()) {
                    LOG.warn("Detected a non-empty configuration file, resetting it to default. " +
                            "Printing existing file content, for reference:\n"
                            + FilesystemUtils.readFromFile(Settings.DEFAULT_CONFIG_FILE_PATH));
                    FilesystemUtils.deleteFile(Settings.DEFAULT_CONFIG_FILE_PATH);
                }
                //Create a default file
                SaveOptions.optionsReset(Settings.DEFAULT_CONFIG_FILE_PATH);
                defaultCfg = true;
            }
        }
        if (cli.hasOption(CLIOptions.CFG) || defaultCfg) {
            if (defaultCfg) {
                configFile = Settings.DEFAULT_CONFIG_FILE_PATH;
            } else {
                configFile = cli.getOptionValue(CLIOptions.CFG);
            }

            if (runGUI) {
                boolean openBrowser = true;
                if (cli.hasOption(CLIOptions.NO_BROWSER)) {
                    openBrowser = false;
                }
                SessionManager.setConfigGlobal(configFile, true);
                Global.updateLiquidityDistManger();
                try {
                    UiServer.startUIserver(configFile, defaultCfg, !isLatestOrDevelop, latestWarningMessage, openBrowser);
                    Global.createShutDownHook();
                } catch (Exception e) {
                    LOG.error("error setting up UI server " + e);
                }

            } else {
                LOG.info("Run NuBot from CLI");
                //set global config
                SessionManager.setConfigGlobal(configFile, false);
                Global.updateLiquidityDistManger();

                //sessionLOG.debug("launch bot");
                try {
                    SessionManager.setModeStarting();
                    SessionManager.launchBot(Global.options);
                    Global.createShutDownHook();
                } catch (NuBotRunException e) {
                    exitWithNotice("could not launch bot " + e);
                }
            }


        } else {
            exitWithNotice("Missing " + CLIOptions.CFG + ". run nubot with \n" + CLIOptions.USAGE_STRING);
        }

    }

    public static CommandLine parseArgs(String args[]) {
        CLIOptions argsParser = new CLIOptions();
        Options options = argsParser.constructGnuOptions();
        return argsParser.parseCommandLineArguments(args, options);
    }


    /**
     * exit application and notify user
     *
     * @param msg
     */
    public static void exitWithNotice(String msg) {
        LOG.error(msg);
        System.exit(0);
    }

}
