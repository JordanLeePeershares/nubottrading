/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.LiquidityDistribution;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class LiquidityCurve {

    private static final Logger LOG = LoggerFactory.getLogger(LiquidityCurve.class.getName());
    public static final String STEEPNESS_LOW = "LOW";
    public static final String STEEPNESS_MID = "MID";
    public static final String STEEPNESS_HIGH = "HIGH";
    public static final String STEEPNESS_FLAT = "FLAT";
    public static final String TYPE_LIN = "LIN";
    public static final String TYPE_EXP = "EXP";
    public static final String TYPE_LOG = "LOG";
    private String type; //type of model, lin-log-exp
    protected String steepness; //steepness of the curve, low-mid-high
    //Abstract methods

    public LiquidityCurve(String steepness) {
        if (steepness.equalsIgnoreCase(STEEPNESS_LOW) || steepness.equalsIgnoreCase(STEEPNESS_MID) || steepness.equalsIgnoreCase(STEEPNESS_HIGH) || steepness.equalsIgnoreCase(STEEPNESS_FLAT)) {
            this.steepness = steepness.toUpperCase();
        } else {
            LOG.error("Value not accepted for steepness : " + steepness);

        }
    }

    abstract double[] computeOrderSize(double[] prices, double wallHeight, String wallType, double startPrice, double pegPrice);

    abstract double computeCoefficient();

    abstract public String getName();

    public String getSteepness() {
        return steepness;
    }

    abstract double computeIncrement(int index, double wallHeight, double deltaP, double pegPrice);

    public static LiquidityCurve generateCurve(String type, String steepness) {
        if (type.equalsIgnoreCase(TYPE_EXP)) {
            return new LiquidityCurveExp(steepness);
        } else if (type.equalsIgnoreCase(TYPE_LIN)) {
            return new LiquidityCurveLin(steepness);
        } else if (type.equalsIgnoreCase(TYPE_LOG)) {
            return new LiquidityCurveLog(steepness);
        } else {
            LOG.error("Unrecognized curve type : " + type + " . Returning Linear curve.");
            return new LiquidityCurveLin(steepness);
        }
    }
}
