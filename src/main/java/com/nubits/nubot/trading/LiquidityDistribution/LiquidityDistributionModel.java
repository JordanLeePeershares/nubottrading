/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.LiquidityDistribution;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class LiquidityDistributionModel {

    private static final Logger LOG = LoggerFactory.getLogger(LiquidityDistributionModel.class.getName());
    private ModelParameters sellParams, buyParams;

    private static final double BOOK_MAX_WIDTH = 0.5;

    public LiquidityDistributionModel(ModelParameters sellParams, ModelParameters buyParams) {
        this.sellParams = sellParams;
        this.buyParams = buyParams;

    }

    public LiquidityDistributionModel(ModelParameters params, String type) {
        if (type.equals(Constant.SELL)) {
            this.sellParams = params;
        } else {
            this.buyParams = params;
        }
    }

    /**
     * Get orders to place Tier1+Tier2
     *
     * @param type
     * @param balance
     * @param pegPrice
     * @param pair
     * @param txFee
     * @return
     */
    public OrderBatch getOrdersToPlace(String type, Amount balance, double pegPrice, CurrencyPair pair, double txFee, double cap, boolean disableT2) {

        //See if a CAP is needed on the funds available
        Amount availableFunds = new Amount(balance.getQuantity(), balance.getCurrency());
        double NBTbalanceEquivalent = balance.getQuantity();

        if (type.equals(Constant.BUY)) {
            NBTbalanceEquivalent *= pegPrice;
        }

        if (cap > 0) {
            //Might need to put a cap on the available balance
            if (NBTbalanceEquivalent > cap) {
                double amountTemp = cap;
                if (type.equals(Constant.BUY)) {
                    amountTemp /= pegPrice; //convert back
                }
                amountTemp = Utils.round(amountTemp);
                availableFunds.setQuantity(amountTemp);
                LOG.warn("Putting a cap on " + type + " liquidity available. [Total Balance  : " + balance.toString() + " ; capped @  " + amountTemp + " " + balance.getCurrency().getCode() + "]");
            }
        }

        //Compute the walls (t1)
        boolean hasWall = true;
        OrderToPlace wall = getWallToPlace(type, availableFunds, pegPrice, pair, txFee);
        double wallHeight = wall.getSize();

        int indexWall = 0;
        if (wallHeight == 0) {
            hasWall = false;
            indexWall = -1;
        }

        ArrayList<OrderToPlace> orders = new ArrayList<>();

        //Compute the tail (t2)
        if (!disableT2) {
            ArrayList<OrderToPlace> t2Orders = new ArrayList<>();
            t2Orders = getOrdersToPlaceTier2(wall, type, availableFunds, pegPrice, pair);

            if (t2Orders.size() > 0) {
                orders = t2Orders;
                if (hasWall) {
                    orders.add(0, wall);
                }
            } else {
                if (hasWall) {
                    orders.add(0, wall);
                }
            }
        } else {
            if (hasWall) {
                orders.add(0, wall);
            }
        }

        OrderBatch toReturn = new OrderBatch(orders, indexWall);
        if (pair.getPaymentCurrency().equals(CurrencyList.NBT)) {
            LOG.debug("Computing swapped order book");
            toReturn = toReturn.swap();
        }
        return toReturn;

    }

    //Tier2
    private ArrayList<OrderToPlace> getOrdersToPlaceTier2(OrderToPlace wall, String type, Amount fundsAvailable, double pegPrice, CurrencyPair pair) {
        if (type.equals(Constant.SELL)) {
            return getOrdersToPlaceImpl(wall, this.sellParams, fundsAvailable, Constant.SELL, pegPrice, pair);
        } else {
            return getOrdersToPlaceImpl(wall, this.buyParams, fundsAvailable, Constant.BUY, pegPrice, pair);
        }
    }

    public double computeWallHeight(String type, Amount fundsAvailable, double pegPrice) {
        double totalFundsAvailNBT = fundsAvailable.getQuantity();

        //Switch params
        ModelParameters params;
        if (type.equals(Constant.SELL)) {
            params = this.sellParams;
        } else {
            params = this.buyParams;
            totalFundsAvailNBT = totalFundsAvailNBT * pegPrice;
        }

        double wallHeight = params.getWallHeight();

        if (wallHeight > totalFundsAvailNBT) {
            LOG.info("The current balance equivalent " + totalFundsAvailNBT
                    + " NBT is not enought to place the full " + type + " wall"
                    + " defined in the liquidity model (" + wallHeight + "). "
                    + "\nResizing the size of the order");

            wallHeight = totalFundsAvailNBT;
        }
        return wallHeight;

    }

    //Tier1
    private OrderToPlace getWallToPlace(String type, Amount fundsAvailable, double pegPrice, CurrencyPair pair, double txFee) {
        OrderToPlace toReturn = null;

        //Switch params
        ModelParameters params;
        if (type.equals(Constant.SELL)) {
            params = this.sellParams;
        } else {
            params = this.buyParams;
        }
        //First create the wall order and add it to the list
        double oneUSD = Utils.round(1 / pegPrice); //one $ expressed in the peg currency
        double wallPrice = oneUSD;

        double offset = Utils.round(params.getOffset() * oneUSD); //Convert the offset in the peg currency

        double fee = Utils.round((oneUSD / 100) * txFee); //Convert the txFee in the peg currency

        double totalOffset = offset + fee;//Compute the total offset by adding spread+fee

        //Add it or remove it from the price, based on the type of order
        if (type.equals(Constant.SELL)) {
            wallPrice += totalOffset;
            if (!Global.options.isDualSide()) {
                wallPrice += Utils.round(Global.options.getPriceIncrement() * oneUSD);
            }
        } else {
            wallPrice -= totalOffset;
        }
        wallPrice = Utils.round(wallPrice);

        double wallPriceUSD = wallPrice * pegPrice;
        double wallHeight = computeWallHeight(type, fundsAvailable, pegPrice);

        toReturn = new OrderToPlace(type, pair, wallHeight, wallPrice);

        return toReturn;
    }

    private ArrayList<OrderToPlace> getSellOrdersToPlace(OrderToPlace sellWall, Amount fundsAvailable, double pegPrice, CurrencyPair pair) {
        return getOrdersToPlaceImpl(sellWall, this.sellParams, fundsAvailable, Constant.SELL, pegPrice, pair);
    }

    private ArrayList<OrderToPlace> getBuyOrdersToPlace(OrderToPlace buyWall, Amount fundsAvailable, double pegPrice, CurrencyPair pair) {
        return getOrdersToPlaceImpl(buyWall, this.buyParams, fundsAvailable, Constant.BUY, pegPrice, pair);
    }

    //Tier2 liquidity
    private ArrayList<OrderToPlace> getOrdersToPlaceImpl(OrderToPlace wall, ModelParameters params, Amount funds, String type, double pegPrice, CurrencyPair pair) {


        ArrayList<OrderToPlace> toReturn = new ArrayList();

        double oneUSD = Utils.round(1 / pegPrice); //one $ expressed in the peg currency

        double endPrice; // The last price of the book
        double wallWidthPeg = Utils.round(BOOK_MAX_WIDTH * oneUSD); //half dollar per side of width

        double totalFundsAvail = funds.getQuantity();
        double wallPrice = wall.getPrice();
        double wallHeight = wall.getSize();

        //Add it or remove it from the price, based on the type of order
        if (type.equals(Constant.SELL)) {
            endPrice = wallPrice + wallWidthPeg;
        } else {
            endPrice = wallPrice - wallWidthPeg;
            totalFundsAvail = Utils.round(totalFundsAvail * pegPrice); //convert to NBT
        }

        totalFundsAvail = totalFundsAvail - wallHeight;  //Reserve the amount necessary to place the wall

        boolean tailAvailable = false;
        if (totalFundsAvail > 0) {
            tailAvailable = true;
        } else {
            LOG.warn("The balance is not enought to place the tier2 liquidity. (Balance is entirely on tier1 wall) " + wallHeight);
        }

        if (tailAvailable) {
            //Compute the array of prices
            double[] prices = getPriceArray(wallPrice, endPrice, type, params.getInterval(), pegPrice);
            double[] sizes = params.getCurve().computeOrderSize(prices, params.getWallHeight(), type, wallPrice, pegPrice);

            for (int i = 0; i < prices.length; i++) {
                if (i < Settings.MAX_NUMBER_OF_ORDERS_PER_SIDE) {
                    if (sizes[i] <= totalFundsAvail) {
                        toReturn.add(new OrderToPlace(type, pair, Utils.round(sizes[i]), prices[i]));
                        totalFundsAvail -= sizes[i];
                    } else {
                        LOG.debug("Not enough liquidity to place the full " + type + " tail as defined in the model. Skipping the rest of the tail"
                                + "\ntotalFundsAvail = " + totalFundsAvail + " NBT ; size required by order (" + i + ") = " + sizes[i]);
                        break;
                    }
                } else {
                    LOG.warn("Limiting max number of orders in tier2 to hardcoded limit : " + Settings.MAX_NUMBER_OF_ORDERS_PER_SIDE);
                    break;
                }
            }

            //Compute the starting volume of the first offset such that all available balance is always put on order
            if (totalFundsAvail > 0) {
                //If funds are left, put all the remaining on an extra order (tier2 liquidity should be available to be optimised)
                //The price of the last order should change according to the size of the new altered order

                if (toReturn.size() > 1) {
                    OrderToPlace lastOrder = toReturn.get(toReturn.size() - 1);
                    double oldSize = lastOrder.getSize();
                    double newSize = oldSize + totalFundsAvail;
                    double oldPrice = lastOrder.getPrice();

                    double prevDeltaSize = Math.abs(oldSize - toReturn.get(toReturn.size() - 2).getSize());
                    double prevDeltaPrice = Math.abs(oldPrice - toReturn.get(toReturn.size() - 2).getPrice());
                    double rate;
                    if (prevDeltaSize == 0) {
                        rate = 1;
                    } else {
                        rate = totalFundsAvail / prevDeltaSize;
                    }
                    double deltaPrice = 0;
                    if (rate >= 1) {
                        deltaPrice = Utils.round(rate * prevDeltaPrice);
                    }
                    double newPrice;
                    if (type.equals(Constant.SELL)) {
                        newPrice = oldPrice + deltaPrice;
                        if (newPrice < (1 + BOOK_MAX_WIDTH) * pegPrice)
                            newPrice = Utils.round(oldPrice * 1.2);
                    } else {
                        newPrice = oldPrice - deltaPrice;
                        if (newPrice < (1 - BOOK_MAX_WIDTH) * pegPrice)
                            newPrice = Utils.round(oldPrice * 0.8);
                    }


                    if (totalFundsAvail >= oldSize) { //Add a new order with funds available
                        toReturn.add(new OrderToPlace(type, pair, Utils.round(totalFundsAvail), Utils.round(newPrice)));

                    } else { //Alter existing order
                        toReturn.get(toReturn.size() - 1).setSize(Utils.round(newSize));
                        toReturn.get(toReturn.size() - 1).setPrice(Utils.round(newPrice));
                    }
                } else {
                    if (toReturn.size() == 1) {
                        toReturn.get(0).setSize(toReturn.get(0).getSize() + totalFundsAvail);
                    } else {
                        //only t1, create a new t2 order
                        double orderPrice;
                        if (type.equals(Constant.SELL))
                            orderPrice = wallPrice + (Utils.round(params.getInterval() / pegPrice));
                        else
                            orderPrice = wallPrice - (Utils.round(params.getInterval() / pegPrice));

                        toReturn.add(new OrderToPlace(type, pair, totalFundsAvail, orderPrice));
                    }
                }


            }

        }
        return toReturn;
    }


    private double[] getPriceArray(double startPrice, double endPrice, String type, double density, double pegPrice) {
        double oneUSD = Utils.round(1 / pegPrice); //one $ expressed in the peg currency
        double distanceAmongOrders = Utils.round(density * oneUSD); //Convert the distance in the peg currency
        int numberOfElements = Utils.safeLongToInt(Math.round((Math.abs(startPrice - endPrice)) / distanceAmongOrders));
        double[] toReturn = new double[numberOfElements];

        for (int i = 0; i < numberOfElements; i++) {
            if (type.equals(Constant.SELL)) {
                toReturn[i] = Utils.round(startPrice + ((i + 1) * distanceAmongOrders));
            } else {
                toReturn[i] = Utils.round(startPrice - ((i + 1) * distanceAmongOrders));
            }
        }


        return toReturn;
    }

    public ModelParameters getSellParams() {
        return sellParams;
    }

    public void setSellParams(ModelParameters sellParams) {
        this.sellParams = sellParams;
    }

    public ModelParameters getBuyParams() {
        return buyParams;
    }

    public void setBuyParams(ModelParameters buyParams) {
        this.buyParams = buyParams;
    }


}
