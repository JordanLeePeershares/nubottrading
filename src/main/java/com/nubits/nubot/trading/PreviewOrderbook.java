package com.nubits.nubot.trading;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.pricefeeds.feedservices.AbstractPriceFeed;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityCurve;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityDistributionModel;
import com.nubits.nubot.trading.LiquidityDistribution.ModelParameters;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.nubits.nubot.utils.LiquidityPlot.*;

/**
 */
public class PreviewOrderbook {
    private static final Logger LOG = LoggerFactory.getLogger(PreviewOrderbook.class.getName());

    public static void drawOrderBooks(ArrayList<Order> sellOrders, ArrayList<Order> buyOrders) {

        double[] xSell = new double[sellOrders.size()];
        double[] ySell = new double[sellOrders.size()];
        double[] xBuy = new double[buyOrders.size()];
        double[] yBuy = new double[buyOrders.size()];


        for (int i = 0; i < sellOrders.size(); i++) {
            Order tempOrder = sellOrders.get(i);
            xSell[i] = tempOrder.getPrice().getQuantity() * 100;
            ySell[i] = tempOrder.getAmount().getQuantity();

        }

        for (int i = 0; i < buyOrders.size(); i++) {
            Order tempOrder = buyOrders.get(i);
            xBuy[i] = tempOrder.getPrice().getQuantity() * 100;
            yBuy[i] = tempOrder.getAmount().getQuantity();

        }

        plot(xSell, ySell); // create a plot using xaxis and yvalues
        addPlot(xBuy, yBuy); // create a second plot on top of first

    }

    public static JSONObject drawOrderBooks(ArrayList<OrderToPlace> sellOrders, ArrayList<OrderToPlace> buyOrders, double pegPrice, boolean showJavaPreview, boolean swapped) {

        JSONObject toRet = new JSONObject();
        JSONArray xArray = new JSONArray();
        JSONArray bidsArray = new JSONArray();
        JSONArray asksArray = new JSONArray();

        double[] xSell = new double[sellOrders.size()];
        double[] ySell = new double[sellOrders.size()];
        double[] xBuy = new double[buyOrders.size()];
        double[] yBuy = new double[buyOrders.size()];


        for (int i = 0; i < sellOrders.size(); i++) {
            OrderToPlace tempOrder = sellOrders.get(i);
            xSell[i] = swapped ? tempOrder.getPrice() / pegPrice * 100 : tempOrder.getPrice() * pegPrice * 100;
            ySell[i] = tempOrder.getSize();

        }

        for (int i = 0; i < buyOrders.size(); i++) {
            OrderToPlace tempOrder = buyOrders.get(i);
            xBuy[i] = swapped ? tempOrder.getPrice() / pegPrice * 100 : tempOrder.getPrice() * pegPrice * 100;
            yBuy[i] = tempOrder.getSize();

        }

        boolean somethingDrawn = false;
        boolean bothDrawn = false;
        if (xSell.length > 0 && ySell.length > 0) {
            if (showJavaPreview)
                plot(xSell, ySell); // create a plot using xaxis and yvalues
            somethingDrawn = true;
        }
        if (xBuy.length > 0 && yBuy.length > 0) {
            if (showJavaPreview)

                addPlot(xBuy, yBuy); // create a second plot on top of first
            if (somethingDrawn)
                bothDrawn = true;
            somethingDrawn = true;
        }


        //add a line at center?
        double[] xCenter = new double[1];
        xCenter[0] = 100;
        double[] yCenter = new double[1];
        double height = 100;
        if (bothDrawn) {
            yCenter[0] = 0.9 * (yBuy[0] + ySell[0]) / 2; //a bit less of the average of two walls
        }

        if (!somethingDrawn) {
            if (showJavaPreview)
                plot(xCenter, yCenter);
        } else {
            if (showJavaPreview)
                addPlot(xCenter, yCenter); // create a second plot on top of first
        }

        int xArraySize = yBuy.length + ySell.length;

        for (int i = 0; i < xArraySize; i++) {
            double tempX, tempBid, tempAsk;
            if (i < xBuy.length) {
                tempX = Utils.roundPlaces(xBuy[xBuy.length - 1 - i] / 100, 4);
                tempBid = Utils.roundPlaces(yBuy[xBuy.length - 1 - i], 4);
                tempAsk = 0;
            } else {
                tempX = Utils.roundPlaces(xSell[i - xBuy.length] / 100, 4);
                tempBid = 0;
                tempAsk = Utils.roundPlaces(ySell[i - xBuy.length], 4);
            }
            if (i == xBuy.length) {
                xArray.add(1);
                bidsArray.add(0);
                asksArray.add(0);
            }

            xArray.add(tempX);

            bidsArray.add(tempBid);
            asksArray.add(tempAsk);

        }


        toRet.put("xArray", xArray);
        if (swapped) {
            toRet.put("asks", bidsArray);
            toRet.put("bids", asksArray);
        } else {
            toRet.put("bids", bidsArray);
            toRet.put("asks", asksArray);
        }

        return toRet;
    }

    public static String getOrderBookStats(ArrayList<OrderToPlace> orders, String type, double wallHeight, double pegPrice, double balance) {
        String toReturn = "----- " + type + "-side order book : \n";
        if (Global.options.getPair().getPaymentCurrency().equals(CurrencyList.NBT)) {
            //Swapped
            toReturn += "\n!Swapped orderbook \n";
        }
        if (orders.size() == 0) {
            return toReturn += "\nempty (no balance available?)";
        }
        if (wallHeight == 0) {
            toReturn += type + " wall is null. (tier1 liquidity will not be placed)\n";
        }
        double sumSize = 0;
        for (int i = 0; i < orders.size(); i++) {
            OrderToPlace tempOrder = orders.get(i);
            //translate to USD (price), realprice , and NBT ( Volume )
            if (Global.options.getPair().getPaymentCurrency().equals(CurrencyList.NBT)) {
                //Swapped
                toReturn += Utils.roundPlaces(tempOrder.getPrice() / pegPrice, 6) + "," + tempOrder.getPrice() + "," + Utils.roundPlaces(tempOrder.getSize() * pegPrice, 6) + "," + Utils.roundPlaces(tempOrder.getSize(), 6) + "\n";
            } else {
                toReturn += Utils.roundPlaces(tempOrder.getPrice() * pegPrice, 6) + "," + tempOrder.getPrice() + "," + tempOrder.getSize() + "\n";
            }
            sumSize += tempOrder.getSize();
        }

        double bestprice = orders.get(0).getPrice();
        double bestpriceUSD = bestprice * pegPrice;

        if (wallHeight == 0) {
            toReturn += type + " wall is null. (tier1 liquidity will not be placed)\n";
        } else {
            toReturn += type + " wall volume : " + wallHeight + "\n";
        }
        toReturn += type + " tier2 volume = " + (sumSize - wallHeight) + " NBT \n";
        toReturn += "Total " + type + " volume = " + sumSize + " NBT \n";

        toReturn += "Best price :" + bestprice + " (" + bestpriceUSD + "$)\n";


        if (type.equals(Constant.BUY)) {
            balance = Utils.round(balance * pegPrice);
        }

        toReturn += type + " balance left = " + (balance - sumSize) + " NBT";


        boolean overThreshold = false;
        if (sumSize > balance) {
            overThreshold = true;
        }

        if (overThreshold) {
            toReturn += "\n\n!The funds are not sufficient to satisfy current order books configuration!";
        }

        toReturn += "----- ";
        return toReturn;
    }

    public static JSONObject previewOrderBook(NuBotOptions options, boolean javaPreview) {
        JSONObject toReturn = new JSONObject();
        JSONObject orderbookJSON = new JSONObject();
        JSONObject statsJSON = new JSONObject();
        boolean error = false;
        String errorMessage = "";

        CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(options.pair);

        ModelParameters sellParams = new ModelParameters(options.bookSellOffset, options.bookSellwall,
                options.bookSellInterval, LiquidityCurve.generateCurve(options.bookSellType, options.bookSellSteepness), options.bookSellMaxVolumeCumulative);
        ModelParameters buyParams = new ModelParameters(options.bookBuyOffset, options.bookBuywall,
                options.bookBuyInterval, LiquidityCurve.generateCurve(options.bookBuyType, options.bookBuySteepness), options.bookBuyMaxVolumeCumulative);


        LiquidityDistributionModel ldm = new LiquidityDistributionModel(sellParams, buyParams);


        //Prepare the trade interface to get balances and pegprice
        //Create the ApiKeys object reading from option files
        ApiKeys keys = new ApiKeys(options.getApiSecret(), options.getApiKey());

        //Create a new TradeInterface object using the custom implementation
        TradeInterface tradeInterface = ExchangeFacade.getInterfaceByName(options.exchangeName, keys);

        //Assign the keys to the TradeInterface
        tradeInterface.setKeys(keys);

        //Get balance
        ApiResponse balancesResponse = tradeInterface.getAvailableBalances(pair);
        PairBalance balance = null;
        if (balancesResponse.isPositive()) {
            balance = (PairBalance) balancesResponse.getResponseObject();
            LOG.info(balance.toString());
        } else {
            LOG.error(balancesResponse.getError().toString());
            error = true;
            errorMessage = "cannot get balance, check your apikeys:" + balancesResponse.getError().toString();
        }
        boolean swapped = pair.getPaymentCurrency().equals(CurrencyList.NBT);

        if (!error) {
            Amount balanceNBT = balance.getNBTAvailable();
            Amount balancePEG = balance.getPEGAvailableBalance();


            //Now get the pegprice
            double pegPrice = 1;
            if (options.requiresSecondaryPegStrategy()) {
                //Only if pegCurrency is not USD

                AbstractPriceFeed feed = null;
                try {
                    feed = FeedFacade.getFeed(FeedFacade.getDefaultMainFeed(pair));
                } catch (Exception e) {
                    LOG.error(e.getMessage());
                    error = true;
                    errorMessage = "cannot get feed:" + e.getMessage();
                }

                if (!error) {

                    LastPrice lastPrice = null;
                    try {
                        lastPrice = feed.getLastPrice(pair);
                    } catch (FeedCallException e) {
                        LOG.error(e.getMessage());
                        error = true;
                        errorMessage = "Error while updating " + pair.getOrderCurrency().getCode() + ""
                                + " price from " + feed.getClass() + " : " + e.toString();
                    }
                    if (!error) {

                        pegPrice = lastPrice.getPrice().getQuantity();
                    }

                    //uncomment for manual settings
                    //Amount balanceNBT = new Amount(10000, CurrencyList.NBT);
                    //Amount balancePEG = new Amount(30, CurrencyList.BTC);
                    //double pegPrice = 300;

                    //Compute orders to place t1+t2

                    ArrayList<OrderToPlace> sellOrders = ldm.getOrdersToPlace(Constant.SELL, balanceNBT, pegPrice, pair, options.txFee, options.bookSellMaxVolumeCumulative, options.bookDisabletier2).getOrders();
                    ArrayList<OrderToPlace> buyOrders = ldm.getOrdersToPlace(Constant.BUY, balancePEG, pegPrice, pair, options.txFee, options.bookBuyMaxVolumeCumulative, options.bookDisabletier2).getOrders();


                    orderbookJSON = drawOrderBooks(sellOrders, buyOrders, pegPrice, javaPreview, swapped);

                    //TODO get stats in json
                    String sellOrderBookInfo = getOrderBookStats(sellOrders, Constant.SELL, ldm.computeWallHeight(Constant.SELL, balanceNBT, pegPrice), pegPrice, balanceNBT.getQuantity());
                    String buyOrderBookInfo = getOrderBookStats(buyOrders, Constant.BUY, ldm.computeWallHeight(Constant.BUY, balancePEG, pegPrice), pegPrice, balancePEG.getQuantity());

                    LOG.info(sellOrderBookInfo);
                    LOG.info(buyOrderBookInfo);

                    if (javaPreview) {
                        appendBookInfo(Constant.SELL, sellOrderBookInfo);
                        appendBookInfo(Constant.BUY, buyOrderBookInfo);
                    }
                }
            }
        }
        toReturn.put("error", error);
        toReturn.put("errorMessage", errorMessage);

        toReturn.put("orderbook", orderbookJSON);
        toReturn.put("stats", statsJSON);

        toReturn.put("volumeCurrency", swapped ? pair.getOrderCurrency().getCode() : CurrencyList.NBT.getCode());

        return toReturn;
    }
}
