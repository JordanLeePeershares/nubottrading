/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.wrappers;

import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.models.Currency;
import com.nubits.nubot.trading.*;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.utils.TimeUtils;
import com.nubits.nubot.utils.Utils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.net.URL;
import java.util.*;


public class HitbtcWrapper implements TradeInterface {
    private static final Logger LOG = LoggerFactory.getLogger(HitbtcWrapper.class.getName());

    private double factor = 0;
    private boolean digitsFetched = false;
    private int digits = Settings.DEFAULT_PRECISION;

    private ApiKeys keys;
    protected HitbtcService service;
    public final String EXCHANGE_NAME = ExchangeFacade.HITBTC;

    private String checkConnectionUrl = "http://hitbtc.com/";
    private final String SIGN_HASH_FUNCTION = "HmacSHA512";
    private final String ENCODING = "UTF-8";

    //handle concurrent requests
    private boolean isBusy;

    //Entry points
    private final String API_BASE_URL = "https://api.hitbtc.com";
    private final String API_GET_BALANCES = "/api/1/trading/balance";
    private final String API_GET_ORDERS = "/api/1/trading/orders/active";
    private final String API_GET_TRADES = "/api/1/trading/trades";
    private final String API_ENTERORDER = "/api/1/trading/new_order";
    private final String API_CANCEL_ORDER = "/api/1/trading/cancel_order";
    private final String API_CLEAR_ORDERS = "/api/1/trading/cancel_orders";

    //Errors
    private ErrorManager errors = new ErrorManager();
    private final String TOKEN_ERR = "error";
    private final String TOKEN_BAD_RETURN = "No Connection With Exchange";

    public HitbtcWrapper(ApiKeys keys) {
        this.keys = keys;
        service = new HitbtcService(keys);
        setupErrors();
        this.setFree();
    }

    private void setupErrors() {
        errors.setExchangeName(this.EXCHANGE_NAME);
    }

    private ApiResponse getQuery(String base, String method, HashMap<String, String> query_args, boolean needAuth, boolean isGet) {
        ApiResponse apiResponse = new ApiResponse();
        String queryResult = query(base, method, query_args, needAuth, isGet);

        if (queryResult == null) {
            apiResponse.setError(errors.nullReturnError);
            return apiResponse;
        }
        if (queryResult.equals(TOKEN_BAD_RETURN)) {
            apiResponse.setError(errors.noConnectionError);
            return apiResponse;
        }

        JSONParser parser = new JSONParser();
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(queryResult));
            boolean valid = true;

            //error example : {"code":"NotAuthorized","message":"Wrong signature"}
            if (httpAnswerJson.containsKey("code")) {
                valid = false;
            }

            if (!valid) {
                String errorMessage = (String) httpAnswerJson.get("code") + " :  " + (String) httpAnswerJson.get("message");
                ApiError apiErr = errors.apiReturnError;
                apiErr.setDescription(errorMessage);
                apiResponse.setError(apiErr);
            } else {
                apiResponse.setResponseObject(httpAnswerJson);
            }
        } catch (ClassCastException cce) {
            //if casting to a JSON object failed, try a JSON Array
            try {
                JSONArray httpAnswerJson = (JSONArray) (parser.parse(queryResult));
                apiResponse.setResponseObject(httpAnswerJson);
            } catch (ParseException pe) {
                LOG.error("httpResponse: " + queryResult + " \n" + pe.toString());
                apiResponse.setError(errors.parseError);
            }
        } catch (ParseException ex) {
            LOG.error("httpresponse: " + queryResult + " \n" + ex.toString());
            apiResponse.setError(errors.parseError);
            return apiResponse;
        }
        return apiResponse;

    }


    @Override
    public ApiResponse getAvailableBalances(CurrencyPair pair) {
        return getBalanceImpl(null, pair);
    }

    @Override
    public ApiResponse getAvailableBalance(Currency currency) {
        return getBalanceImpl(currency, null);
    }

    @Override
    public ApiResponse getLastPrice(CurrencyPair pair) {
        Ticker ticker = new Ticker();
        ApiResponse apiResponse = new ApiResponse();

        double last = -1;
        double ask = -1;
        double bid = -1;


        String url = "https://api.hitbtc.com/api/1/public/" + pair.toStringSepSpecial("").toUpperCase() + "/ticker";
        String method = "";
        HashMap<String, String> query_args = new HashMap<>();
        boolean isGet = true;

        LOG.trace("get from " + url);
        LOG.trace("method " + method);
        ApiResponse response = getQuery(url, method, query_args, false, isGet);

        LOG.trace("response " + response);
        if (!response.isPositive()) {
            return response;
        } else {
            /* Sample response
                 *{"ask":"0.006000","bid":"0.002100","last":"0.002502","low":"0.002502","high":"0.560000","open":"0.003000","volume":"8.550","volume_quote":"0.280000","timestamp":1442585862919}
            */
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

            last = Utils.getDouble(httpAnswerJson.get("last"));
            bid = Utils.getDouble(httpAnswerJson.get("bid"));
            ask = Utils.getDouble(httpAnswerJson.get("ask"));

            ticker.setAsk(ask);
            ticker.setBid(bid);
            ticker.setLast(last);
            apiResponse.setResponseObject(ticker);

            return apiResponse;

        }
    }

    private ApiResponse getBalanceImpl(Currency currency, CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String base = API_BASE_URL;
        String method = API_GET_BALANCES;
        boolean isGet = true;
        HashMap<String, String> query_args = new HashMap<>();

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
               /*
                {"balance": [
                      {"currency_code": "BTC","cash": 0.045457701,"reserved": 0.01},
                      {"currency_code": "EUR","cash": 0.0445544,"reserved": 0},
                      {"currency_code": "LTC","cash": 0.7,"reserved": 0.1},
                      {"currency_code": "USD","cash": 2.9415029,"reserved": 1.001}
                 ]
                 }
                 */
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

            JSONArray listBalances = (JSONArray) httpAnswerJson.get("balance");

            if (currency == null) { //Get all balances

                boolean foundNBTavail = false;
                boolean foundPEGavail = false;
                Amount NBTAvail = new Amount(0, pair.getOrderCurrency()),
                        PEGAvail = new Amount(0, pair.getPaymentCurrency());
                Amount PEGonOrder = new Amount(0, pair.getPaymentCurrency());
                Amount NBTonOrder = new Amount(0, pair.getOrderCurrency());
                String NBTcode = pair.getOrderCurrency().getCode().toUpperCase();
                String PEGcode = pair.getPaymentCurrency().getCode().toUpperCase();

                for (int i = 0; i < listBalances.size(); i++) { //Assumes that we have one line per currency (no duplicates)
                    JSONObject tempBalance = (JSONObject) listBalances.get(i);
                    String tempCode = (String) tempBalance.get("currency_code");
                    if (tempCode.equalsIgnoreCase(NBTcode)) {
                        foundNBTavail = true;
                        NBTonOrder.setQuantity(Utils.getDouble(tempBalance.get("reserved")));
                        NBTAvail.setQuantity(Utils.getDouble(tempBalance.get("cash")));
                    } else if (tempCode.equalsIgnoreCase(PEGcode)) {
                        foundPEGavail = true;
                        PEGonOrder.setQuantity(Utils.getDouble(tempBalance.get("reserved")));
                        PEGAvail.setQuantity(Utils.getDouble(tempBalance.get("cash")));
                    }
                }

                PairBalance balance = new PairBalance(PEGAvail, NBTAvail, PEGonOrder, NBTonOrder);
                apiResponse.setResponseObject(balance);
                if (!foundNBTavail || !foundPEGavail) {
                    LOG.info("Cannot find a balance for currency with code "
                            + "" + NBTcode + " or " + PEGcode + " in your balance. "
                            + "NuBot assumes that balance is 0");
                }

            } else {
                //Get specific balance
                boolean found = false;
                Amount avail = new Amount(0, currency);
                String code = currency.getCode().toUpperCase();
                for (int i = 0; i < listBalances.size(); i++) {
                    JSONObject tempBalance = (JSONObject) listBalances.get(i);
                    String tempCode = (String) tempBalance.get("currency_code");
                    if (tempCode.equalsIgnoreCase(code)) {
                        found = true;
                        double balance = Utils.getDouble(tempBalance.get("cash"));
                        avail.setQuantity(balance);
                        break;
                    }
                }
                apiResponse.setResponseObject(avail);

                if (!found) {
                    LOG.warn("Cannot find a balance for currency with code "
                            + code + " in your balance. NuBot assumes that balance is 0");
                }

            }

        } else {
            apiResponse = response;
        }


        return apiResponse;
    }

    @Override
    public ApiResponse sell(CurrencyPair pair, double amount, double rate) {
        return enterOrder(Constant.SELL.toUpperCase(), pair, amount, rate);
    }

    @Override
    public ApiResponse buy(CurrencyPair pair, double amount, double rate) {
        return enterOrder(Constant.BUY.toUpperCase(), pair, amount, rate);
    }

    private ApiResponse enterOrder(String type, CurrencyPair pair, double amount, double rate) {
        ApiResponse apiResponse = new ApiResponse();
        String base = API_BASE_URL;
        String method = API_ENTERORDER;
        boolean isGet = false;

        String clientOrderId = UUID.randomUUID().toString().substring(24);
        String order_id = "";

        //Convert the amount
        amount *= getFactor(pair);
        HashMap<String, String> query_args = new HashMap<>();
        query_args.put("clientOrderId", clientOrderId);
        query_args.put("symbol", pair.toStringSepSpecial("").toUpperCase());
        query_args.put("side", type.toLowerCase());
        query_args.put("price", Utils.formatNumber(rate, getDigits(pair))); //TODO change to default Settings.DEFAULT_PRECISION when they do
        query_args.put("type", "limit");
        query_args.put("quantity", Utils.formatNumber(amount, Settings.DEFAULT_PRECISION)); //TODO change to default precision

        ApiResponse response = getQuery(base, method, query_args, true, isGet);

        if (response.isPositive()) {


            /* succesfull reply :
            {
            "ExecutionReport":
                {
                    "orderId": "58521038",
                    "clientOrderId": "11111112",
                    "execReportType": "new",
                    "orderStatus": "new",
                    "symbol": "BTCUSD",
                    "side": "buy",
                    "timestamp": 1395236779235,
                    "price": 0.1,
                    "quantity": 100,
                    "type": "limit",
                    "timeInForce": "GTC",
                    "lastQuantity": 0,
                    "lastPrice": 0,
                    "leavesQuantity": 100,
                    "cumQuantity": 0,
                    "averagePrice": 0
                }
             }

             Error reply :
                {
                  "ExecutionReport": {
                    "orderId": "N/A",
                    "clientOrderId": "1aaa1bb3e54f",
                    "orderStatus": "rejected",
                    "symbol": "NBTBTC",
                    "side": "buy",
                    "price": "0.0001",
                    "quantity": 0,
                    "type": "limit",
                    "timeInForce": "GTC",
                    "lastQuantity": 0,
                    "lastPrice": "",
                    "leavesQuantity": 0,
                    "cumQuantity": 0,
                    "averagePrice": "0",
                    "execReportType": "rejected",
                    "orderRejectReason": "badQuantity"
                  }
}
             */

            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            JSONObject ExecutionReport = (JSONObject) httpAnswerJson.get("ExecutionReport");
            String orderID = (String) ExecutionReport.get("clientOrderId");

            String checkError = (String) ExecutionReport.get("orderId");
            if (checkError.equals("N/A") || checkError.equals("N\\/A")) {
                String reason = (String) ExecutionReport.get("orderRejectReason");
                apiResponse.setError(new ApiError(errors.genericError.getCode(), reason));
            } else {
                apiResponse.setResponseObject(orderID);
            }
            return apiResponse;
        } else {
            apiResponse = response;
        }

        return apiResponse;
    }


    @Override
    public ApiResponse getActiveOrders() {
        return getOrdersImpl(null, null);
    }


    @Override
    public ApiResponse getActiveOrders(CurrencyPair pair) {
        return getOrdersImpl(pair, null);
    }

    private ApiResponse getOrdersImpl(CurrencyPair pair, String orderID) {
        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Order> orderList = new ArrayList<Order>();

        String base = API_BASE_URL;
        String method = API_GET_ORDERS;

        boolean isGet = true;


        HashMap<String, String> query_args = new HashMap<>();

        if (pair != null) {
            query_args.put("symbols", pair.toStringSepSpecial("").toUpperCase());
        }

        if (orderID != null) {
            //If orderID is set, then we are looking for a specific order instead that a orderList
            query_args.put("clientOrderId", orderID);
        }

        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            JSONArray ordersArray = (JSONArray) httpAnswerJson.get("orders");

            if (orderID == null) {
                for (int i = 0; i < ordersArray.size(); i++) {
                    JSONObject orderObject = (JSONObject) ordersArray.get(i);
                    Order tempOrder = parseOrder(orderObject);
                    orderList.add(tempOrder);
                }

                apiResponse.setResponseObject(orderList);
                return apiResponse;
            } else {
                //Looking for a specific order
                if (ordersArray.size() == 1) {
                    Order tempOrder = parseOrder((JSONObject) ordersArray.get(0));
                    apiResponse.setResponseObject(tempOrder);
                } else if (ordersArray.size() == 0) {
                    //Order not found
                    apiResponse.setError(errors.orderNotFound);
                } else {
                    apiResponse.setError(new ApiError(errors.genericError.getCode(), "Was expecting 1 order in the list but found " + ordersArray.size()));
                }
                return apiResponse;

            }
        } else

        {
            apiResponse = response;
        }

        return apiResponse;
    }

    private Order parseOrder(JSONObject orderObject) {

        /* succesfull reply :
            {
            "orders": [
                    {
                    "orderId": "51521638",
                    "orderStatus": "new",
                    "lastTimestamp": 1394798401494,
                    "orderPrice": 1000,
                    "orderQuantity": 1,
                    "avgPrice": 0,
                    "quantityLeaves": 1,
                    "type": "limit",
                    "timeInForce": "GTC",
                    "cumQuantity": 0,
                    "clientOrderId": "7fb8756ec8045847c3b840e84d43bd83",
                    "symbol": "LTCBTC",
                    "side": "sell",
                    "execQuantity": 0
                    }
                ]
            }
            */
        Order order = new Order();
        String symbol = ((String) orderObject.get("symbol"));
        symbol = symbol.substring(0, 3) + "_" + symbol.substring(3);
        CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(symbol);
        order.setType(((String) orderObject.get("side")).toUpperCase());
        order.setId((String) orderObject.get("clientOrderId"));

        order.setAmount(new Amount(Utils.round(Utils.getDouble(orderObject.get("quantityLeaves")) / getFactor(pair)), pair.getOrderCurrency()));
        order.setPrice(new Amount(Utils.getDouble(orderObject.get("orderPrice")), pair.getPaymentCurrency()));

        order.setCompleted(false);
        order.setPair(pair);
        try {
            order.setInsertedDate(new Date((long) (orderObject.get("lastTimestamp"))));
        } catch (NullPointerException e) {
            order.setInsertedDate(new Date(System.currentTimeMillis()));
        }


        order.setPair(CurrencyPair.getCurrencyPairFromString(symbol));
        return order;
    }


    private Trade parseTrade(JSONObject tradeObject) {

        /* succesfull reply :
             {
                    "tradeId": 39,
                    "execPrice": 150,
                    "timestamp": 1395231854030,
                    "originalOrderId": "114",
                    "fee": 0.03,
                    "clientOrderId": "FTO18jd4ou41--25",
                    "symbol": "BTCUSD",
                    "side": "sell",
                    "execQuantity": 10
                    }
            }
            */
        Trade trade = new Trade();

        trade.setId(Long.toString((Long) tradeObject.get("tradeId")));
        trade.setOrder_id((String) tradeObject.get("clientOrderId"));
        trade.setExchangeName(ExchangeFacade.HITBTC);

        String symbol = ((String) tradeObject.get("symbol"));
        symbol = symbol.substring(0, 3) + "_" + symbol.substring(3);
        CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(symbol);
        trade.setPair(pair);


        trade.setType(((String) tradeObject.get("side")).toUpperCase());

        trade.setAmount(new Amount(Utils.round(Utils.getDouble(tradeObject.get("execQuantity")) / getFactor(pair)), pair.getOrderCurrency()));
        trade.setPrice(new Amount(Utils.getDouble(tradeObject.get("execPrice")), pair.getPaymentCurrency()));
        trade.setFee(new Amount(Utils.getDouble(tradeObject.get("fee")), pair.getPaymentCurrency()));


        trade.setDate(new Date((long) (tradeObject.get("timestamp"))));

        return trade;
    }

    @Override
    public ApiResponse getOrderDetail(String orderID) {
        return getOrdersImpl(null, orderID);
    }

    @Override
    public ApiResponse cancelOrder(String orderID, CurrencyPair pair) {

        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Order> orderList = new ArrayList<Order>();

        String base = API_BASE_URL;
        String method = API_CANCEL_ORDER;

        boolean isGet = false;

        String cancelRequestClientOrderId = UUID.randomUUID().toString().substring(24);
        String order_id = "";

        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("clientOrderId", orderID);
        query_args.put("cancelRequestClientOrderId", cancelRequestClientOrderId);
        query_args.put("symbol", pair.toStringSepSpecial("").toUpperCase());


        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            /* GOOD execution
            "ExecutionReport":
            {
                "orderId": "58521038",
                "clientOrderId": "11111112",
                "execReportType": "canceled",
                "orderStatus": "canceled",
                "symbol": "BTCUSD",
                "side": "buy",
                "timestamp": 1395236779346,
                "price": 0.1,
                "quantity": 100,
                "type": "limit",
                "timeInForce": "GTC",
                "lastQuantity": 0,
                "lastPrice": 0,
                "leavesQuantity": 0,
                "cumQuantity": 0,
                "averagePrice": 0
            }

            BAD EXECUTION :

           "CancelReject": {
                "cancelRequestClientOrderId": "011111112",
                "clientOrderId": "11111112",
                "rejectReasonCode": "orderNotFound"
            }
             */

            boolean valid = httpAnswerJson.containsKey("CancelReject") ? false : true;
            if (valid) {
                apiResponse.setResponseObject(true);
            } else {
                JSONObject CancelReject = (JSONObject) httpAnswerJson.get("CancelReject");
                apiResponse.setError(new ApiError(errors.genericError.getCode(), (String) CancelReject.get("rejectReasonCode")));
            }
        } else {
            apiResponse = response;
        }

        return apiResponse;

    }

    @Override
    public ApiResponse getTxFee() {
        return new ApiResponse(true, 0.1, null);
    }

    @Override
    public ApiResponse getTxFee(CurrencyPair pair) {
        LOG.debug("Hitbtc uses global TX fee, currency pair not supprted. \n" + "now calling getTxFee()");
        return getTxFee();
    }

    @Override
    public ApiResponse getLastTrades(CurrencyPair pair) {
        return getTradesImpl(pair, 0);
    }


    @Override
    public ApiResponse getLastTrades(CurrencyPair pair, long startTime) {
        return getTradesImpl(pair, startTime);
    }

    private ApiResponse getTradesImpl(CurrencyPair pair, long startTime) {

        LOG.warn("Maximum number of trades retrieved is 1000 due to technical limitation of HitBtc");
        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Trade> tradeList = new ArrayList<Trade>();

        String base = API_BASE_URL;
        String method = API_GET_TRADES;

        boolean isGet = true;

        String from = Long.toString(startTime);
        if (startTime == 0) {
            long now = System.currentTimeMillis();
            long yesterday = Math.round((now - TimeUtils.getOneDayInMillis()) / 1000);
            from = Long.toString(yesterday); //24hours
        }

        HashMap<String, String> query_args = new HashMap<>();


        query_args.put("by", "ts");
        query_args.put("start_index", "0");
        query_args.put("max_results", "1000");
        query_args.put("symbols", pair.toStringSepSpecial("").toUpperCase());
        query_args.put("from", from);


        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            /*
            {
            "trades": [
                    {
                    "tradeId": 39,
                    "execPrice": 150,
                    "timestamp": 1395231854030,
                    "originalOrderId": "114",
                    "fee": 0.03,
                    "clientOrderId": "FTO18jd4ou41--25",
                    "symbol": "BTCUSD",
                    "side": "sell",
                    "execQuantity": 10
                    },
                   ...
                    }
                  ]
                }
             */


            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            JSONArray tradesArray = (JSONArray) httpAnswerJson.get("trades");
            for (int i = 0; i < tradesArray.size(); i++) {
                JSONObject tradeObject = (JSONObject) tradesArray.get(i);
                Trade tempTrade = parseTrade(tradeObject);
                tradeList.add(tempTrade);
            }

            apiResponse.setResponseObject(tradeList);
            return apiResponse;
        } else {
            apiResponse = response;
        }

        return apiResponse;

    }

    @Override
    public ApiResponse isOrderActive(String id) {
        ApiResponse existResponse = new ApiResponse();

        ApiResponse orderDetailResponse = getOrderDetail(id);
        if (orderDetailResponse.isPositive()) {
            Order order = (Order) orderDetailResponse.getResponseObject();
            existResponse.setResponseObject(true);
        } else {
            ApiError err = orderDetailResponse.getError();
            if (err.getDescription().contains("Order not found")) {
                existResponse.setResponseObject(false);
            } else {
                existResponse.setError(err);
            }
        }
        return existResponse;
    }

    @Override
    public ApiResponse getOrderBook(CurrencyPair pair) {

        ApiResponse apiResponse = new ApiResponse();
        boolean isGet = true;

        String url = "https://api.hitbtc.com/api/1/public/" + pair.toStringSepSpecial("").toUpperCase() + "/orderbook";
        String method = "";
        HashMap<String, String> query_args = new HashMap<>();


        ApiResponse response = getQuery(url, method, query_args, true, isGet);
        if (response.isPositive()) {

            /* succesfull reply :
           {"asks":[["0.009999","0.100"],["0.010000","91.936"],["0.011000","0.021"]],"bids":[["0.004000","4.522"],["0.002002","0.001"],["0.002001","34.959"],["0.002000","50.000"]]}
             */

            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            ArrayList<Order> orderBook = new ArrayList<>();
            JSONArray asks = (JSONArray) httpAnswerJson.get("asks");
            JSONArray bids = (JSONArray) httpAnswerJson.get("bids");

            for (Iterator<JSONArray> order = asks.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.SELL, pair));
            }
            for (Iterator<JSONArray> order = bids.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.BUY, pair));
            }
            apiResponse.setResponseObject(new OrderBook(this.EXCHANGE_NAME, pair, orderBook));

            return apiResponse;
        } else {
            apiResponse = response;
        }

        return apiResponse;

    }

    private Order parseOrderSimple(JSONArray in, String type, CurrencyPair pair) {
        Order out = new Order();

        out.setType(type);
        out.setPair(pair);

        Amount amount = new Amount(Utils.getDouble(in.get(1)), pair.getOrderCurrency());
        out.setAmount(amount);
        Amount price = new Amount(Utils.getDouble(in.get(0)), pair.getPaymentCurrency());
        out.setPrice(price);

        out.setCompleted(false);

        return out;
    }

    @Override
    public ApiResponse placeOrders(OrderBatch batch, CurrencyPair pair) {
        return TradeUtils.placeMultipleOrdersSequentiallyImplementation(batch, pair, TradeUtils.INTERVAL_MID, this);

    }

    @Override
    public ApiResponse placeOrdersParallel(OrderBatch batch, CurrencyPair pair, ArrayList<ApiKeys> keys) {
        return TradeUtils.placeMultipleOrdersParallelImplementation(batch, pair, keys, this);
    }

    @Override
    public ApiResponse clearOrders(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String base = API_BASE_URL;
        String method = API_CLEAR_ORDERS;

        boolean isGet = false;

        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("symbols", pair.toStringSepSpecial("").toUpperCase());


        ApiResponse response = getQuery(base, method, query_args, true, isGet);
        if (response.isPositive()) {
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();
            JSONArray ExecutionReport = (JSONArray) httpAnswerJson.get("ExecutionReport");
            LOG.debug(ExecutionReport.toJSONString());
            apiResponse.setResponseObject(true);
            return apiResponse;
        } else {
            apiResponse = response;
        }

        return apiResponse;

    }


    private int getDigits(CurrencyPair pair) {
        //reads the number of digits allowed in setting the prices of orders
        if (!this.digitsFetched) {
            try {
                fetchDigits(pair); //will update the this.digits content
                return this.digits;
            } catch (Exception e) {
                LOG.error("Critical error: the bot is unable to get the step via API " +
                        "(" + e.toString() + "). Forcing termination of  NuBot the operation : please try again later or contact developers if problem persist.");
                SessionManager.tryToStopBot();
            }
        } else { //no need to fetch
            return this.digits;
        }
        LOG.error("Something is wrong, bot should never execute this statement.");
        return Settings.DEFAULT_PRECISION;//reached only in exceptional cases
    }


    private double getFactor(CurrencyPair pair) {
        //reads the factor to multiply quantities as defined in the documentation
        if (this.factor == 0) {
            try {
                fetchFactor(pair); //will update the this.factor content
                return this.factor;
            } catch (Exception e) {
                LOG.error("Critical error: the bot is unable to get the multiplication factor via API " +
                        "(" + e.toString() + "). Forcing termination of  NuBot the operation : please try again later or contact developers if problem persist.");
                SessionManager.tryToStopBot();
            }
        } else { //no need to fetch
            return factor;
        }
        return 0;//reached only in exceptional cases
    }


    private void fetchDigits(CurrencyPair pair) throws Exception {
        String url = "https://api.hitbtc.com/api/1/public/symbols";
        String method = "";
        HashMap<String, String> query_args = new HashMap<>();
        boolean isGet = true;

        LOG.trace("get from " + url);
        LOG.trace("method " + method);
        ApiResponse response = getQuery(url, method, query_args, false, isGet);

        LOG.trace("response " + response);
        if (!response.isPositive()) {
            throw new Exception("Cannot fetch digits :" + response.getError().toString());
        } else {
            /* Sample response
          {"symbols":[
              {"symbol":"BCNBTC","step":"0.000000001","lot":"100","currency":"BTC","commodity":"BCN","takeLiquidityRate":"0.001","provideLiquidityRate":"-0.0001"},
              {"symbol":"BTCEUR","step":"0.01","lot":"0.01","currency":"EUR","commodity":"BTC","takeLiquidityRate":"0.001","provideLiquidityRate":"-0.0001"},        JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

          */
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

            JSONArray symbolArray = (JSONArray) httpAnswerJson.get("symbols");
            boolean found = false;
            String seekSymbol = pair.toStringSepSpecial("").toUpperCase();

            for (int i = 0; i < symbolArray.size(); i++) {
                JSONObject tempObj = (JSONObject) symbolArray.get(i);
                String tempSymbol = (String) (tempObj).get("symbol");
                if (tempSymbol.equalsIgnoreCase(seekSymbol)) {
                    String step = (String) tempObj.get("step");

                    //Compute number of decimals
                    int integerPlaces = step.indexOf('.');
                    int decimalPlaces = step.length() - integerPlaces - 1;

                    found = true;
                    this.digitsFetched = true;
                    this.digits = decimalPlaces; //Assignn
                    LOG.debug("Fetched HitBtc digits = " + digits + " (step=" + step + ")");

                }
            }
            if (!found) {
                throw new Exception("Cannot fetch digits : symbol " + seekSymbol + " not found");
            }
        }
    }

    private void fetchFactor(CurrencyPair pair) throws Exception {
        String url = "https://api.hitbtc.com/api/1/public/symbols";
        String method = "";
        HashMap<String, String> query_args = new HashMap<>();
        boolean isGet = true;

        LOG.trace("get from " + url);
        LOG.trace("method " + method);
        ApiResponse response = getQuery(url, method, query_args, false, isGet);

        LOG.trace("response " + response);
        if (!response.isPositive()) {
            throw new Exception("Cannot fetch factor :" + response.getError().toString());
        } else {
            /* Sample response
          {"symbols":[
              {"symbol":"BCNBTC","step":"0.000000001","lot":"100","currency":"BTC","commodity":"BCN","takeLiquidityRate":"0.001","provideLiquidityRate":"-0.0001"},
              {"symbol":"BTCEUR","step":"0.01","lot":"0.01","currency":"EUR","commodity":"BTC","takeLiquidityRate":"0.001","provideLiquidityRate":"-0.0001"},        JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

          */
            JSONObject httpAnswerJson = (JSONObject) response.getResponseObject();

            JSONArray symbolArray = (JSONArray) httpAnswerJson.get("symbols");
            boolean found = false;
            String seekSymbol = pair.toStringSepSpecial("").toUpperCase();

            for (int i = 0; i < symbolArray.size(); i++) {
                JSONObject tempObj = (JSONObject) symbolArray.get(i);
                String tempSymbol = (String) (tempObj).get("symbol");
                if (tempSymbol.equalsIgnoreCase(seekSymbol)) {
                    double lot = Utils.getDouble(tempObj.get("lot"));
                    double factor = Utils.round(1 / lot);
                    found = true;
                    LOG.debug("Fetched HitBtc factor = " + factor + " (lot=" + lot + ")");
                    this.factor = factor; //Assignn
                }
            }
            if (!found) {
                throw new Exception("Cannot fetch factor : symbol " + seekSymbol + " not found");
            }
        }
    }


    @Override
    public ApiError getErrorByCode(int code) {
        throw new UnsupportedOperationException("Not active");
    }

    @Override
    public String getUrlConnectionCheck() {
        return checkConnectionUrl;
    }

    @Override
    public String query(String base, String method, AbstractMap<String, String> args, boolean needAuth,
                        boolean isGet) {
        String queryResult = TOKEN_BAD_RETURN; //Will return this string in case it fails
        if (this.isFree()) {
            this.setBusy();
            queryResult = service.executeQuery(base, method, args, needAuth, isGet);
            this.setFree();
        } else {
            //Another thread is probably executing a query. Init the retry procedure
            long sleeptime = Settings.RETRY_SLEEP_INCREMENT * 1;
            int counter = 0;
            long startTimeStamp = System.currentTimeMillis();
            LOG.debug(method + " blocked, another call is being processed ");
            boolean exit = false;
            do {
                counter++;
                sleeptime = counter * Settings.RETRY_SLEEP_INCREMENT; //Increase sleep time
                sleeptime += (int) (Math.random() * 200) - 100;// Add +- 100 ms random to facilitate competition
                LOG.debug("Retrying for the " + counter + " time. Sleep for " + sleeptime + "; Method=" + method);
                try {
                    Thread.sleep(sleeptime);
                } catch (InterruptedException e) {
                    LOG.error(e.toString());
                }

                //Try executing the call
                if (this.isFree()) {
                    LOG.debug("Finally the exchange is free, executing query after " + counter + " attempt. Method=" + method);
                    this.setBusy();
                    queryResult = service.executeQuery(base, method, args, needAuth, isGet);
                    this.setFree();
                    break; //Exit loop
                } else {
                    LOG.debug("Exchange still busy : " + counter + " .Will retry soon; Method=" + method);
                    exit = false;
                }
                if (System.currentTimeMillis() - startTimeStamp >= Settings.TIMEOUT_QUERY_RETRY) {
                    exit = true;
                    LOG.error("Method=" + method + " failed too many times and timed out. attempts = " + counter);
                }
            } while (!exit);
        }

        return queryResult;
    }

    @Override
    public SignedRequest getOpenOrdersRequest(CurrencyPair pair) {

        String nonce = Long.toString(System.currentTimeMillis());

        //Create the vocabulary
        HashMap<String, String> query_args = new HashMap<>();
        query_args.put("symbols", pair.toStringSepSpecial("").toUpperCase());
        query_args.put("nonce", nonce);
        query_args.put("apikey", keys.getApiKey());
        query_args.put("method", API_GET_ORDERS);

        String baseQuery = "nonce=" + nonce + "&apikey=" + keys.getApiKey();

        String to_sign = API_GET_ORDERS + "?" + baseQuery + "&symbols=" + pair.toStringSepSpecial("").toUpperCase();

        //Create the signature
        String signature = TradeUtils.signRequest(keys.getPrivateKey(), to_sign, SIGN_HASH_FUNCTION, ENCODING);

        return new SignedRequest(query_args, signature);
    }

    @Override
    public void setKeys(ApiKeys keys) {
        this.keys = keys;
    }

    @Override
    public void setApiBaseUrl(String apiBaseUrl) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isBusy() {
        return isBusy;
    }

    @Override
    public boolean isFree() {
        return !isBusy;
    }

    @Override
    public void setBusy() {
        this.isBusy = true;
    }

    @Override
    public void setFree() {
        this.isBusy = false;
    }


    private class HitbtcService implements ServiceInterface {

        protected ApiKeys keys;

        private HitbtcService(ApiKeys keys) {
            this.keys = keys;
        }

        @Override
        public String executeQuery(String base, String method, AbstractMap<String, String> args, boolean needAuth, boolean isGet) {
            String answer = null;
            String signature = "";
            String argsString = "";
            String url = base + method;

            List<NameValuePair> postData = new ArrayList<NameValuePair>();

            String nonce = Long.toString(System.currentTimeMillis());

            String baseQuery = "nonce=" + nonce + "&apikey=" + keys.getApiKey();
            url += "?" + baseQuery;


            for (Iterator<Map.Entry<String, String>> argumentIterator = args.entrySet().iterator(); argumentIterator.hasNext(); ) {

                Map.Entry<String, String> argument = argumentIterator.next();

                postData.add(new BasicNameValuePair(argument.getKey().toString(), argument.getValue().toString()));

                if (argsString.length() > 0) {
                    argsString += "&";
                }

                argsString += argument.getKey() + "=" + argument.getValue();

            }

            String to_sign = "";
            if (isGet) {
                url = argsString.length() > 0 ? url + "&" + argsString : url;
                to_sign = argsString.length() > 0 ? method + "?" + baseQuery + "&" + argsString : method + "?" + baseQuery;
            } else {
                to_sign = method + "?" + baseQuery + argsString;
            }


            LOG.trace("url = " + url);
            LOG.trace("baseQuery = " + baseQuery);
            LOG.trace("argsString = " + argsString);
            LOG.trace("toSign = " + to_sign);

            signature = TradeUtils.signRequest(keys.getPrivateKey(), to_sign, SIGN_HASH_FUNCTION, ENCODING);

            // add header
            Header[] headers = new Header[1];
            headers[0] = new BasicHeader("X-Signature", signature);

            URL queryUrl;
            try {
                queryUrl = new URL(url);
            } catch (MalformedURLException ex) {
                LOG.error(ex.toString());
                return null;
            }
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = null;
            HttpGet get = null;
            HttpResponse response = null;

            try {
                if (!isGet) {
                    post = new HttpPost(url);
                    post.setEntity(new UrlEncodedFormEntity(postData));
                    post.setHeaders(headers);
                    response = client.execute(post);
                } else {
                    get = new HttpGet(url);
                    get.setHeaders(headers);
                    response = client.execute(get);
                }
            } catch (NoRouteToHostException e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            } catch (SocketException e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            } catch (Exception e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            }
            BufferedReader rd;


            try {
                rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));


                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    buffer.append(line);
                }

                answer = buffer.toString();
            } catch (IOException ex) {

                LOG.error(ex.toString());
                return null;
            } catch (IllegalStateException ex) {

                LOG.error(ex.toString());
                return null;
            }


            LOG.trace("\nSending request to URL : " + url + " ; get = " + isGet);
            LOG.trace("Response Code : " + response.getStatusLine().getStatusCode());
            LOG.trace("Response :" + response);


            return answer;
        }
    }
}
