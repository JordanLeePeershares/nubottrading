/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.trading.wrappers;

import com.nubits.nubot.ALPClient.ErrorGeneratingALPRequestException;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.models.Currency;
import com.nubits.nubot.trading.*;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.utils.Utils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SouthxchangeWrapper implements TradeInterface {
    private static final Logger LOG = LoggerFactory.getLogger(SouthxchangeWrapper.class.getName());

    //Class fields
    private ApiKeys keys;
    protected SouthxchangeService service;
    public final String EXCHANGE_NAME = ExchangeFacade.SOUTHXCHANGE;
    private final int TIME_OUT = 15000;
    private String apiBaseUrl;
    private String checkConnectionUrl = "https://www.southxchange.com/";
    private final String SIGN_HASH_FUNCTION = "HmacSHA512";
    private final String ENCODING = "UTF-8";


    //handle concurrent requests
    private boolean isBusy;

    //Entry Points
    private final String API_BASE_URL = "https://www.southxchange.com/api/";
    private final String API_LISTBALANCES = "listBalances";
    private final String API_ENTERORDER = "placeOrder";
    private final String API_CANCELORDER = "cancelOrder";
    private final String API_CLEARORDERS = "cancelMarketOrders";

    private final String API_LISTORDERS = "listOrders";


    //Tokens
    private final String TOKEN_BAD_RETURN = "No Connection With Exchange";


    //Pool queue
    protected long lastRequest = 0;
    //Errors
    ErrorManager errors = new ErrorManager();

    public SouthxchangeWrapper(ApiKeys keys) {
        this.keys = keys;
        service = new SouthxchangeService(keys);
        setupErrors();
        this.setFree();
    }

    private void setupErrors() {
        errors.setExchangeName(this.EXCHANGE_NAME);
    }


    @Override
    public ApiResponse getAvailableBalances(CurrencyPair pair) {
        return getBalanceImpl(pair, null);

    }

    @Override
    public ApiResponse getAvailableBalance(Currency currency) {
        return getBalanceImpl(null, currency);

    }

    private ApiResponse getBalanceImpl(CurrencyPair pair, Currency currency) {
        ApiResponse apiResponse = new ApiResponse();

        String method = API_LISTBALANCES;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();
        ApiResponse response = getQuery(API_BASE_URL, method, query_args, true, isGet);

        if (response.isPositive()) {
            /*
            [{"Currency":"BTC","Deposited":0.08447049,"Available":0.08447049,"Unconfirmed":0.0},{"Currency":"NBT","Deposited":11.99800000,"Available":11.99800000,"Unconfirmed":0.0}]
             */
            JSONArray balanceArrayJSON = (JSONArray) response.getResponseObject();

            if (currency == null) { //Get all balances
                boolean foundNBT = false;
                boolean foundPEG = false;

                Currency pegCurrency;
                try {
                    pegCurrency = Currency.getCurrencyToTrack(pair);
                } catch (CurrencyToTrackNotAvailableException e) {
                    apiResponse.setError(new ApiError(errors.genericError.getCode(), e.getMessage()));
                    return apiResponse;
                }

                Amount NBTAvail = new Amount(0, CurrencyList.NBT),
                        PEGAvail = new Amount(0, pegCurrency);

                Amount PEGonOrder = new Amount(0, pegCurrency);
                Amount NBTonOrder = new Amount(0, CurrencyList.NBT);
                String NBTcode = "NBT";
                String PEGcode = pegCurrency.getCode().toUpperCase();


                //Iterate on available balances
                for (int i = 0; i < balanceArrayJSON.size(); i++) {
                    JSONObject tempObj = (JSONObject) balanceArrayJSON.get(i);
                    String tempCurrencyCode = (String) tempObj.get("Currency");
                    double tempAvail = Utils.getDouble(tempObj.get("Available"));
                    double tempDeposited = Utils.getDouble(tempObj.get("Deposited"));

                    if (tempCurrencyCode.equalsIgnoreCase(NBTcode)) {
                        foundNBT = true;
                        NBTAvail.setQuantity(tempAvail);
                        NBTonOrder.setQuantity(tempDeposited - tempAvail);
                    } else if (tempCurrencyCode.equalsIgnoreCase(PEGcode)) {
                        foundPEG = true;
                        PEGAvail.setQuantity(tempAvail);
                        PEGonOrder.setQuantity(tempDeposited - tempAvail);
                    }
                    if (foundNBT && foundPEG) break;
                }
                PairBalance balance = new PairBalance(PEGAvail, NBTAvail, PEGonOrder, NBTonOrder);
                apiResponse.setResponseObject(balance);

                if (!(foundNBT && foundPEG)) {
                    LOG.info("Cannot find a balance for currency with code "
                            + "" + NBTcode + " or " + PEGcode + " in your balance. "
                            + "NuBot assumes that balance is 0");

                }


            } else { //Get specific balance
                boolean found = false;
                Amount avail = new Amount(0, currency);
                String code = currency.getCode().toUpperCase();
                for (int i = 0; i < balanceArrayJSON.size(); i++) {
                    JSONObject tempObj = (JSONObject) balanceArrayJSON.get(i);
                    String tempCurrencyCode = (String) tempObj.get("Currency");
                    double tempAvail = Utils.getDouble(tempObj.get("Available"));
                    if (tempCurrencyCode.equalsIgnoreCase(code)) {
                        found = true;
                        avail.setQuantity(tempAvail);
                        break;
                    }
                }
                if (found) {
                    apiResponse.setResponseObject(avail);
                } else {
                    LOG.warn("Cannot find a balance for currency with code "
                            + code + " in your balance. NuBot assumes that balance is 0");
                    avail.setQuantity(0);
                    apiResponse.setResponseObject(avail);
                }
            }
        } else {
            apiResponse = response;
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getLastPrice(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        double last = -1;
        double ask = -1;
        double bid = -1;

        String tickerURL = getTickerPath(pair);

        try {
            String resp = Utils.getHTML(tickerURL, false);
            JSONParser parser = new JSONParser();
            JSONObject responseJSON = (JSONObject) parser.parse(resp);

            last = Utils.getDouble(responseJSON.get("Last"));
            ask = Utils.getDouble(responseJSON.get("Ask"));
            bid = Utils.getDouble(responseJSON.get("Bid"));

            Ticker t = new Ticker(last, ask, bid);

            apiResponse.setResponseObject(t);

        } catch (ClassCastException e) {
            apiResponse.setError(new ApiError(-1, "Cannot find a valid ticker for pair " + pair.toString()));
        } catch (IOException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        } catch (ParseException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        }

        return apiResponse;
    }

    private String getTickerPath(CurrencyPair pair) {
        return API_BASE_URL + "price/" + pair.getOrderCurrency().getCode().toUpperCase() + "/" + pair.getPaymentCurrency().getCode().toUpperCase();
    }

    private String getBookPath(CurrencyPair pair) {
        return API_BASE_URL + "book/" + pair.getOrderCurrency().getCode().toUpperCase() + "/" + pair.getPaymentCurrency().getCode().toUpperCase();
    }

    @Override
    public ApiResponse sell(CurrencyPair pair, double amount, double rate) {
        return enterOrderImpl(Constant.SELL, pair, amount, rate);
    }

    @Override
    public ApiResponse buy(CurrencyPair pair, double amount, double rate) {
        return enterOrderImpl(Constant.BUY, pair, amount, rate);
    }

    private ApiResponse enterOrderImpl(String type, CurrencyPair pair, double amount, double rate) {
        ApiResponse apiResponse = new ApiResponse();

        String method = API_ENTERORDER;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("type", type.toLowerCase());
        query_args.put("limitPrice", Utils.formatNumber(rate, Settings.DEFAULT_PRECISION));
        query_args.put("amount", Utils.formatNumber(amount, Settings.DEFAULT_PRECISION));
        query_args.put("listingCurrency", pair.getOrderCurrency().getCode().toUpperCase());
        query_args.put("referenceCurrency", pair.getPaymentCurrency().getCode().toUpperCase());


        ApiResponse response = getQuery(API_BASE_URL, method, query_args, true, isGet);

        if (response.isPositive()) {
            /*
            orderid
             */
            String orderID = (String) response.getResponseObject();
            apiResponse.setResponseObject(orderID.replaceAll("\"", "")); //Remove quotes

        } else {
            LOG.error("Can't place order " + type + " " + amount + " @ " + rate);

            apiResponse = response;
        }

        return apiResponse;
    }

    @Override
    public ApiResponse getActiveOrders() {
        return getOrdersImpl(null);
    }

    @Override
    public ApiResponse getActiveOrders(CurrencyPair pair) {
        return getOrdersImpl(pair);
    }

    private ApiResponse getOrdersImpl(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();
        ArrayList<Order> orderList = new ArrayList<Order>();

        String method = API_LISTORDERS;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        ApiResponse response = getQuery(API_BASE_URL, method, query_args, true, isGet);

        if (response.isPositive()) {
            /*
            [] (no open orders)

             */
            JSONArray ordersArray = (JSONArray) response.getResponseObject();
            for (int i = 0; i < ordersArray.size(); i++) {
                JSONObject orderObject = (JSONObject) ordersArray.get(i);
                Order tempOrder = parseOrder(orderObject, pair);
                if (tempOrder != null)
                    orderList.add(tempOrder);
            }

            apiResponse.setResponseObject(orderList);
        } else {
            apiResponse = response;
        }

        return apiResponse;

    }

    private Order parseOrder(JSONObject orderObject, CurrencyPair pairSeeking) {

           /*
            {
                "type"
                "LimitPrice"
                "ListingCurrency"
                "Amount"
                "Code"
                "OriginalAmount"
                "ReferenceCurrency"
              }
            */

        Order order = new Order();

        String listingCurrencyCode = ((String) orderObject.get("ListingCurrency")).toUpperCase(); //orderCurrency
        String referenceCurrencyCode = ((String) orderObject.get("ReferenceCurrency")).toUpperCase(); //paymentCurrency

        CurrencyPair tempPair = CurrencyPair.getCurrencyPairFromString(listingCurrencyCode + "_" + referenceCurrencyCode);

        boolean samePair = false;
        if (pairSeeking != null) {
            if (pairSeeking.equals(tempPair)) {
                samePair = true;
            }
        }

        if (samePair || pairSeeking == null) {
            order.setType(((String) orderObject.get("Type")).toUpperCase());
            order.setId((String) orderObject.get("Code"));


            order.setAmount(new Amount(Utils.getDouble(orderObject.get("Amount")), tempPair.getOrderCurrency()));
            order.setPrice(new Amount(Utils.getDouble(orderObject.get("LimitPrice")), tempPair.getPaymentCurrency()));

            order.setCompleted(false);
            order.setPair(tempPair);
            order.setInsertedDate(new Date());//info not provided

            return order;
        } else {
            return null;
        }


    }

    @Override
    public ApiResponse getOrderDetail(String orderID) {
        //Since there is no API entry point for that, this call will iterate over actie
        ApiResponse toReturn = new ApiResponse();

        ApiResponse activeOrdersResponse = getActiveOrders();
        boolean found = false;
        if (activeOrdersResponse.isPositive()) {
            ArrayList<Order> orderList = (ArrayList<Order>) activeOrdersResponse.getResponseObject();
            for (int i = 0; i < orderList.size(); i++) {
                Order tempOrder = orderList.get(i);
                if (tempOrder.getId().equals(orderID)) {
                    found = true;
                    toReturn.setResponseObject(tempOrder);
                    break;
                }
            }
            if (!found) {
                toReturn.setError(errors.orderNotFound);
            }
        } else {
            LOG.error(activeOrdersResponse.getError().toString());
            toReturn.setError(activeOrdersResponse.getError());
            return toReturn;
        }
        return toReturn;
    }

    @Override
    public ApiResponse cancelOrder(String orderID, CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String method = API_CANCELORDER;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("orderCode", orderID);
        ApiResponse response = getQuery(API_BASE_URL, method, query_args, true, isGet);

        if (response.isPositive()) {
            /*
            empty string "" === OK!
             */
            String resp = (String) response.getResponseObject();
            if (resp.equals("")) {
                apiResponse.setResponseObject(true);
            }

        } else {
            LOG.error("Can't delete order " + orderID);
            apiResponse = response;
        }

        return apiResponse;

    }

    @Override
    public ApiResponse getTxFee() {
        return new ApiResponse(true, new Double(0.2), null);

    }

    @Override
    public ApiResponse getTxFee(CurrencyPair pair) {
        return new ApiResponse(true, new Double(0.2), null);
    }

    @Override
    public ApiResponse getLastTrades(CurrencyPair pair) {
        throw new UnsupportedOperationException(EXCHANGE_NAME + " doesn't support an entrypoint to get trade history. Submit a ticket to their support to see that happening faster.");

    }

    @Override
    public ApiResponse getLastTrades(CurrencyPair pair, long startTime) {
        throw new UnsupportedOperationException(EXCHANGE_NAME + " doesn't support an entrypoint to get trade history. Submit a ticket to their support to see that happening faster.");

    }

    @Override
    public ApiResponse isOrderActive(String orderID) {
        //Since there is no API entry point for that, this call will iterate over actie
        ApiResponse toReturn = new ApiResponse();

        ApiResponse activeOrdersResponse = getActiveOrders();
        boolean found = false;
        if (activeOrdersResponse.isPositive()) {
            ArrayList<Order> orderList = (ArrayList<Order>) activeOrdersResponse.getResponseObject();
            for (int i = 0; i < orderList.size(); i++) {
                Order tempOrder = orderList.get(i);
                if (tempOrder.getId().equals(orderID)) {
                    found = true;
                    toReturn.setResponseObject(true);
                    break;
                }
            }
            if (!found) {
                toReturn.setResponseObject(false);
            }
        } else {
            LOG.error(activeOrdersResponse.getError().toString());
            toReturn.setError(activeOrdersResponse.getError());
            return toReturn;
        }
        return toReturn;
    }

    @Override
    public ApiResponse getOrderBook(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String tickerURL = getBookPath(pair);

        try {
            String resp = Utils.getHTML(tickerURL, false);
            JSONParser parser = new JSONParser();
            JSONObject returnJSON = (JSONObject) parser.parse(resp);

            ArrayList<Order> orderBook = new ArrayList<>();


            JSONArray asks = (JSONArray) returnJSON.get("SellOrders");
            JSONArray bids = (JSONArray) returnJSON.get("BuyOrders");

            for (Iterator<JSONObject> order = asks.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.SELL, pair));
            }
            for (Iterator<JSONObject> order = bids.iterator(); order.hasNext(); ) {
                orderBook.add(parseOrderSimple(order.next(), Constant.BUY, pair));
            }
            apiResponse.setResponseObject(new OrderBook(this.EXCHANGE_NAME, pair, orderBook));

            return apiResponse;

        } catch (ClassCastException e) {
            apiResponse.setError(new ApiError(-1, "Cannot find an ordebook for pair " + pair.toString()));
        } catch (IOException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        } catch (ParseException e) {
            apiResponse.setError(new ApiError(-1, e.getMessage()));
        }

        return apiResponse;
    }

    private Order parseOrderSimple(JSONObject in, String type, CurrencyPair pair) {
        Order out = new Order();

        out.setType(type);
        out.setPair(pair);


        Amount amount = new Amount(Utils.getDouble(in.get("Amount")), pair.getOrderCurrency());
        out.setAmount(amount);

        Amount price = new Amount(Utils.getDouble(in.get("Price")), pair.getPaymentCurrency());
        out.setPrice(price);


        out.setCompleted(false);

        return out;
    }

    @Override
    public ApiResponse placeOrders(OrderBatch batch, CurrencyPair pair) {
        return TradeUtils.placeMultipleOrdersSequentiallyImplementation(batch, pair, TradeUtils.INTERVAL_FAST, this);

    }

    @Override
    public ApiResponse placeOrdersParallel(OrderBatch batch, CurrencyPair pair, ArrayList<ApiKeys> keys) {
        return TradeUtils.placeMultipleOrdersParallelImplementation(batch, pair, keys, this);
    }

    @Override
    public ApiResponse clearOrders(CurrencyPair pair) {
        ApiResponse apiResponse = new ApiResponse();

        String method = API_CLEARORDERS;
        boolean isGet = false;
        HashMap<String, String> query_args = new HashMap<>();

        query_args.put("listingCurrency", pair.getOrderCurrency().getCode().toUpperCase());
        query_args.put("referenceCurrency", pair.getPaymentCurrency().getCode().toUpperCase());

        ApiResponse response = getQuery(API_BASE_URL, method, query_args, true, isGet);

        if (response.isPositive()) {
            /*
            empty string "" === OK!
             */
            String resp = (String) response.getResponseObject();
            if (resp.equals("")) {
                apiResponse.setResponseObject(true);
            } else {
                apiResponse.setResponseObject(false);
            }

        } else {
            apiResponse = response;
        }

        return apiResponse;
    }

    @Override
    public ApiError getErrorByCode(int code) {
        return null;
    }

    @Override
    public String getUrlConnectionCheck() {
        return checkConnectionUrl;
    }

    @Override
    public String query(String base, String method, AbstractMap<String, String> args, boolean needAuth,
                        boolean isGet) {
        String queryResult = TOKEN_BAD_RETURN; //Will return this string in case it fails

        if (this.isFree()) {
            this.setBusy();
            queryResult = service.executeQuery(base, method, args, needAuth, isGet);
            this.setFree();
        } else {
            //Another thread is probably executing a query. Init the retry procedure
            long sleeptime = Settings.RETRY_SLEEP_INCREMENT * 1;
            int counter = 0;
            long startTimeStamp = System.currentTimeMillis();
            LOG.debug(method + " blocked, another call is being processed ");
            boolean exit = false;
            do {
                counter++;
                sleeptime = counter * Settings.RETRY_SLEEP_INCREMENT; //Increase sleep time
                sleeptime += (int) (Math.random() * 200) - 100;// Add +- 100 ms random to facilitate competition
                LOG.debug("Retrying for the " + counter + " time. Sleep for " + sleeptime + "; Method=" + method);
                try {
                    Thread.sleep(sleeptime);
                } catch (InterruptedException e) {
                    LOG.error(e.toString());
                }

                //Try executing the call
                if (this.isFree()) {
                    LOG.debug("Finally the exchange is free, executing query after " + counter + " attempt. Method=" + method);
                    this.setBusy();
                    queryResult = service.executeQuery(base, method, args, needAuth, isGet);
                    this.setFree();
                    break; //Exit loop
                } else {
                    LOG.debug("Exchange still busy : " + counter + " .Will retry soon; Method=" + method);
                    exit = false;
                }
                if (System.currentTimeMillis() - startTimeStamp >= 15 * 1000) {
                    exit = true;
                    LOG.error("Method=" + method + " failed too many times and timed out. attempts = " + counter);
                }
            } while (!exit);
        }
        return queryResult;
    }

    @Override
    public SignedRequest getOpenOrdersRequest(CurrencyPair pair) throws ErrorGeneratingALPRequestException {
        String nonce = Long.toString(System.currentTimeMillis());

        //Create the vocabulary
        HashMap<String, String> query_args = new HashMap<>();
        query_args.put("nonce", nonce);
        query_args.put("key", keys.getApiKey());

        String postJSONString = "{";
        if (query_args != null) {
            for (Iterator<Map.Entry<String, String>> argumentIterator = query_args.entrySet().iterator(); argumentIterator.hasNext(); ) {
                Map.Entry<String, String> argument = argumentIterator.next();

                String k = argument.getKey().toString();
                String v = argument.getValue().toString();

                String newJSONelem = "\"" + k + "\":" + "\"" + v + "\"";

                postJSONString += newJSONelem;
                if (argumentIterator.hasNext())
                    postJSONString += ",";

            }
        }
        postJSONString += "}";


        query_args.put("pair", pair.toString());

        //Create the signature
        String signature = TradeUtils.signRequest(keys.getPrivateKey(), postJSONString, SIGN_HASH_FUNCTION, ENCODING);

        return new SignedRequest(query_args, signature);
    }

    @Override
    public void setKeys(ApiKeys keys) {
        this.keys = keys;
    }

    @Override
    public void setApiBaseUrl(String apiBaseUrl) {
        this.apiBaseUrl = apiBaseUrl;
    }


    private ApiResponse getQuery(String url, String method, HashMap<String, String> query_args, boolean needAuth, boolean isGet) {
        ApiResponse apiResponse = new ApiResponse();
        String queryResult = query(url, method, query_args, needAuth, isGet);
        if (queryResult == null) {
            apiResponse.setError(errors.nullReturnError);
            return apiResponse;
        }
        if (queryResult.startsWith(TOKEN_BAD_RETURN)) {
            if (queryResult.equals(TOKEN_BAD_RETURN)) {
                apiResponse.setError(errors.noConnectionError);
                return apiResponse;
            } else {
                apiResponse.setError(new ApiError(errors.genericError.getCode(), queryResult.substring(TOKEN_BAD_RETURN.length())));
                return apiResponse;
            }
        }
        if (queryResult.equals("")) {
            apiResponse.setResponseObject("");
            return apiResponse;
        }

        JSONParser parser = new JSONParser();

        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(queryResult));
            String success = (String) httpAnswerJson.get("success");
            if (success.equals("0")) {
                ApiError error = errors.apiReturnError;
                error.setDescription(httpAnswerJson.get("error").toString());
                apiResponse.setError(error);
                return apiResponse;
            }
            apiResponse.setResponseObject(httpAnswerJson);
        } catch (ClassCastException cce) {
            //if casting to a JSON object failed, try a JSON Array
            try {
                JSONArray httpAnswerJson = (JSONArray) (parser.parse(queryResult));
                apiResponse.setResponseObject(httpAnswerJson);
            } catch (ClassCastException pe) {
                String httpAnswerStr = queryResult;
                apiResponse.setResponseObject(httpAnswerStr);
            } catch (ParseException pe) {
                LOG.error("httpResponse: " + queryResult + " \n" + pe.toString());
                apiResponse.setError(errors.parseError);
            }
        } catch (ParseException pe) {
            LOG.error("httpResponse: " + queryResult + " \n" + pe.toString());
            apiResponse.setError(errors.parseError);
            return apiResponse;
        }
        return apiResponse;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public boolean isFree() {
        return !isBusy;
    }

    public void setBusy() {
        this.isBusy = true;
    }

    public void setFree() {
        this.isBusy = false;
    }


    private class SouthxchangeService implements ServiceInterface {

        protected ApiKeys keys;

        public SouthxchangeService(ApiKeys keys) {
            this.keys = keys;
        }

        @Override
        public String executeQuery(String base, String method, AbstractMap<String, String> args, boolean needAuth, boolean isGet) {
            String answer = null;
            String url = base + method;

            args.put("key", keys.getApiKey());
            args.put("nonce", Long.toString(System.currentTimeMillis()));

            String postJSONString = "{";
            if (args != null) {
                for (Iterator<Map.Entry<String, String>> argumentIterator = args.entrySet().iterator(); argumentIterator.hasNext(); ) {
                    Map.Entry<String, String> argument = argumentIterator.next();

                    String k = argument.getKey().toString();
                    String v = argument.getValue().toString();

                    String newJSONelem = "\"" + k + "\":" + "\"" + v + "\"";

                    postJSONString += newJSONelem;
                    if (argumentIterator.hasNext())
                        postJSONString += ",";

                }
            }
            postJSONString += "}";

            String signature = TradeUtils.signRequest(keys.getPrivateKey(), postJSONString, SIGN_HASH_FUNCTION, ENCODING);


            LOG.trace("\nurl = " + url +
                    "\n, postData = " + postJSONString +
                    "\n, signature = " + signature +
                    "\n, isGet = " + isGet);

            // add header
            Header[] headers = new Header[2];
            headers[0] = new BasicHeader("Hash", signature);
            headers[1] = new BasicHeader("Content-Type", "application/json");

            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = null;
            HttpGet get = null;
            HttpResponse response = null;

            try {
                if (!isGet) {
                    post = new HttpPost(url);
                    StringEntity params = new StringEntity(postJSONString);
                    post.setHeaders(headers);
                    post.setEntity(params);
                    response = client.execute(post);
                } else {
                    get = new HttpGet(url);
                    get.setHeaders(headers);
                    response = client.execute(get);
                }
            } catch (NoRouteToHostException e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            } catch (SocketException e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            } catch (Exception e) {
                if (!isGet) {
                    post.abort();
                } else {
                    get.abort();
                }
                LOG.error(e.toString());
                return null;
            }
            BufferedReader rd;

            if (response.getStatusLine().getStatusCode() > 400) {
                LOG.debug("HttpCode : " + response.getStatusLine().getStatusCode() + ":" + response.getStatusLine().getReasonPhrase());
                return TOKEN_BAD_RETURN + " " + response.getStatusLine().getReasonPhrase();
            }

            try {
                rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));


                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    buffer.append(line);
                }

                answer = buffer.toString();
            } catch (IOException ex) {

                LOG.error(ex.toString());
                return null;//TODO return a meaningful error instead
            } catch (IllegalStateException ex) {

                LOG.error(ex.toString());
                return null;//TODO return a meaningful error instead
            }


            LOG.trace("\nSending request to URL : " + url + " ; get = " + isGet);
            LOG.trace("Response Code : " + response.getStatusLine().getStatusCode());
            LOG.trace("Response :" + response);


            return answer;
        }
    }
}
