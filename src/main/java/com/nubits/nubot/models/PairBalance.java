/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PairBalance {

    private static final Logger LOG = LoggerFactory.getLogger(PairBalance.class.getName());

    private Amount PEGTotal;
    private Amount NBTTotal;
    private Amount PEGonOrder;
    private Amount NBTonOrder;
    private Amount PEGAvailable;
    private Amount NBTAvailable;
    private Amount PEGT1;
    private Amount NBTT1;
    private Amount PEGT2;
    private Amount NBTT2;


    /**
     * @param NBTTotal
     * @param PEGTotal
     */
    public PairBalance(Amount NBTTotal, Amount PEGTotal) {
        this.NBTTotal = NBTTotal;
        this.PEGTotal = PEGTotal;
        this.PEGonOrder = new Amount(0, Currency.createCurrency(PEGTotal.getCurrency().getCode()));
        this.NBTonOrder = new Amount(0, CurrencyList.NBT);
        this.PEGAvailable = PEGTotal;
        this.NBTAvailable = NBTTotal;

        fillMultiTierData();


    }

    private void fillMultiTierData() {
        //switch sell/buy according to swapped pairs
        String NBTType = Constant.SELL;
        String PEGType = Constant.BUY;

        if (Global.swappedPair) {
            NBTType = Constant.BUY;
            PEGType = Constant.SELL;
        }

        if (Global.swappedPair) {
            this.NBTT1 = new Amount(getTier1Liquidity(NBTType) * Global.conversion, CurrencyList.NBT);
            this.PEGT1 = new Amount(getTier1Liquidity(PEGType), Currency.createCurrency(PEGTotal.getCurrency().getCode()));
        } else {
            this.NBTT1 = new Amount(getTier1Liquidity(NBTType), CurrencyList.NBT);
            this.PEGT1 = new Amount(getTier1Liquidity(PEGType) / Global.conversion, Currency.createCurrency(PEGTotal.getCurrency().getCode()));
        }
        this.NBTT2 = new Amount((NBTTotal.getQuantity() - NBTT1.getQuantity()), CurrencyList.NBT);
        this.PEGT2 = new Amount((PEGTotal.getQuantity() - PEGT1.getQuantity()), Currency.createCurrency(PEGTotal.getCurrency().getCode()));
    }

    /**
     * @param PEGAvail
     * @param NBTAvail
     * @param PEGonOrder
     * @param NBTonOrder
     */
    public PairBalance(Amount PEGAvail, Amount NBTAvail, Amount PEGonOrder, Amount NBTonOrder) {
        this.PEGAvailable = PEGAvail;
        this.NBTAvailable = NBTAvail;
        this.PEGonOrder = PEGonOrder;
        this.NBTonOrder = NBTonOrder;
        this.PEGTotal = new Amount(PEGAvailable.getQuantity() + PEGonOrder.getQuantity(), Currency.createCurrency(PEGonOrder.getCurrency().getCode()));
        this.NBTTotal = new Amount(NBTAvailable.getQuantity() + NBTonOrder.getQuantity(), CurrencyList.NBT);

        fillMultiTierData();
    }

    /**
     * @return the PEGTotal
     */
    public Amount getPEGBalance() {
        return PEGTotal;
    }

    /**
     * @return the NBTTotal
     */
    public Amount getNubitsBalance() {
        return NBTTotal;
    }

    /**
     * @return
     */
    public Amount getPEGBalanceonOrder() {
        return PEGonOrder;
    }

    /**
     * @return
     */
    public Amount getNBTonOrder() {
        return NBTonOrder;
    }

    /**
     * @return
     */
    public Amount getPEGAvailableBalance() {
        return PEGAvailable;
    }

    /**
     * @return
     */
    public Amount getNBTAvailable() {
        return NBTAvailable;
    }

    public Amount getPEGT1() {
        return PEGT1;
    }

    public void setPEGT1(Amount PEGT1) {
        this.PEGT1 = PEGT1;
    }

    public Amount getNBTT1() {
        return NBTT1;
    }

    public void setNBTT1(Amount NBTT1) {
        this.NBTT1 = NBTT1;
    }

    public Amount getPEGT2() {
        return PEGT2;
    }

    public void setPEGT2(Amount PEGT2) {
        this.PEGT2 = PEGT2;
    }

    public Amount getNBTT2() {
        return NBTT2;
    }

    public void setNBTT2(Amount NBTT2) {
        this.NBTT2 = NBTT2;
    }

    @Override
    public String toString() {
        return "PairBalance{" +
                "PEGTotal=" + PEGTotal +
                ", NBTTotal=" + NBTTotal +
                ", PEGonOrder=" + PEGonOrder +
                ", NBTonOrder=" + NBTonOrder +
                ", PEGAvailable=" + PEGAvailable +
                ", NBTAvailable=" + NBTAvailable +
                ", PEGT1=" + PEGT1 +
                ", NBTT1=" + NBTT1 +
                ", PEGT2=" + PEGT2 +
                ", NBTT2=" + NBTT2 +
                '}';
    }

    public static PairBalance getSwappedBalance(PairBalance original) {
        return new PairBalance(original.NBTAvailable, original.getPEGAvailableBalance(), original.getNBTonOrder(), original.getPEGBalanceonOrder());
    }

    /**
     * Count the size in NBT of liquidity still available on T1
     * It tries to retrieve the order corresponding to the wall
     * and checks the amount still on order
     */
    public static double getTier1Liquidity(String type) {
        String orderID = "";
        if (type.equalsIgnoreCase(Constant.SELL)) {
            orderID = Global.sellWallOrderID;
        } else {
            orderID = Global.buyWallOrderID;
        }

        if (!orderID.equals("")) {
            ApiResponse orderDetailResponse = null;
            try {
                orderDetailResponse = Global.trade.getOrderDetail(orderID);
                if (orderDetailResponse.isPositive()) {
                    Order order = (Order) orderDetailResponse.getResponseObject();
                    return order.getAmount().getQuantity();
                } else {
                    LOG.debug(orderDetailResponse.getError().toString());
                    return 0;
                }
            } catch (Exception e) {
                LOG.error("Err :" + e.toString()); //TODO remove
                return 0;
            }

        } else {
            LOG.debug("Cannot find orderid of " + type + " wall");
            return 0;
        }

    }

}
