/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.utils.CSVtools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Objects;

import static com.nubits.nubot.models.CurrencyList.NBT;


public class Currency {

    private static final Logger LOG = LoggerFactory.getLogger(Currency.class.getName());
    private boolean fiat; // indicate whether its crypto or fiat
    private String code; // i.e USD
    private String extendedName; // the extended name where available
    private String defaultMainFeedName; //The name of the default mainFeed
    private ArrayList<String> defaultbackupFeedNames; //The names of the default backupFeeds

    /**
     *
     */
    public static Currency createCurrency(String code) {
        Currency toRet = null;
        ArrayList<String[]> currencyList = CSVtools.parseCsvFromFile(Settings.CURRENCY_FILE_PATH);
        /*
        Currencycode,ExtendedName,isFiat,defaultMainFeedName,defaultBackupFeedName1,defaultBackupFeedName2,...,defaultBackupFeedNameN
        USD,US Dollar,true,,
        CNY,Chinese yen,true,yahoo,Openexchangerates,Exchangeratelab,GoogleOfficial
        EUR,Euro,true,BitstampEUR,yahoo,Openexchangerates,Exchangeratelab,GoogleOfficial
         */
        boolean found = false;
        for (int j = 1; j < currencyList.size(); j++) {
            String[] tempLine = currencyList.get(j);

            if (tempLine[0].equalsIgnoreCase(code)) {

                String cCode = tempLine[0];
                String extendedName = tempLine[1];
                boolean isFiat = Boolean.parseBoolean(tempLine[2]);

                String defaultMainFeedName = "";
                if (tempLine.length > 3)
                    defaultMainFeedName = tempLine[3];

                ArrayList<String> defaultbackupFeedNames = new ArrayList<String>();
                if (tempLine.length > 4)
                    defaultbackupFeedNames = extractBackupFeedNames(tempLine);

                return new Currency(isFiat, cCode, extendedName, defaultMainFeedName, defaultbackupFeedNames);
            }
        }

        if (!found) {
            LOG.warn("Didn't find a currency with code " + code + " in lookup table " + Settings.CURRENCY_FILE_PATH
                    + "\nUpdate the currency file to avoid malfunctionings.");

            return new Currency(false, code, "", "", new ArrayList<String>());

        }


        return toRet;
    }

    private static ArrayList<String> extractBackupFeedNames(String[] row) {
        /*
            Expect tow to be :
            EUR,Euro,true,BitstampEUR,yahoo,Openexchangerates,Exchangeratelab,GoogleOfficial

            Where the first backupFeedName is in position 4 of the array
         */
        int firstPosition = 4;
        ArrayList<String> backupFeedNames = new ArrayList<String>();
        for (int i = firstPosition; i < row.length; i++)
            backupFeedNames.add(row[i]);
        return backupFeedNames;

    }

    public Currency(boolean fiat, String code, String extendedName, String defaultMainFeedName, ArrayList<String> defaultbackupFeedNames) {
        this.fiat = fiat;
        this.code = code;
        this.extendedName = extendedName;
        this.defaultMainFeedName = defaultMainFeedName;
        this.defaultbackupFeedNames = defaultbackupFeedNames;

    }

    public static Currency getCurrencyToTrack(CurrencyPair pair) throws CurrencyToTrackNotAvailableException {
        Currency toTrackCurrency;

        if (pair.equals(CurrencyList.NBT_USD) || pair.equals(CurrencyList.USD_NBT))
            return CurrencyList.USD;

        if (pair.getPaymentCurrency().equals(NBT) || pair.getPaymentCurrency().equals(CurrencyList.USD)) { //NBTorUSD as paymentCurrency
            toTrackCurrency = pair.getOrderCurrency();
        } else {
            toTrackCurrency = pair.getPaymentCurrency();
        }

        if (!pair.getPaymentCurrency().equals(NBT) && !pair.getPaymentCurrency().equals(CurrencyList.USD)
                && !pair.getOrderCurrency().equals(NBT) && !pair.getOrderCurrency().equals(CurrencyList.USD)) {
            throw new CurrencyToTrackNotAvailableException("Cannot track pair :" + pair.toString());
        }

        return toTrackCurrency;
    }

    /**
     * @return
     */
    public boolean isFiat() {
        return fiat;
    }

    /**
     * @param fiat
     */
    public void setFiat(boolean fiat) {
        this.fiat = fiat;
    }

    /**
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return
     */
    public String getExtendedName() {
        return extendedName;
    }

    /**
     * @param extendedName
     */
    public void setExtendedName(String extendedName) {
        this.extendedName = extendedName;
    }

    @Override
    public String toString() {
        return "Currency{fiat=" + fiat + ", code=" + code + ", extendedName=" + extendedName + '}';
    }

    public String getDefaultMainFeedName() {
        return defaultMainFeedName;
    }

    public void setDefaultMainFeedName(String defaultMainFeedName) {
        this.defaultMainFeedName = defaultMainFeedName;
    }

    public ArrayList<String> getDefaultbackupFeedNames() {
        return defaultbackupFeedNames;
    }

    public void setDefaultbackupFeedNames(ArrayList<String> defaultbackupFeedNames) {
        this.defaultbackupFeedNames = defaultbackupFeedNames;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Currency other = (Currency) obj;
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        return true;
    }
}
