/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Used by TradeInterface.placeOrder(), wrapped in the ApiResponse
 */


public class MultipleOrdersResponse {
    private static final Logger LOG = LoggerFactory.getLogger(MultipleOrdersResponse.class.getName());

    private ArrayList<OrderPlaced> ordersRequested;
    private long timeElapsed = 0;

    public MultipleOrdersResponse(ArrayList<OrderPlaced> individualResponses) {
        this.ordersRequested = individualResponses;
    }

    public boolean areAllOrdersPlacedCorrectly() {
        for (int i = 0; i < ordersRequested.size(); i++)
            if (!ordersRequested.get(i).getResponse().isPositive())
                return false; //one negative response
        return true;
    }

    public int countOrdersRequested() {
        return ordersRequested.size();
    }

    public int countOrdersPlacedSuccessfully() {
        int toRet = 0;
        for (int i = 0; i < ordersRequested.size(); i++)
            if (ordersRequested.get(i).getResponse().isPositive())
                toRet++;

        return toRet;
    }

    public int countOrdersPlacedWithErrors() {
        int toRet = 0;
        for (int i = 0; i < ordersRequested.size(); i++)
            if (!ordersRequested.get(i).getResponse().isPositive())
                toRet++;
        return toRet;
    }

    public ArrayList<OrderPlaced> getOrdersPlacedWithErrors() {
        ArrayList<OrderPlaced> toRet = new ArrayList<OrderPlaced>();
        for (int i = 0; i < ordersRequested.size(); i++)
            if (!ordersRequested.get(i).getResponse().isPositive())
                toRet.add(ordersRequested.get(i));
        return toRet;
    }

    public ArrayList<OrderPlaced> getOrdersPlacedSuccessfully() {
        ArrayList<OrderPlaced> toRet = new ArrayList<OrderPlaced>();
        for (int i = 0; i < ordersRequested.size(); i++)
            if (ordersRequested.get(i).getResponse().isPositive())
                toRet.add(ordersRequested.get(i));
        return toRet;
    }

    public void printOrdersPlacedSuccessfully() {
        for (int i = 0; i < ordersRequested.size(); i++) {
            if (ordersRequested.get(i).getResponse().isPositive()) {
                LOG.info("Order placed = " + ordersRequested.get(i).getOrder().toString());
                LOG.info("Order ID = " + ordersRequested.get(i).getResponse().getResponseObject().toString() + "\n");
            }
        }

    }

    public void printOrdersPlacedWithErrors() {
        for (int i = 0; i < ordersRequested.size(); i++) {
            if (!ordersRequested.get(i).getResponse().isPositive()) {
                LOG.info("Order attempted to place = " + ordersRequested.get(i).getOrder().toString());
                LOG.info("Order attempted error = " + ordersRequested.get(i).getResponse().getError().toString() + "\n");

            }
        }

    }

    public double getTotalAmountPlaced() {
        double cumulativeQuantity = 0;
        for (int i = 0; i < ordersRequested.size(); i++) {
            if (ordersRequested.get(i).getResponse().isPositive()) {
                cumulativeQuantity += ordersRequested.get(i).getOrder().getSize();
            }
        }
        return cumulativeQuantity;
    }

    public double getTier1OrderSize(String tier1OrderID) {
        if (!tier1OrderID.equals("")) {
            for (int i = 0; i < ordersRequested.size(); i++) {
                if (ordersRequested.get(i).getResponse().isPositive()) {
                    String tempID = (String) ordersRequested.get(i).getResponse().getResponseObject();
                    if (tier1OrderID.equalsIgnoreCase(tempID)) {
                        return ordersRequested.get(i).getOrder().getSize();
                    }
                }
            }
            LOG.debug("Unable to find t1 order with id " + tier1OrderID);
            return 0;
        } else {
            LOG.debug("There is no wall");
            return 0;
        }
    }

    public double getTier2CumulativeOrdersSize(String tier1OrderID) {
        return getTotalAmountPlaced() - getTier1OrderSize(tier1OrderID);
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }

    public void setTimeElapsed(long timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public ArrayList<OrderPlaced> getAllOrdersRequested() {
        return ordersRequested;
    }


}
