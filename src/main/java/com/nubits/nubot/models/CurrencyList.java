/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.models;


import java.util.ArrayList;

public class CurrencyList {


    public static Currency NBT;
    public static ArrayList<Currency> allCurrencies = new ArrayList<>();
    public static ArrayList<CurrencyPair> allPairs = new ArrayList<>();
    public static ArrayList<CurrencyPair> supportedPairs = new ArrayList<>();

    //fiat
    public static Currency USD;
    public static Currency CNY;
    public static Currency EUR;
    public static Currency PHP;
    public static Currency JPY;
    public static Currency HKD;

    //crypto
    public static Currency BTC;
    public static Currency NSR;
    public static Currency PPC;
    public static Currency LTC;
    public static Currency DOGE;
    public static Currency ETH;
    public static Currency XRP;

    //Unkwnown
    public static Currency UNKWNOWN;

    //nbt fiat pairs
    public static CurrencyPair NBT_USD;
    public static CurrencyPair USD_NBT;

    public static CurrencyPair NBT_EUR;
    public static CurrencyPair NBT_CNY;
    public static CurrencyPair NBT_JPY;
    public static CurrencyPair NBT_PHP;
    public static CurrencyPair NBT_HKD;

    //fiat to fiat pairs
    public static CurrencyPair EUR_USD;
    public static CurrencyPair CNY_USD;
    public static CurrencyPair PHP_USD;
    public static CurrencyPair HKD_USD;
    public static CurrencyPair JPY_USD;

    //nbt crypto pairs
    public static CurrencyPair NBT_BTC;
    public static CurrencyPair NBT_PPC;

    //swapped crypto pairs
    public static CurrencyPair BTC_NBT;
    public static CurrencyPair LTC_NBT;
    public static CurrencyPair XRP_NBT;
    public static CurrencyPair ETH_NBT;

    //other pairs
    public static CurrencyPair XRP_BTC;
    public static CurrencyPair ETH_BTC;
    public static CurrencyPair LTC_BTC;
    public static CurrencyPair BTC_USD;
    public static CurrencyPair PPC_USD;
    public static CurrencyPair ETH_USD;
    public static CurrencyPair XRP_USD;
    public static CurrencyPair LTC_USD;
    public static CurrencyPair PPC_BTC;
    public static CurrencyPair PPC_LTC;
    public static CurrencyPair BTC_CNY;
    public static CurrencyPair DOGE_BTC;

    //Unknown
    public static CurrencyPair UNKNOWPAIR;


    static {
        USD = Currency.createCurrency("USD");
        allCurrencies.add(USD);

        CNY = Currency.createCurrency("CNY");
        allCurrencies.add(CNY);

        EUR = Currency.createCurrency("EUR");
        allCurrencies.add(EUR);

        PHP = Currency.createCurrency("PHP");
        allCurrencies.add(PHP);

        HKD = Currency.createCurrency("HKD");
        allCurrencies.add(HKD);

        JPY = Currency.createCurrency("JPY");
        allCurrencies.add(JPY);

        BTC = Currency.createCurrency("BTC");
        allCurrencies.add(BTC);

        NBT = Currency.createCurrency("NBT");
        allCurrencies.add(NBT);

        NSR = Currency.createCurrency("NSR");
        allCurrencies.add(NSR);

        PPC = Currency.createCurrency("PPC");
        allCurrencies.add(PPC);

        LTC = Currency.createCurrency("LTC");
        allCurrencies.add(LTC);

        DOGE = Currency.createCurrency("DOGE");
        allCurrencies.add(DOGE);

        XRP = Currency.createCurrency("XRP");
        allCurrencies.add(XRP);

        ETH = Currency.createCurrency("ETH");
        allCurrencies.add(ETH);

        UNKWNOWN = new Currency(false, "???", "Unknown currency", "", new ArrayList<>());
        allCurrencies.add(UNKWNOWN);

        NBT_USD = new CurrencyPair(NBT, USD);
        allPairs.add(NBT_USD);

        USD_NBT = new CurrencyPair(USD, NBT);
        allPairs.add(USD_NBT);

        BTC_USD = new CurrencyPair(BTC, USD);
        allPairs.add(BTC_USD);

        NBT_BTC = new CurrencyPair(NBT, BTC);
        allPairs.add(NBT_BTC);

        BTC_NBT = new CurrencyPair(BTC, NBT);
        allPairs.add(BTC_NBT);

        NBT_PPC = new CurrencyPair(NBT, PPC);
        allPairs.add(NBT_PPC);

        NBT_EUR = new CurrencyPair(NBT, EUR);
        allPairs.add(NBT_EUR);

        NBT_CNY = new CurrencyPair(NBT, CNY);
        allPairs.add(NBT_CNY);

        NBT_JPY = new CurrencyPair(NBT, JPY);
        allPairs.add(NBT_JPY);

        NBT_PHP = new CurrencyPair(NBT, PHP);
        allPairs.add(NBT_PHP);

        NBT_HKD = new CurrencyPair(NBT, HKD);
        allPairs.add(NBT_HKD);

        BTC_USD = new CurrencyPair(BTC, USD);
        allPairs.add(BTC_USD);

        PPC_USD = new CurrencyPair(PPC, USD);
        allPairs.add(PPC_USD);

        ETH_USD = new CurrencyPair(ETH, USD);
        allPairs.add(ETH_USD);

        XRP_USD = new CurrencyPair(XRP, USD);
        allPairs.add(XRP_USD);

        LTC_USD = new CurrencyPair(LTC, USD);
        allPairs.add(LTC_USD);

        PPC_BTC = new CurrencyPair(PPC, BTC);
        allPairs.add(PPC_BTC);

        PPC_LTC = new CurrencyPair(PPC, LTC);
        allPairs.add(PPC_LTC);

        BTC_CNY = new CurrencyPair(BTC, CNY);
        allPairs.add(BTC_CNY);

        EUR_USD = new CurrencyPair(EUR, USD);
        allPairs.add(EUR_USD);

        CNY_USD = new CurrencyPair(CNY, USD);
        allPairs.add(CNY_USD);

        PHP_USD = new CurrencyPair(PHP, USD);
        allPairs.add(PHP_USD);

        HKD_USD = new CurrencyPair(HKD, USD);
        allPairs.add(HKD_USD);

        JPY_USD = new CurrencyPair(JPY, USD);
        allPairs.add(JPY_USD);

        LTC_BTC = new CurrencyPair(LTC, BTC);
        allPairs.add(LTC_BTC);

        DOGE_BTC = new CurrencyPair(DOGE, BTC);
        allPairs.add(DOGE_BTC);

        ETH_NBT = new CurrencyPair(ETH, NBT);
        allPairs.add(ETH_NBT);

        LTC_NBT = new CurrencyPair(LTC, NBT);
        allPairs.add(LTC_NBT);

        XRP_NBT = new CurrencyPair(XRP, NBT);
        allPairs.add(XRP_NBT);

        ETH_BTC = new CurrencyPair(ETH, BTC);
        allPairs.add(ETH_BTC);

        LTC_BTC = new CurrencyPair(LTC, BTC);
        allPairs.add(LTC_BTC);

        XRP_BTC = new CurrencyPair(XRP, BTC);
        allPairs.add(XRP_BTC);

        UNKNOWPAIR = new CurrencyPair(UNKWNOWN, UNKWNOWN);
        allPairs.add(UNKNOWPAIR);

        //Pairs supported by NuBot
        supportedPairs.add(NBT_USD);
        supportedPairs.add(NBT_BTC);
        supportedPairs.add(NBT_EUR);
        supportedPairs.add(NBT_CNY);
        supportedPairs.add(NBT_PPC);
        supportedPairs.add(NBT_BTC);
        //swapped
        supportedPairs.add(BTC_NBT);
        supportedPairs.add(LTC_NBT);
        supportedPairs.add(XRP_NBT);
        supportedPairs.add(ETH_NBT);


    }

    public static boolean isPairSupported(CurrencyPair pair) {
        for (int i = 0; i < supportedPairs.size(); i++) {
            if (pair.equals(supportedPairs.get(i))) {
                return true;
            }
        }
        return false;
    }


}
