/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.streamclient;


import com.google.gson.internal.LinkedHashTreeMap;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.CredentialManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.pricefeeds.feedservices.AbstractPriceFeed;
import com.nubits.nubot.strategy.Secondary.NuBotSecondary;
import com.nubits.nubot.strategy.Secondary.StrategySecondaryPegTask;
import com.nubits.nubot.tasks.PriceMonitorTriggerTask;
import com.nubits.nubot.trading.TradeUtils;
import com.nubits.nubot.utils.Utils;
import com.nubits.nubotstream.InitSocketMessage;
import com.nubits.nubotstream.ShiftOrdersMessage;
import com.nubits.nubotstream.StreamMessageFacade;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * simple subscriber
 */
public class Subscriber implements Runnable {

    final static Logger LOG = LoggerFactory.getLogger(Subscriber.class);

    private ZMQ.Context context;
    private String id;

    private String server;
    private String currencyCode;
    private String mainport; //port to init connection.
    private String pingport; //port for ping? requests
    private String currencyport; //port for ping? requests
    private String currencyportInitial; //port for ping? requests
    private String currencyToken; //port for ping? requests
    private int wrongTokenCount = 0;
    private static boolean reactedToFirstMessage;
    private static boolean startedStrategy;
    private ScheduledFuture<?> pingHandle;

    private int discordantCounter = 0;
    private static boolean connected = false;

    public Subscriber(String server, String mainPort, Currency currency) {
        this.currencyCode = currency.getCode();
        this.server = server;
        this.mainport = mainPort;

        int intmainport = Integer.parseInt(mainPort);
        this.pingport = Integer.toString(intmainport - 1); //pingport = mainport-1

        if (SessionManager.getSessionId() != null)
            this.id = SessionManager.getSessionId();
        else
            this.id = Utils.generateSessionID();
        this.reactedToFirstMessage = false;
        this.startedStrategy = false;

        context = ZMQ.context(1);

    }


    private void subscribe(ZMQ.Context context, String currencyport) {
        if (connected) {
            ZMQ.Socket priceSubscriber = context.socket(ZMQ.SUB);
            String connectionString = getSocketAddress(currencyport);
            LOG.warn("Subscribing to websocket push updates from streamer @ " + connectionString);
            priceSubscriber.connect(connectionString);

            // Subscribe to everything
            String filter = "";
            priceSubscriber.subscribe(filter.getBytes(ZMQ.CHARSET));

            //priceSubscriber.setReceiveTimeOut(2000);
            priceSubscriber.setSendTimeOut(2000);

            int consecutiveFailuresCounter = 0;
            while (!Thread.currentThread().isInterrupted()) {
                if (connected) {
                    String recvstring = priceSubscriber.recvStr(0);
                    LOG.trace("received json: " + recvstring);

                    try {
                        reactOnMessageReceived(recvstring, false);
                        consecutiveFailuresCounter = 0;
                    } catch (StreamerException e) {
                        LOG.error(e.toString());
                        consecutiveFailuresCounter++;
                        if (consecutiveFailuresCounter > 3) {
                            LOG.warn("Turning off Streamer tracking and switching to local price tracking");
                            terminateAndStartLocalTracker();
                        }
                    }
                } else {
                    LOG.error("Cannot read from feed, connection problem with server");
                    consecutiveFailuresCounter++;
                    LOG.warn("Turning off Streamer tracking and switching to local price tracking");
                    terminateAndStartLocalTracker();
                }

            }
            priceSubscriber.close();
            context.term();
        } else {
            LOG.error("Cannot subscribe to feed, connection problem with server");
            terminateAndStartLocalTracker();
        }
    }


    private void reactOnMessageReceived(String message, boolean firstTime) throws StreamerException {
        if (message == null) {
            throw new StreamerException("Connection with streamer timed out, or null response");
        } else if (message.equals(StreamMessageFacade.COMMAND_TERMINATE_CONNECTION)) {
            LOG.info("Connection terminated successfully");
            return;
        }
        JSONParser parser = new JSONParser();
        try {
            JSONObject responseJSON = (JSONObject) parser.parse(message);
            String command = (String) responseJSON.get("command");
            if (command.equalsIgnoreCase(StreamMessageFacade.COMMAND_SHIFT_WALLS)) {
                //Reconstruct ShiftOrdersMessage
                ShiftOrdersMessage sm = ShiftOrdersMessage.parseFromString(message);
                LOG.info("ShiftOrdersMessage: " + Utils.minify(sm.toString(), false));
                LOG.info("These is the raw list of prices obtained by Streamer : " + sm.getAttachments()[0]);

                if (sm.getErrorMessage().equals("")) {
                    if (sm.getToken().equals(currencyToken)) {
                        wrongTokenCount = 0; //reset counter
                        double price = Utils.getDouble(sm.getArgs()[1]);
                        double spread = Utils.getDouble(sm.getArgs()[8]);
                        double shiftTreshold = Utils.getDouble(sm.getArgs()[7]);
                        String feed = sm.getArgs()[6].toString();
                        String pairString = sm.getArgs()[0].toString();

                        CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(pairString);
                        String currencyTracked = pair.getOrderCurrency().getCode();
                        LOG.info("Streamer requested a wallShift. pair= " + pairString + " feed=" + feed + " price=" + price + " currencyTracked= " + currencyTracked + ", firstTime:" + firstTime);

                        if (firstTime) {
                            //INIT orders
                            if (isPriceOk(currencyTracked, price, feed)) {
                                Global.isSubscribed = true;
                                LOG.warn("Initiating price subscription. Streamer will push a notification when price changes more than " + shiftTreshold + "%. Initial price to track: " + sm.getArgs()[1] + " USD");
                                initStrategy(sm);
                            } else {
                                LOG.warn("Remote price not considered OK. Starting local price tracker.");
                                terminateAndStartLocalTracker();
                            }
                        } else {
                            //ShiftWalls
                            if (isPriceOk(currencyTracked, price, feed)) {
                                discordantCounter = 0;
                                LOG.warn("Streamer :command shift walls. newprice: " + sm.getArgs()[1]);
                                shiftWalls(sm);
                            } else {
                                discordantCounter++;
                                LOG.debug("Streamer provided a discortant for " + discordantCounter + " times in a row!");
                                if (discordantCounter >= 3) {
                                    terminateAndStartLocalTracker();
                                    LOG.error("Remote price not considered OK. Starting local price tracker.");
                                } else {
                                    LOG.warn("Ignoring shift command. discordantCounter:" + discordantCounter);
                                }
                            }
                        }

                    } else {
                        wrongTokenCount++;
                        LOG.warn("The token sent by Streamer doesn't look right. counter=" + wrongTokenCount);
                        if (wrongTokenCount > 2) {
                            LOG.warn("Terminating subscriber and init local Strategy.");
                            terminateAndStartLocalTracker();

                        } else {
                            LOG.warn("Trying to get the new token and restart orders");
                            getAuthMessage(context);
                        }

                    }
                } else {
                    LOG.error("Streamer reported an error :" + sm.getErrorMessage());
                    if (sm.getErrorMessage().startsWith(StreamMessageFacade.ERROR_CANT_FETCH)) {
                        int consecutiveFailureReported = Integer.parseInt(sm.getAttachments()[1].toString());

                        if (consecutiveFailureReported > 3) {
                            LOG.error("Streamer reported " + consecutiveFailureReported + " consecutive failure in fetching prices. Starting local price tracker.");
                            terminateAndStartLocalTracker();
                        }
                    }
                }
            } else if (command.equalsIgnoreCase(StreamMessageFacade.COMMAND_INIT_SOCKET)) {

                InitSocketMessage ism = InitSocketMessage.parseFromString(message);
                LOG.info("InitSocketMessage: " + Utils.minify(ism.toString(), false));

                currencyport = (String) ism.getArgs()[0];
                currencyportInitial = Integer.toString(Integer.parseInt(currencyport) + 100);
                currencyToken = (String) ism.getArgs()[1];

                if (!reactedToFirstMessage) {
                    this.reactedToFirstMessage = true;

                    getFirstPrice(context, currencyportInitial, currencyToken);
                    subscribe(context, currencyport);
                } else {
                    LOG.warn("The token has been updated.");
                    getFirstPrice(context, currencyportInitial, currencyToken);

                }
            } else {
                LOG.warn("Unknown command received: " + command);
            }
        } catch (ParseException e) {
            throw new StreamerException("Cannot parse message received from Streamer " + e.toString());
        }
    }

    private boolean isPriceOk(String currencyCode, double remotePrice, String feedname) {
        //is currencyTracked correct?
        Currency toTrackCurrency = null;
        try {
            toTrackCurrency = Currency.getCurrencyToTrack(Global.options.getPair());
        } catch (Exception e) {
            LOG.error(e.toString());
            return false;
        }

        if (!toTrackCurrency.getCode().equals(currencyCode)) {
            LOG.error("Failed in validating price received: the currency received (" + currencyCode + ") is different from local (" + toTrackCurrency + ")");
            return false;
        }


        //does feed exist?
        AbstractPriceFeed feed = null;
        try {
            feed = FeedFacade.getFeed(feedname);
        } catch (NuBotConfigException e) {
            LOG.error(e.toString());
            return false;
        }


        //fetch last price
        LastPrice lastPriceLocal = null;
        try {
            lastPriceLocal = feed.forceFetchLastPrice(new CurrencyPair(toTrackCurrency, CurrencyList.USD));
        } catch (FeedCallException fce) {
            LOG.error(fce.toString());
            return false;
        }

        //is price within boundary
        double diffPercentage = Utils.percentagePriceVariation(lastPriceLocal.getPrice().getQuantity(), remotePrice);
        if (diffPercentage > Settings.MAX_DEVIANCE_REMOTEFEED) {
            LOG.error("Locally fetched price (" + lastPriceLocal.getPrice().getQuantity() + " from " + lastPriceLocal.getSource() + ") is not inline with price provided by Streamer (" + remotePrice + " from " + feedname + ")");
            return false;
        } else {
            LOG.debug("Locally fetched price (" + lastPriceLocal.getPrice().getQuantity() + ") is inline with price provided by Streamer (" + remotePrice + ") from " + feedname);
            return true;
        }

    }

    private void shiftWalls(ShiftOrdersMessage sm) {
        //crosscheck with computeNewPrices in PriceMonitorTriggerTask

        if (SessionManager.sessionInterrupted()) {
            LOG.warn("Session is inactive, no need to shift walls");
            return; //external interruption
        }

        CurrencyPair receivedPair = CurrencyPair.getCurrencyPairFromString((String) sm.getArgs()[0]);
        double peg_price = Utils.getDouble(sm.getArgs()[1]);

        double sellPriceUSD_remote = Utils.getDouble(sm.getArgs()[2]);
        double buyPriceUSD_remote = Utils.getDouble(sm.getArgs()[3]);
        double sellPricePEG_remote = Utils.getDouble(sm.getArgs()[4]);
        double buyPricePEG_remote = Utils.getDouble(sm.getArgs()[5]);
        String source = (String) sm.getArgs()[6];
        double wallShiftThreshold = Utils.getDouble(sm.getArgs()[7]);
        double spread_remote = Utils.getDouble(sm.getArgs()[8]);


        ArrayList<LastPrice> lastPrices = parseLastPricesAttached(sm.getAttachments()[0]);

        Global.conversion = peg_price; //used then for liquidity info

        BidAskPair usdPrices = TradeUtils.computeUSDPrices(peg_price);
        String message = "Computing USD prices with sellOffset:" + Global.options.getBookSellOffset() + "$ and buyOffset:" + Global.options.getBookBuyOffset() + "  : buy @ " + usdPrices.getAsk();
        if (Global.options.isDualSide()) {
            message += " buy @ " + usdPrices.getBid();
        }
        LOG.info(message);


        BidAskPair pegPrices = TradeUtils.computePEGPrices(usdPrices, peg_price);

        String message2 = "Actual prices from " + source + " (using 1 " + Global.options.getPair().getPaymentCurrency().getCode() + " = " + peg_price + " USD)"
                + " : sell @ " + pegPrices.getAsk() + " " + Global.options.getPair().getPaymentCurrency().getCode() + "";

        if (Global.options.isDualSide()) {
            message2 += "; buy @ " + pegPrices.getBid() + " " + Global.options.getPair().getPaymentCurrency().getCode();
        }
        LOG.info(message2);


        String currency = receivedPair.getPaymentCurrency().getCode();
        String crypto = receivedPair.getOrderCurrency().getCode();

        StrategySecondaryPegTask secTask = (StrategySecondaryPegTask) Global.taskManager.getSecondaryPegTask().getTask();


        secTask.notifyPriceChanged(pegPrices.getAsk(), pegPrices.getBid(), peg_price);


        PriceMonitorTriggerTask.writeShiftToFileAndNotify(source, peg_price, receivedPair.getPaymentCurrency().getCode(),
                receivedPair.getOrderCurrency().getCode(), lastPrices, pegPrices.getAsk(), pegPrices.getBid(), wallShiftThreshold, true);

    }

    private ArrayList<LastPrice> parseLastPricesAttached(Object o) {
        ArrayList<LastPrice> toReturn = new ArrayList<>();

        try {
            ArrayList<LinkedHashTreeMap> list = (ArrayList) o;
            for (int i = 0; i < list.size(); i++) {
                LinkedHashTreeMap temp = (LinkedHashTreeMap) list.get(i);

                boolean error = (Boolean) temp.get("error");
                String source = (String) temp.get("source");


                // --
                LinkedHashTreeMap currencyMeasuredObject = (LinkedHashTreeMap) temp.get("currencyMeasured");
                Currency currencyMeasured = parseCurrency(currencyMeasuredObject);

                //Parse price
                LinkedHashTreeMap priceObject = (LinkedHashTreeMap) temp.get("price");
                double quantity = Utils.getDouble(priceObject.get("quantity"));
                LinkedHashTreeMap currencyObject = (LinkedHashTreeMap) priceObject.get("currency");
                Currency priceCurrency = parseCurrency(currencyObject);
                Amount amount = new Amount(quantity, priceCurrency);

                toReturn.add(new LastPrice(error, source, currencyMeasured, amount));
            }
        } catch (ClassCastException e) {
            LOG.error(e.toString());
        }


        return toReturn;
    }

    private Currency parseCurrency(LinkedHashTreeMap map) {
        String code = (String) map.get("code");
        return Currency.createCurrency(code);
    }

    private void initStrategy(ShiftOrdersMessage sm) {
        ((PriceMonitorTriggerTask) Global.taskManager.getPriceTriggerTask().getTask()).init();
        //crosscheck with initStrategy (PriceMonitorTriggerTask)
        if (SessionManager.sessionInterrupted()) {
            LOG.warn("Session is inactive, no need to shift walls");
            return; //external interruption
        }
        double peg_price = Utils.getDouble(sm.getArgs()[1]);
        double sellPriceUSD_remote = Utils.getDouble(sm.getArgs()[2]);
        double buyPriceUSD_remote = Utils.getDouble(sm.getArgs()[3]);
        double sellPricePEG_remote = Utils.getDouble(sm.getArgs()[4]);
        double buyPricePEG_remote = Utils.getDouble(sm.getArgs()[5]);
        String source = (String) sm.getArgs()[6];
        double spread_remote = Utils.getDouble(sm.getArgs()[8]);

        ArrayList<LastPrice> lastPrices = parseLastPricesAttached(sm.getAttachments()[0]);

        Global.conversion = peg_price; //used then for liquidity info


        BidAskPair usdPrices = TradeUtils.computeUSDPrices(peg_price);
        String message = "Computing USD prices with sellOffset:" + Global.options.getBookSellOffset() + "$ and buyOffset:" + Global.options.getBookBuyOffset() + "  : buy @ " + usdPrices.getAsk();

        if (Global.options.isDualSide()) {
            message += " buy @ " + usdPrices.getBid();
        }
        LOG.info(message);

        BidAskPair pegPrices = TradeUtils.computePEGPrices(usdPrices, peg_price);

        String message2 = "Actual prices from " + source + " (using 1 " + Global.options.getPair().getPaymentCurrency().getCode() + " = " + peg_price + " USD)"
                + " : sell @ " + pegPrices.getAsk() + " " + Global.options.getPair().getPaymentCurrency().getCode() + "";

        if (Global.options.isDualSide()) {
            message2 += "; buy @ " + pegPrices.getBid() + " " + Global.options.getPair().getPaymentCurrency().getCode();
        }
        LOG.info(message2);

        if (SessionManager.sessionInterrupted()) return; //external interruption

        //Assign prices
        StrategySecondaryPegTask secTask = (StrategySecondaryPegTask) Global.taskManager.getSecondaryPegTask().getTask();
        if (!Global.swappedPair) {
            secTask.setBuyPricePEG(pegPrices.getBid());
            secTask.setSellPricePEG(pegPrices.getAsk());
        } else {
            secTask.setBuyPricePEG(pegPrices.getAsk());
            secTask.setSellPricePEG(pegPrices.getBid());
        }
        this.startedStrategy = true;

        //Start strategy
        Global.taskManager.getSecondaryPegTask().start();
    }


    private void getAuthMessage(ZMQ.Context context) {
        if (connected) {
            //open a new socket
            ZMQ.Socket requestAuthSocket = context.socket(ZMQ.REQ);
            String cstr = getSocketAddress(mainport);

            requestAuthSocket.connect(cstr);

            requestAuthSocket.setSendTimeOut(2000);
            requestAuthSocket.setReceiveTimeOut(2000);

            //prepare the message :
            //Message Format : id token currencycode
            String token = Global.credentialManager.isAuthValid() ? Global.credentialManager.get(CredentialManager.STREAMER_TOKEN_KEY) : Settings.DEFAULT_STREAMING_SERVER;

            String request = id + " " + token + " " + currencyCode;

            requestAuthSocket.send(request.getBytes(ZMQ.CHARSET), 0);

            byte[] reply = requestAuthSocket.recv(0);
            String srep = new String(reply, ZMQ.CHARSET);

            try {
                reactOnMessageReceived(srep, true);
            } catch (StreamerException e) {
                LOG.error(e.toString());
                LOG.warn("Terminating subscriber and switching to local tracker.");
                terminateAndStartLocalTracker();
            }

            requestAuthSocket.close();
        } else {
            LOG.error("Cannot get first message, connection problem with server");
            LOG.warn("Terminating subscriber and switching to local tracker.");
            terminateAndStartLocalTracker();
        }
    }


    private void getFirstPrice(ZMQ.Context context, String port, String currencyToken) {
        if (connected) {
            syncStatusImplementation(context, port, currencyToken, "start");
        } else {
            LOG.error("Cannot get first message, connection problem with server. Starting local tracker.");
            terminateAndStartLocalTracker();
        }
    }

    public void sendDisconnectMessage() {
        if (connected) {
            syncStatusImplementation(context, this.currencyportInitial, this.currencyToken, "stop");
        } else {
            LOG.error("Cannot send disconnection message, connection problem with server");
        }
    }

    private void syncStatusImplementation(ZMQ.Context context, String port, String currencyToken, String status) {
        //open a new socket
        ZMQ.Socket requestFirstPriceSocket = context.socket(ZMQ.REQ);
        String cstr = getSocketAddress(port);
        LOG.info("Sync status from " + cstr);

        requestFirstPriceSocket.connect(cstr);

        requestFirstPriceSocket.setSendTimeOut(2000);
        requestFirstPriceSocket.setReceiveTimeOut(2000);

        //prepare the message :
        //Message Format : currencyToken id action

        String request = currencyToken + " " + id + " " + status;

        requestFirstPriceSocket.send(request.getBytes(ZMQ.CHARSET), 0);

        byte[] reply = requestFirstPriceSocket.recv(0);
        if (reply != null) {
            String srep = new String(reply, ZMQ.CHARSET);
            try {
                reactOnMessageReceived(srep, !startedStrategy);
            } catch (StreamerException e) {
                LOG.error(e.toString());
                LOG.warn("Terminating subscriber and switching to local tracker.");
                terminateAndStartLocalTracker();
            }
        } else {
            LOG.error("Error while getting first price. Null response");
            LOG.warn("Terminating subscriber and switching to local tracker.");
            terminateAndStartLocalTracker();
        }

        requestFirstPriceSocket.close();
    }


    @Override
    public void run() {
        if (isStreamerUp()) {
            connected = true;
            LOG.info("Connection OK");

            //Start a task to periodically check connection and set the variable connected
            ScheduledExecutorService scheduler =
                    Executors.newScheduledThreadPool(1);

            final Runnable pingConnectionTask = new Runnable() {
                int countOffline = 0;
                int disconnectAfter = 3;//Disconnect after n failed attempts

                public void run() {
                    connected = isStreamerUp();
                    if (!connected) {
                        countOffline++;
                        if (countOffline >= disconnectAfter) {
                            LOG.error("Failed to connect to streaming server for " + countOffline + " times. Shutting down Subscriber and turning on local price tracking.");
                            terminateAndStartLocalTracker();
                        }
                    } else {
                        countOffline = 0;
                    }
                }
            };

            pingHandle = scheduler.scheduleAtFixedRate(pingConnectionTask, 0, 30, SECONDS);

            getAuthMessage(context);

            context.term();
        } else {
            LOG.error("Streamer server down. Cannot start Streamer");
            terminateAndStartLocalTracker();
        }
    }

    private void terminateAndStartLocalTracker() {
        ((NuBotSecondary) Global.bot).startLocalPriceMonitor();
        Global.isSubscribed = false;
        terminate();
    }

    public boolean isStreamerUp() {
        boolean up = false;
        //  Socket to talk to server
        LOG.debug("Testing connection to server @ " + server);

        ZMQ.Socket requestFirstSocket = context.socket(ZMQ.REQ);
        String cstr = getSocketAddress(pingport);
        requestFirstSocket.connect(cstr);
        requestFirstSocket.setSendTimeOut(2000);
        requestFirstSocket.setReceiveTimeOut(2000);
        String request = "ping?";
        requestFirstSocket.send(request.getBytes(ZMQ.CHARSET), 0); //Flag was 0

        byte[] reply = requestFirstSocket.recv(0);
        //reply is null if no streamer
        //reply is '!pong' if streamer up
        if (reply == null) {
            up = false;
            LOG.warn("Streamer appear to be offline @ " + server);
        } else {
            String srep = new String(reply, ZMQ.CHARSET);
            LOG.debug("ping? : " + srep);
            if (srep.equals("pong!")) {
                up = true;
            }
        }

        requestFirstSocket.close();

        return up;
    }

    private String getSocketAddress(String port) {
        return "tcp://" + server + ":" + port;
    }


    public void terminate() {
        LOG.debug("Stopping ping process");
        pingHandle.cancel(false);
        //context.term();
        ScheduledExecutorService scheduler =
                Executors.newScheduledThreadPool(1);

        final Runnable terminatorTask = new Runnable() {
            public void run() {
                context.term();
            }
        };

        final ScheduledFuture<?> terminatorHandle =
                scheduler.schedule(terminatorTask, 6, SECONDS);
    }
}
