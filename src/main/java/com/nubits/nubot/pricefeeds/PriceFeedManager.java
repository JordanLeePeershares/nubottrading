/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds;

import com.nubits.nubot.models.Currency;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.feedservices.AbstractPriceFeed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Manager for a selected list of price feeds
 * see also Feeds, which manages all existing feeds
 */
public class PriceFeedManager {

    private static final Logger LOG = LoggerFactory.getLogger(PriceFeedManager.class.getName());
    //private AbstractPriceFeed mainfeed;
    private ArrayList<AbstractPriceFeed> feedList = new ArrayList<>();
    private CurrencyPair pair;

    private PriceBatch batch; //LastPriceResponse response;

    final int main_index = 0;

    public PriceFeedManager(String mainFeed, ArrayList<String> backupFeedList, CurrencyPair pair) throws NuBotConfigException {

        this.pair = pair;

        //Check that the feeds we are adding are available for the currency to track

        ArrayList<String> allowedFeeds = new ArrayList<>();
        try {
            allowedFeeds.add(FeedFacade.getDefaultMainFeed(pair));
            ArrayList<String> backupFeeds = FeedFacade.getDefaultBackupFeeds(pair);
            for (String feed : backupFeeds)
                allowedFeeds.add(feed);
        } catch (Exception e) {
            LOG.error("Can't get default feeds " + e.toString());
            throw new NuBotConfigException("error getting default feeds");
        }

        //Check if mainfeed is allowed
        boolean mainFeedOK = false;
        String allowedFeedStr = "";
        for (String allowed : allowedFeeds) {
            allowedFeedStr += "; " + allowed;
            if (mainFeed.equalsIgnoreCase(allowed)) {
                mainFeedOK = true;
            }
        }

        if (mainFeedOK) {
            feedList.add(FeedFacade.getFeed(mainFeed)); //add the main feed at index 0
        } else {
            LOG.error("mainFeed=" + mainFeed + " is not a valid entry for pair " + pair.toString() + " . List of allowed feeds : " + allowedFeedStr.substring(1));
            throw new NuBotConfigException("Error configuring mainFeed");
        }

        //Check if all backupfeeds are allowed
        for (String backup : backupFeedList) {
            boolean tempBackupValid = false;
            for (String allowed : allowedFeeds) {
                if (backup.equalsIgnoreCase(allowed)) {
                    tempBackupValid = true;
                    break;
                }
            }
            if (!tempBackupValid) {
                LOG.error("backupFeed=" + backup + " is not a valid entry for pair " + pair.toString() + " . List of allowed feeds : " + allowedFeedStr.substring(1));
                throw new NuBotConfigException("Error configuring backupfeeds");
            }
        }


        for (int i = 0; i < backupFeedList.size(); i++) {
            feedList.add(FeedFacade.getFeed(backupFeedList.get(i)));
        }
    }

    /**
     * trigger fetches from all feeds
     *
     * @return
     */
    public void fetchLastPrices() {

        LOG.debug("fetch last prices");

        batch = new PriceBatch();
        boolean isMainFeedValid = false;
        ArrayList<LastPrice> prices = new ArrayList<>();
        int i = 0;
        for (AbstractPriceFeed tempFeed : feedList) {
            LastPrice lastPrice = null;
            try {
                lastPrice = tempFeed.getLastPrice(pair);

                //Additional check to verify if the currency tracked is the same for all feeds
                Currency trackedCurrency = null;
                try {
                    trackedCurrency = Currency.getCurrencyToTrack(pair);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                LOG.debug("[" + trackedCurrency.getCode() + "] Obtained price : " + lastPrice.getPrice().getQuantity() + " from " + tempFeed.getClass().getSimpleName());
                prices.add(lastPrice);

                if (i == main_index) {
                    isMainFeedValid = true;
                }
            } catch (FeedCallException e) {
                LOG.error("Error while updating " + pair.getOrderCurrency().getCode() + ""
                        + " price from " + tempFeed.getClass() + " : " + e.toString());
            }
        }
        batch.setMainFeedValid(isMainFeedValid);
        batch.setPrices(prices);

    }


    public PriceBatch getPriceBatch() {
        return this.batch;
    }


    public ArrayList<AbstractPriceFeed> getFeedList() {
        return feedList;
    }


    public CurrencyPair getPair() {
        return pair;
    }

    public void setPair(CurrencyPair pair) {
        this.pair = pair;
    }

}
