/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 */
public class KrakenPriceFeed extends AbstractPriceFeed {
    private static final Logger LOG = LoggerFactory.getLogger(KrakenPriceFeed.class.getName());

    public KrakenPriceFeed() {
        refreshMinTime = Settings.FEEDFETCH_DEFAULT_INTERVAL;
        name = FeedFacade.KrakenFeed;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        if (pair.equals(CurrencyList.ETH_USD)) {
            //IF ETH USD, then first get XBTUSD String htmlString;

            try {
                double btcusdRate = getPriceImpl(generateTickerUrl(CurrencyList.BTC_USD));
                double etcbtcRate = getPriceImpl(generateTickerUrl(CurrencyList.ETH_BTC));

                double last = Utils.round(btcusdRate * etcbtcRate);

                LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(last, CurrencyList.USD));
                return tmp;

            } catch (FeedCallException e) {
                throw e;
            }

        } else if (pair.equals(CurrencyList.BTC_USD)) {
            try {
                double btcusdRate = getPriceImpl(generateTickerUrl(CurrencyList.BTC_USD));

                LastPrice tmp = new LastPrice(false, name, pair.getOrderCurrency(), new Amount(btcusdRate, CurrencyList.USD));
                return tmp;

            } catch (FeedCallException e) {
                throw e;
            }
        } else {
            throw new FeedCallException("Pair " + pair.toString() + " not supported by " + name);
        }


    }

    private double getPriceImpl(String url) throws FeedCallException {
        String htmlString = "";
        try {
            htmlString = Utils.getHTML(url, true);
        } catch (IOException ex) {
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price."));
        }

        JSONParser parser = new JSONParser();
        try {
            JSONObject httpAnswerJson = (JSONObject) (parser.parse(htmlString));
            JSONObject tickerObject = (JSONObject) httpAnswerJson.get("result");

            String seek = (String) (tickerObject.keySet().iterator().next());
            JSONObject intermediateObject = (JSONObject) tickerObject.get(seek);
            JSONArray lastObject = (JSONArray) intermediateObject.get("c");

            return Utils.getDouble(lastObject.get(0));

        } catch (Exception ex) {
            LOG.error(htmlString);
            LOG.error(ex.toString());
            throw new FeedCallException((this.getClass().getSimpleName() + " : Unable to fetch last price. (parseError)"));
        }
    }

    private String generateTickerUrl(CurrencyPair pair) {
        String pairStr = pair.toStringSepSpecial("").toUpperCase();
        if (pairStr.contains("BTC"))
            pairStr = pairStr.replace("BTC", "XBT");
        return "https://api.kraken.com/0/public/Ticker?pair=" + pairStr;
    }
}
