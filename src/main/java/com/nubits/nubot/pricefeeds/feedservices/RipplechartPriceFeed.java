/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.Amount;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import com.nubits.nubot.pricefeeds.FeedFacade;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 */
public class RipplechartPriceFeed extends AbstractPriceFeed {
    private static final Logger LOG = LoggerFactory.getLogger(RipplechartPriceFeed.class.getName());

    public RipplechartPriceFeed() {
        refreshMinTime = Settings.FEEDFETCH_DEFAULT_INTERVAL;
        name = FeedFacade.RipplechartsFeed;
    }

    @Override
    public LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException {
        //Documentation at https://ripple.com/build/charts-api/#exchange-rates
        try {
            String url = "https://api.ripplecharts.com/api/exchange_rates";


            String jsonPayload = "{\n" +
                    "  \"pairs\": [\n" +
                    "    {\n" +
                    "      \"base\": {\n" +
                    "        \"currency\": \"BTC\",\n" +
                    "        \"issuer\": \"rMwjYedjc7qqtKYVLiAccJSmCwih4LnE2q\"\n" +
                    "      },\n" +
                    "      \"counter\": {\n" +
                    "        \"currency\": \"XRP\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"base\": {\n" +
                    "        \"currency\": \"USD\",\n" +
                    "        \"issuer\": \"rMwjYedjc7qqtKYVLiAccJSmCwih4LnE2q\"\n" +
                    "      },\n" +
                    "      \"counter\": {\n" +
                    "        \"currency\": \"XRP\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"range\": \"day\"\n" +
                    "}";


            String result = Utils.sendJsonPOST(url, jsonPayload);
            JSONParser parser = new JSONParser();
            try {
                JSONArray httpAnswerJson = (org.json.simple.JSONArray) (parser.parse(result));

                String mainFeedBTCName = CurrencyList.BTC.getDefaultMainFeedName();
                AbstractPriceFeed mainFeedBTC = FeedFacade.getFeed(mainFeedBTCName);
                double btcusdRate = mainFeedBTC.getLastPrice(CurrencyList.BTC_USD).getPrice().getQuantity();

                double XRPUSD = getRate(httpAnswerJson, "USD");
                double XRPBTC = getRate(httpAnswerJson, "BTC") / btcusdRate;

                double average = (XRPUSD + XRPBTC) / 2;
                double last = Utils.round(1 / average);

                return new LastPrice(false, name, pair.getOrderCurrency(), new Amount(last, CurrencyList.USD));
            } catch (ParseException e) {
                throw new FeedCallException(e.toString());
            } catch (Exception e) {
                throw new FeedCallException(e.toString());
            }

        } catch (IOException e) {
            throw new FeedCallException(e.toString());
        }

    }

    private double getRate(JSONArray array, String currency) throws Exception {
        boolean found = false;
        for (int i = 0; i < array.size(); i++) {
            JSONObject tempObject = (JSONObject) array.get(i);
            JSONObject base = (JSONObject) tempObject.get("base");
            String tempCurrency = (String) base.get("currency");
            if (tempCurrency.equalsIgnoreCase(currency)) {
                found = true;
                return Utils.getDouble(tempObject.get("last"));
            }
        }

        if (!found) {
            throw new Exception("Can't find currency " + currency);
        }
        return 0;//never reached
    }


}
