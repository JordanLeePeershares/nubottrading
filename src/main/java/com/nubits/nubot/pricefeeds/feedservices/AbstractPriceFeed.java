/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.pricefeeds.feedservices;

import com.nubits.nubot.models.Currency;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.LastPrice;
import com.nubits.nubot.pricefeeds.FeedCallException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A price feed
 */
public abstract class AbstractPriceFeed {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractPriceFeed.class.getName());

    long refreshMinTime;
    long lastRequest;
    LastPrice lastPrice;
    public String name;
    Currency lastCurrency; //store here the last currency

    public abstract LastPrice forceFetchLastPrice(CurrencyPair pair) throws FeedCallException;

    public LastPrice getLastPrice(CurrencyPair pair) throws FeedCallException {
        long now = System.currentTimeMillis();
        long diff = now - lastRequest;

        CurrencyPair pairToTrack;
        try {
            pairToTrack = new CurrencyPair(Currency.getCurrencyToTrack(pair), CurrencyList.USD);
        } catch (Exception e) {
            LOG.error("Unable to forceFetchLastPrice " + e.toString());
            throw new FeedCallException("Unable to forceFetchLastPrice " + e.toString());
        }

        boolean sameCurrency = lastCurrency != null && lastCurrency.equals(pairToTrack.getOrderCurrency());

        if (diff >= refreshMinTime || !sameCurrency) {
            try {
                lastRequest = System.currentTimeMillis();
                lastPrice = forceFetchLastPrice(pairToTrack);
                lastCurrency = pairToTrack.getOrderCurrency();
            } catch (FeedCallException e) {
                LOG.error("Unable to forceFetchLastPrice " + e.toString());
                throw e;
            }
            //Additional check to verify if the currency tracked is the same for all feeds
            Currency toTrackCurrency = null;
            try {
                toTrackCurrency = Currency.getCurrencyToTrack(pair);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!lastPrice.getCurrencyMeasured().equals(toTrackCurrency)) {
                lastPrice.setError(true);
                LOG.error(this.getClass().getSimpleName() + " : Retrieved price of " + lastPrice.getCurrencyMeasured().getCode() + " while expecting " +
                        toTrackCurrency.getCode());
            }

            return lastPrice;

        } else {
            double t = (refreshMinTime - (System.currentTimeMillis() - lastRequest));
            LOG.warn("Wait " + t + " ms "
                    + "before making a new request on " + name + " for " + pairToTrack.toString() + " Now returning the last saved price\n\n");
            return lastPrice;
        }

    }


}
