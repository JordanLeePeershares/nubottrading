package com.nubits.nubot.utils;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.spi.FilterReply;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Settings;

import java.util.Arrays;
import java.util.List;

/**
 * Custom filter for Stdout
 * ERROR and WARN go into separate loggers
 */
public class LogCustomLevelFilterStdOut extends ch.qos.logback.core.filter.AbstractMatcherFilter {

    @Override
    public FilterReply decide(Object event) {

        LoggingEvent loggingEvent = (LoggingEvent) event;

        Level level = loggingEvent.getLevel();

        //always ignore trace
        if (loggingEvent.getLevel().equals(Level.TRACE))
            return FilterReply.DENY;

        //always ignore WARN and ERROR because these are separated
        if (loggingEvent.getLevel().equals(Level.WARN) || loggingEvent.getLevel().equals(Level.ERROR))
            return FilterReply.DENY;

        if (Global.options == null)
            return FilterReply.NEUTRAL;

        List<Level> h_levels = Arrays.asList(Level.DEBUG, Level.INFO, Level.WARN, Level.ERROR);
        List<Level> n_levels = Arrays.asList(Level.INFO, Level.WARN, Level.ERROR);
        List<Level> l_levels = Arrays.asList(Level.WARN, Level.ERROR);

        boolean ishigh = Global.options.verbosity.equalsIgnoreCase(Settings.LEVEL_HIGH);
        boolean ishnormal = Global.options.verbosity.equalsIgnoreCase(Settings.LEVEL_NORMAL);
        boolean islow = Global.options.verbosity.equalsIgnoreCase(Settings.LEVEL_LOW);

        //only filter if global.options is defined

        if (Global.options != null) {
            List<Level> selector = null;
            if (ishigh) {
                selector = h_levels;
            }
            if (ishnormal) {
                selector = n_levels;
            }
            if (islow) {
                selector = l_levels;
            }

            if (selector.contains(level))
                return FilterReply.NEUTRAL;
            else
                return FilterReply.DENY;

        }

        return FilterReply.DENY;

    }

}