/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


package com.nubits.nubot.utils;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.models.Trade;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeroturnaround.zip.ZipUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SaveTradesAndFinalize implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(SaveTradesAndFinalize.class.getName());

    private CurrencyPair pair;
    private long startTime;
    private String output;
    String logFolder;

    public SaveTradesAndFinalize(CurrencyPair pair, long startTime, String logFolder) {
        this.pair = pair;
        this.startTime = startTime;
        this.output = Global.sessionPath + "/" + Settings.EXECUTED_TRADES;
        this.logFolder = logFolder;
    }

    @Override
    public void run() {
        ApiResponse activeOrdersResponse = Global.trade.getLastTrades(pair, startTime);

        if (pair.getPaymentCurrency().equals(CurrencyList.NBT)) {
            Global.swappedPair = true;
        } else {
            Global.swappedPair = false;
        }

        int count = 0;
        int countSell = 0, countBuy = 0;
        double totalAmountPEG = 0;
        double totalAmountNBT = 0;
        double threshold = 1000; //NBT
        int countLargeOrders = 0;
        int paidInFees = 0;

        if (activeOrdersResponse.isPositive()) {
            ArrayList<Trade> tradeList = (ArrayList<Trade>) activeOrdersResponse.getResponseObject();
            FilesystemUtils.writeToFile("{\n", output, false);

            for (int i = 0; i < tradeList.size(); i++) {
                Trade tempTrade = tradeList.get(i);

                if (tempTrade.getType().equalsIgnoreCase(Constant.SELL)) {
                    countSell++;
                } else {
                    countBuy++;
                }

                count++;

                double amountNBT;

                if (Global.swappedPair) {
                    amountNBT = tempTrade.getAmount().getQuantity() * tempTrade.getPrice().getQuantity();
                    totalAmountNBT += amountNBT;
                    totalAmountPEG += tempTrade.getAmount().getQuantity();
                } else {
                    amountNBT = tempTrade.getAmount().getQuantity();
                    totalAmountPEG += amountNBT * tempTrade.getPrice().getQuantity();
                    totalAmountNBT += tempTrade.getAmount().getQuantity();
                }

                if (amountNBT >= threshold) {
                    countLargeOrders++;
                }

                paidInFees += tempTrade.getFee().getQuantity();
                String comma = ",\n";
                if (i == tradeList.size() - 1) {
                    comma = "";
                }
                FilesystemUtils.writeToFile(tempTrade.toJSONString() + comma, output, true);
            }

            FilesystemUtils.writeToFile("}", output, true);
        } else {
            LOG.error(activeOrdersResponse.getError().toString());
        }

        //Generate mini Report :

        String report = "Executed orders : " + count + " (sells : " + countSell + ";  buys : " + countBuy + ")"
                + "\nTotal volume transacted : " + totalAmountPEG + " BTC ; " + totalAmountNBT + " NBT )"
                + "\nOrders > " + threshold + " NBT : " + countLargeOrders
                + "\nPaid in fees : " + paidInFees;

        FilesystemUtils.writeToFile(report, output.replace(".json", "") + "_report.txt", false);

        //Zip Session log folder...
        ZipUtil.pack(new File(logFolder), new File(logFolder + ".zip"));
        //and delete directory to save up disk-space
        try {
            FileUtils.deleteDirectory(new File(logFolder));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
