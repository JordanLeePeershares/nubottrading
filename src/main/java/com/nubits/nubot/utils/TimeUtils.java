/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.utils;

import com.nubits.nubot.NTP.NTPClient;
import com.nubits.nubot.bot.SessionManager;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class TimeUtils {

    static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    public static Date GetUTCdatetimeAsDate() {
        return StringDateToDate(GetUTCdatetimeAsString());
    }

    public static String GetUTCdatetimeAsString() {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATEFORMAT);

        try {
            dateToReturn = (Date) dateFormat.parse(StrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }

    /**
     * @return
     */
    public static String getTimestampString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static long getTimestampLong() {
        Date timeStamp = new Date();
        return timeStamp.getTime();

    }


    public static long getOneDayInMillis() {
        return 1000 * 60 * 60 * 24;
    }

    //Computes the seconds missing till the next remote minutes clocks
    public static int getSecondsToRemoteMinute() {
        Date remoteDate = new NTPClient().getTime();
        Calendar remoteCalendar = new GregorianCalendar();
        remoteCalendar.setTime(remoteDate);
        int remoteTimeInSeconds = remoteCalendar.get(Calendar.SECOND);
        int delay = (60 - remoteTimeInSeconds);
        return delay;
    }

    public static String calcDate(long millisecs) {
        String timezone = "UTC";
        SimpleDateFormat date_format = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS");
        date_format.setTimeZone(TimeZone.getTimeZone("timezone"));
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate) + " " + timezone;

    }

    public static String getDurationDate(DateTime from, DateTime to) {
        Duration duration = new Duration(from, to);
        Period period = duration.toPeriod();
        Period normalizedPeriod = period.normalizedStandard();
        PeriodFormatter minutesAndSeconds = new PeriodFormatterBuilder()
                .appendDays()
                .appendSuffix(" day", " days")
                .appendSeparator(" ")
                .printZeroIfSupported()
                //.minimumPrintedDigits(2)
                .appendHours()
                .appendSuffix(" hour", " hours")
                .appendSeparator(" ")
                .appendMinutes()
                .appendSuffix(" minute", " minutes")
                .printZeroIfSupported()
                //.minimumPrintedDigits(2)
                .appendSeparator(" ")
                .appendSeconds()
                .appendSuffix(" second", " seconds")
                //.minimumPrintedDigits(2)
                .toFormatter();

                /*
                .printZeroAlways()
                .appendMinutes()
                .appendSeparator(":")
                .appendSeconds()
                .toFormatter();*/
        String result = minutesAndSeconds.print(normalizedPeriod);
        return result;
    }

    public static String getBotUptimeDate() {
        DateTime now = new DateTime();
        return getDurationDate(SessionManager.sessionStartDate, now);
    }

    public static double getHoursFromMillis(long millis) {
        return Utils.roundPlaces((getMinutesFromMillis(millis)) / 60, 2);
    }

    public static double getMinutesFromMillis(long millis) {
        return Utils.roundPlaces(((millis / 1000) / 60), 2);
    }

    public static double getDaysFromMillis(long millis) {
        return Utils.roundPlaces((getHoursFromMillis(millis)) / 24, 2);
    }

    public static String formatDuration(long seconds) {
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }


    public static String formatDate(Date dateIn, boolean timeZone) {
        DateFormat df = timeZone ? new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z") : new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

        String dstr = df.format(dateIn);
        return dstr;
    }

}
