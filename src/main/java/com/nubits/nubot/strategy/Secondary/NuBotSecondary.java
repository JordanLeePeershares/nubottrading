/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.strategy.Secondary;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.NuBotBase;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.launch.MainLaunch;
import com.nubits.nubot.models.Currency;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.PriceFeedManager;
import com.nubits.nubot.strategy.BalanceManager;
import com.nubits.nubot.strategy.OrderManager;
import com.nubits.nubot.streamclient.Subscriber;
import com.nubits.nubot.tasks.PriceMonitorTriggerTask;
import com.nubits.nubot.tasks.CheckOrdersTask;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * a NuBot implementing dual strategy
 */
public class NuBotSecondary extends NuBotBase {
    public ScheduledFuture<?> streamingSocketHandle;
    final static Logger LOG = LoggerFactory.getLogger(NuBotSecondary.class);

    @Override
    public void configureStrategy() throws NuBotConfigException {

        Global.balanceManager = new BalanceManager();
        Global.orderManager = new OrderManager();

        if (Global.options.isDualSide()) {
            LOG.info("Configuring NuBot for Dual-Side strategy");
        } else {
            LOG.info("Configuring NuBot for Sell-Side strategy");
        }

        //Force the a spread to avoid collisions
        if (Global.options.isMultipleOperators()) {
            double forceOffset = Settings.FORCED_OFFSET_TOTAL;
            double offset = Global.options.getBookBuyOffset() + Global.options.getBookSellOffset();
            if (offset < forceOffset) {
                Global.options.setBookBuyOffset(forceOffset / 2);
                Global.options.setBookSellOffset(forceOffset / 2);
                LOG.info("Forcing a " + forceOffset + "$ minimum spread to protect from collisions");
            }
        }

        Currency toTrackCurrency = null;
        try {
            toTrackCurrency = Currency.getCurrencyToTrack(Global.options.getPair());
        } catch (Exception e) {
            MainLaunch.exitWithNotice(e.toString());
        }

        CurrencyPair toTrackCurrencyPair = new CurrencyPair(toTrackCurrency, CurrencyList.USD);

        PriceMonitorTriggerTask pmTask = (PriceMonitorTriggerTask) Global.taskManager.getPriceTriggerTask().getTask();
        StrategySecondaryPegTask stratTask = (StrategySecondaryPegTask) (Global.taskManager.getSecondaryPegTask().getTask());

        // set trading strategy to the price monitor task
        pmTask.setStrategy(stratTask);

        // set price monitor task to the strategy
        stratTask.setPriceMonitorTask(pmTask);

        // set checkorder task to the strategy
        CheckOrdersTask liqTask = (CheckOrdersTask) Global.taskManager.getCheckOrdersTask().getTask();
        stratTask.setSendLiquidityTask(liqTask);

        PriceFeedManager pfm = null;
        try {
            pfm = new PriceFeedManager(Global.options.getMainFeed(), Global.options.getBackupFeeds(), toTrackCurrencyPair);
        } catch (NuBotConfigException e) {
            throw new NuBotConfigException("can't configure price feeds");
        } catch (Exception e) {
            LOG.error("" + Global.options);
            throw new NuBotConfigException("something wrong with options");
        }

        pmTask.setPriceFeedManager(pfm);

        //Set the wallet shift threshold
        pmTask.setWallchangeThreshold(Global.options.getWallchangeThreshold());


        if (Global.options.bypassStreaming) {
            startLocalPriceMonitor();
        } else {
            startRemoteStreamMonitor(toTrackCurrency);
        }
    }


    private void startRemoteStreamMonitor(Currency toTrackCurrency) {
        //Use the Streamer service to track prices
        StringTokenizer st = new StringTokenizer(Global.options.streamingserver, ":");
        String server = (String) st.nextElement();
        String mainPort = (String) st.nextElement();

        LOG.info("Plugging into websocket at " + server + ":" + mainPort);
        Global.subscriber = new Subscriber(server, mainPort, toTrackCurrency);

        if (Global.subscriber.isStreamerUp()) {


            ScheduledExecutorService scheduler =
                    Executors.newScheduledThreadPool(1);

            final Runnable startTask = new Runnable() {
                public void run() {
                    Global.subscriber.run();

                }
            };

            streamingSocketHandle = scheduler.schedule(startTask, 10, MILLISECONDS);
            return;
        } else {
            Global.isSubscribed = false;
            LOG.error("Streamer is offline, using local price monitor instead.");
            startLocalPriceMonitor();
        }
    }

    @Override
    public void terminateStreamingSocket() {
        if (streamingSocketHandle != null) {
            streamingSocketHandle.cancel(false);

            Global.subscriber.terminate();
        }
    }

    public void startLocalPriceMonitor() {
        //read the delay to sync with remote clock
        //issue 136 - multi custodians on a pair.
        //walls are removed and re-added every three minutes.
        //Bot needs to wait for next 3 min window before placing walls
        //set the interval from settings

        int reset_every = Settings.RESET_EVERY_MINUTES;

        int interval = 1;
        if (Global.options.isMultipleOperators()) {
            interval = 60 * reset_every;
        } else {
            interval = Settings.CHECK_PRICE_INTERVAL;
        }
        Global.taskManager.getPriceTriggerTask().setInterval(interval);

        int delaySeconds = 0;


        if (Global.options.isMultipleOperators()) {
            delaySeconds = Utils.getSecondsToNextwindow(reset_every);
            LOG.warn("NuBot will start running in " + delaySeconds + " seconds, to sync with remote NTP and place walls during next wall shift window.");
        } else {
            LOG.warn("NuBot will not try to sync with other bots via remote NTP : 'multiple-custodians' is set to false");
        }

        //then start the thread
        Global.taskManager.getPriceTriggerTask().start(delaySeconds);
    }
}
