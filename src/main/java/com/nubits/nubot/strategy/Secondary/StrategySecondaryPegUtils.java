/*
 * Copyright (C) 2014 desrever <desrever at nubits.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package com.nubits.nubot.strategy.Secondary;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.notifications.GitterNotifications;
import com.nubits.nubot.notifications.MailNotifications;
import com.nubits.nubot.trading.PreviewOrderbook;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class StrategySecondaryPegUtils {


    final static Logger LOG = LoggerFactory.getLogger(StrategySecondaryPegUtils.class);
    private final int MAX_RANDOM_WAIT_SECONDS = 5;
    private final int SHORT_WAIT_SECONDS = 6;
    private StrategySecondaryPegTask strategy;

    public StrategySecondaryPegUtils(StrategySecondaryPegTask strategy) {
        this.strategy = strategy;
    }

    public boolean reInitiateOrders(boolean firstTime) {
        strategy.setResettingOrders(true);
        LOG.debug("reInitiateOrders . firstTime=" + firstTime);

        if (SessionManager.sessionInterrupted()) return false; //external interruption
        //They are either 0 or need to be cancelled
        Global.orderManager.fetchOrders();
        int totalOrders = Global.orderManager.getNumTotalActiveOrders();
        if (totalOrders > 0) {
            //Delete all orders
            ApiResponse deleteOrdersResponse = Global.trade.clearOrders(Global.options.getPair());
            if (deleteOrdersResponse.isPositive()) {
                if (SessionManager.sessionInterrupted()) return false;
                boolean deleted = (boolean) deleteOrdersResponse.getResponseObject();
                if (deleted) {
                    LOG.info("Clear all orders request successfully");
                    if (firstTime) //update the initial balance of the secondary peg
                    {
                        Global.frozenBalancesManager.setBalanceAlreadyThere(Global.options.getPair().getPaymentCurrency());
                    }
                    //Wait until there are no active orders
                    boolean timedOut = false;
                    long timeout = Global.options.getEmergencyTimeout() * 1000;
                    long wait = SHORT_WAIT_SECONDS * 1000;
                    long count = 0L;

                    boolean areAllOrdersCanceled = false;
                    do {
                        try {
                            if (SessionManager.sessionInterrupted()) return false;
                            Thread.sleep(wait);
                            areAllOrdersCanceled = Global.orderManager.tryCancelAllOrders(Global.options.getPair());
                            if (areAllOrdersCanceled) {
                                LOG.warn("All orders canceled successfully");
                            } else {
                                LOG.error("There was a problem cancelling the orders (count: " + count + ")");
                            }

                            count += wait;
                            timedOut = count > timeout;
                            if (SessionManager.sessionInterrupted()) return false;
                        } catch (InterruptedException ex) {
                            LOG.error(ex.toString());
                        }
                    } while (!areAllOrdersCanceled && !timedOut);

                    if (timedOut) {
                        String message = "There was a problem cancelling all existing orders (timeout:" + timeout + ", count: " + count + ")";
                        LOG.error(message);
                        GitterNotifications.sendMessage(message);
                        MailNotifications.send(Global.options.getMailRecipient(), "NuBot : Problem cancelling existing orders", message);
                        //Continue anyway, maybe there is some balance to put up on order.
                    }
                    //Update the balance
                    placeInitialWalls();
                } else {
                    String message = "Could not submit request to clear orders";
                    LOG.error(message);
                    strategy.setResettingOrders(false);
                    return false;
                }

            } else {
                LOG.error(deleteOrdersResponse.getError().toString());
                String message = "Could not submit request to clear orders";
                LOG.error(message);
                strategy.setResettingOrders(false);
                return false;
            }
        } else {
            if (firstTime) //update the initial balance of the secondary peg
            {
                Global.frozenBalancesManager.setBalanceAlreadyThere(Global.options.getPair().getPaymentCurrency());
            }
            placeInitialWalls();
        }
        try {
            Thread.sleep(SHORT_WAIT_SECONDS); //Give the time to new orders to be placed before counting again
        } catch (InterruptedException ex) {
            LOG.error(ex.toString());
        }
        strategy.setResettingOrders(false);
        return true;
    }

    public void placeInitialWalls() {
        double pegPrice = Global.conversion;
        if (SessionManager.sessionInterrupted()) return;
        boolean buysOrdersOk = true;
        if (SessionManager.sessionInterrupted()) return;
        LOG.debug("init sell orders.");
        boolean sellsOrdersOk = initOrders(Constant.SELL, pegPrice);
        if (SessionManager.sessionInterrupted()) return;
        if (Global.options.isDualSide()) {
            if (SessionManager.sessionInterrupted()) return;
            LOG.debug("init buy orders.");
            buysOrdersOk = initOrders(Constant.BUY, pegPrice);
            if (SessionManager.sessionInterrupted()) return;
        }

        if (buysOrdersOk && sellsOrdersOk) {
            strategy.setMightNeedInit(false);
            LOG.info("Strategy initialization completed");
        } else {
            strategy.setMightNeedInit(true);
        }
    }

    private Currency getCurrency(String type) {
        Currency currency;
        if (!Global.swappedPair) {
            if (type.equals(Constant.SELL)) {
                currency = Global.options.getPair().getOrderCurrency();
            } else {
                currency = Global.options.getPair().getPaymentCurrency();
            }
        } else {
            if (type.equals(Constant.SELL)) {
                currency = Global.options.getPair().getPaymentCurrency();
            } else {
                currency = Global.options.getPair().getOrderCurrency();
            }
        }

        return currency;
    }


    public boolean initOrders(String type, double pegPrice) {
        if (SessionManager.sessionInterrupted()) return false;
        if (!strategy.isInitializingOrders(type)) {
            strategy.setInitializingOrders(type, true);
            LOG.info("initOrders " + type);

            boolean success = true;
            Amount balance = null;
            //Update the available balance
            Currency currency = getCurrency(type);

            ApiResponse balancesResponse = Global.trade.getAvailableBalance(currency);
            if (!balancesResponse.isPositive()) {
                LOG.error(balancesResponse.getError().toString());
                strategy.setInitializingOrders(type, false);
                return false;
            }
            if (SessionManager.sessionInterrupted()) return false;

            double oneNBT = 1;
            if (type.equals(Constant.SELL)) {
                balance = (Amount) balancesResponse.getResponseObject();
            } else {
                //Here its time to compute the balance to put apart, if any
                balance = (Amount) balancesResponse.getResponseObject();
                balance = Global.frozenBalancesManager.removeFrozenAmount(balance, Global.frozenBalancesManager.getFrozenAmount());
                oneNBT = Utils.round(1 / Global.conversion);
            }
            if (SessionManager.sessionInterrupted()) return false;

            if (balance.getQuantity() < oneNBT * 2) {
                LOG.info("No need to execute " + type + " orders : available balance value is less than 2 NBT.  Balance : " + Utils.formatNumber(balance.getQuantity(), Settings.DEFAULT_PRECISION));
                strategy.setInitializingOrders(type, false);
                return true;
            }


            CurrencyPair pair = CurrencyPair.getCurrencyPairFromString(Global.options.pair);

            //Update TX fee :
            //Get the current transaction fee associated with a specific CurrencyPair
            ApiResponse txFeeNTBPEGResponse = Global.trade.getTxFee(Global.options.getPair());
            if (SessionManager.sessionInterrupted()) return false;


            if (txFeeNTBPEGResponse.isPositive()) {
                double txFeePEGNTB = (Double) txFeeNTBPEGResponse.getResponseObject();
                LOG.trace("Updated Transaction fee = " + txFeePEGNTB + "%");

                double cap = type.equals(Constant.SELL) ? Global.options.bookSellMaxVolumeCumulative : Global.options.bookBuyMaxVolumeCumulative;

                OrderBatch batch = Global.ldm.getOrdersToPlace(type, balance, pegPrice, pair, txFeePEGNTB, cap, Global.options.bookDisabletier2);
                ArrayList<OrderToPlace> ordersToPlace = batch.getOrders();
                String orderBookInfo = PreviewOrderbook.getOrderBookStats(ordersToPlace, type, Global.ldm.computeWallHeight(type, balance, pegPrice), pegPrice, balance.getQuantity());
                LOG.info(type + "- OrderBook : " + orderBookInfo);
                LOG.info("Trying to place " + ordersToPlace.size() + " orders...");
                if (SessionManager.sessionInterrupted()) return false;
                ApiResponse placeOrdersResponse = Global.trade.placeOrders(batch, pair);
                if (SessionManager.sessionInterrupted()) return false;
                if (placeOrdersResponse.isPositive()) {
                    MultipleOrdersResponse multipleOrdersResponse = (MultipleOrdersResponse) placeOrdersResponse.getResponseObject();
                    LOG.debug("Orders correctly placed list : -------- \n");
                    Global.orderManager.addToTotalOrdersSubmitted(multipleOrdersResponse.getOrdersPlacedSuccessfully().size());

                    int succesfullyPlaced = multipleOrdersResponse.getOrdersPlacedSuccessfully().size();
                    String t1OrderId = type.equals(Constant.SELL) ? Global.sellWallOrderID : Global.buyWallOrderID;
                    int tier2Orders = multipleOrdersResponse.getTier1OrderSize(t1OrderId) == 0 ? succesfullyPlaced : succesfullyPlaced - 1;

                    String msg = "**" + type + "** orders re-initialized on  **" + Global.options.getExchangeName() + "** :  "
                            + succesfullyPlaced + "/" + ordersToPlace.size() + " placed successfully\n" +
                            "total amount placed : " + multipleOrdersResponse.getTotalAmountPlaced() + "\n" +
                            "Tier1 order size : " + multipleOrdersResponse.getTier1OrderSize(t1OrderId) + "\n" +
                            "Tier2 cumulative order size : " + multipleOrdersResponse.getTier2CumulativeOrdersSize(t1OrderId) + " (" + tier2Orders + " orders) \n";
                    LOG.warn(msg);
                    GitterNotifications.sendMessage(msg);
                    multipleOrdersResponse.printOrdersPlacedSuccessfully();
                } else {
                    LOG.error(placeOrdersResponse.getError().toString());
                }
            } else {
                LOG.error("Error while reading the tx fee " + txFeeNTBPEGResponse.getError().toString());
                success = false;
            }
            strategy.setInitializingOrders(type, false);
            return success;
        } else {
            LOG.warn("Strategy is already initializing " + type + " orders. Cannot execute.");
            return false;
        }
    }


    public void aggregateAndKeepProceeds() {
        if (SessionManager.sessionInterrupted()) return; //external interruption

        LOG.info("aggregateAndKeepProceeds");

        boolean cancel = Global.orderManager.takeDownOrders(Constant.BUY, Global.options.getPair());
        if (!cancel) {
            LOG.error("An error occurred while attempting to cancel buy orders.");
            return;
        }

        //get the balance and see if it does still require an aggregation

        Global.frozenBalancesManager.freezeNewFunds();

        //Introuce an aleatory sleep time to desync bots at the time of placing orders.
        //This will favour competition in markets with multiple custodians
        try {
            Thread.sleep(Utils.randInt(0, MAX_RANDOM_WAIT_SECONDS) * 1000);
        } catch (InterruptedException ex) {
            LOG.error(ex.toString());
        }

        LOG.info("init buy orders.");
        initOrders(Constant.BUY, Global.conversion);
    }

    public boolean shiftWalls() {
        if (SessionManager.sessionInterrupted()) return false; //external interruption

        if (strategy.isResettingOrders()) {
            LOG.warn("Cannot shift walls, orders are being reinitiated. Price is already adjusted.");
            return false;
        }

        LOG.debug("Executing shiftWalls()");

        boolean success = true;

        //Communicate to the priceMonitorTask that a wall shift is in place
        strategy.getPriceMonitorTask().setWallsBeingShifted(true);
        strategy.getSendLiquidityTask().setWallsBeingShifted(true);

        //fix prices, so that if they change during wait time, this wall shift is not affected.
        double pegPrice = Global.conversion;


        LOG.info("Immediately try to cancel all orders");
        if (SessionManager.sessionInterrupted()) return false; //external interruption

        //immediately try to : cancel all active orders
        ApiResponse deleteOrdersResponse = Global.trade.clearOrders(Global.options.getPair());
        if (SessionManager.sessionInterrupted()) return false; //external interruption

        if (deleteOrdersResponse.isPositive()) {
            boolean deleted = (boolean) deleteOrdersResponse.getResponseObject();
            if (deleted) {
                LOG.warn("Orders deleted");
                if (Global.options.isMultipleOperators() || Global.isSubscribed) {
                    //Introuce an aleatory sleep time to desync bots at the time of placing orders.
                    //This will favour competition in markets with multiple custodians
                    if (SessionManager.sessionInterrupted()) return false; //external interruption
                    try {
                        Thread.sleep(SHORT_WAIT_SECONDS + Utils.randInt(0, MAX_RANDOM_WAIT_SECONDS) * 1000); //SHORT_WAIT_SECONDS gives the time to other bots to take down their order
                    } catch (InterruptedException ex) {
                        LOG.error(ex.toString());
                    }
                }
                if (SessionManager.sessionInterrupted()) return false; //external interruption

                //Update frozen balances
                if (!Global.options.isDualSide() //Do not do this for sell side custodians or...
                        || !Global.options.getPair().getPaymentCurrency().isFiat()) //...do not do this for stable secondary pegs (e.g EUR)
                {
                    // update the initial balance of the secondary peg
                    Global.frozenBalancesManager.freezeNewFunds();
                }


                if (SessionManager.sessionInterrupted()) return false; //external interruption

                //Reset sell side orders
                boolean initSells = initOrders(Constant.SELL, pegPrice); //Force init sell orders

                if (!initSells) {
                    success = false;
                }

                if (initSells) { //Only move the buy orders if sure that the sell have been taken down
                    if (Global.options.isDualSide()) {
                        boolean initBuys;
                        if (SessionManager.sessionInterrupted()) return false; //external interruption

                        initBuys = initOrders(Constant.BUY, pegPrice);
                        if (!initBuys) {
                            success = false;
                            LOG.error("NuBot has not been able to shift buy orders");
                        }
                        if (SessionManager.sessionInterrupted()) return false; //external interruption

                    }
                } else { //success false with the first part of the shift
                    LOG.error("NuBot has not been able to shift sell orders");
                }
            } else {
                LOG.error("Coudn't delete orders ");
            }

            //Here I wait until the two orders are correctly displaied. It can take some seconds
            try {
                Thread.sleep(SHORT_WAIT_SECONDS * 2000);
            } catch (InterruptedException ex) {
                LOG.error(ex.toString());
            }

            //Communicate to the priceMonitorTask that the wall shift is over
            strategy.getPriceMonitorTask().setWallsBeingShifted(false);
            strategy.getSendLiquidityTask().setWallsBeingShifted(false);

        } else {
            LOG.info("Could not submit request to clear orders");
            success = false;
            //Communicate to the priceMonitorTask that the wall shift is over
            strategy.getPriceMonitorTask().setWallsBeingShifted(false);
            strategy.getSendLiquidityTask().setWallsBeingShifted(false);
            LOG.error(deleteOrdersResponse.getError().toString());
        }

        return success;
    }


    /*
    * Deprecated.
    * This method was used before nubot 0.3.2, placing two orders per side.
    */
    public boolean initOrders_DEPRECATED(String type, double price) {
        if (SessionManager.sessionInterrupted()) return false;

        LOG.info("initOrders " + type + ", wall price " + price);

        boolean success = true;
        Amount balance = null;
        //Update the available balance
        Currency currency = getCurrency(type);

        ApiResponse balancesResponse = Global.trade.getAvailableBalance(currency);
        if (!balancesResponse.isPositive()) {
            LOG.error(balancesResponse.getError().toString());
            return false;
        }
        if (SessionManager.sessionInterrupted()) return false;

        double oneNBT = 1;
        if (type.equals(Constant.SELL)) {
            balance = (Amount) balancesResponse.getResponseObject();
        } else {
            //Here its time to compute the balance to put apart, if any
            balance = (Amount) balancesResponse.getResponseObject();
            balance = Global.frozenBalancesManager.removeFrozenAmount(balance, Global.frozenBalancesManager.getFrozenAmount());
            oneNBT = Utils.round(1 / Global.conversion);
        }
        if (SessionManager.sessionInterrupted()) return false;

        if (balance.getQuantity() < oneNBT * 2) {
            LOG.info("No need to execute " + type + " orders : available balance value is less than 2 NBT.  Balance : " + balance.getQuantity());
            return true;
        }

        //Update TX fee :
        //Get the current transaction fee associated with a specific CurrencyPair
        ApiResponse txFeeNTBPEGResponse = Global.trade.getTxFee(Global.options.getPair());
        if (SessionManager.sessionInterrupted()) return false;

        //short hand variables
        double sellWallHeight = Global.options.getBookSellwall();
        double buyWallHeight = Global.options.getBookBuywall();

        LOG.debug("balance " + balance + " maxBuy " + buyWallHeight + ". maxSell " + buyWallHeight);

        if (txFeeNTBPEGResponse.isPositive()) {
            double txFeePEGNTB = (Double) txFeeNTBPEGResponse.getResponseObject();
            LOG.trace("Updated Transaction fee = " + txFeePEGNTB + "%");

            double amount1 = Utils.round(balance.getQuantity() / 2);
            LOG.debug("amount1: " + amount1 + " . balance " + balance.getQuantity());
            //check the calculated amount against the set maximum sell amount set in the options.json file


            if (sellWallHeight > 0 && type.equals(Constant.SELL)) {
                if (amount1 > (sellWallHeight / 2))
                    amount1 = (sellWallHeight / 2);
            }

            if (type.equals(Constant.BUY) && !Global.swappedPair) {
                amount1 = Utils.round(amount1 / price);
                LOG.debug("buy: => amount " + amount1);
                //check the calculated amount against the max buy amount option, if any.
                if (buyWallHeight > 0) {
                    if (amount1 > (buyWallHeight / 2))
                        amount1 = (buyWallHeight / 2);
                }

            }
            if (SessionManager.sessionInterrupted()) return false;

            success = Global.orderManager.executeOrder(type, Global.options.getPair(), amount1, price);
            if (!success)
                return false;

            //wait a while to give the time to the new amount to update

            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException ex) {
                LOG.error(ex.toString());
            }
            if (SessionManager.sessionInterrupted()) return false;

            //read balance again
            ApiResponse balancesResponse2 = Global.trade.getAvailableBalance(currency);
            if (balancesResponse2.isPositive()) {

                balance = (Amount) balancesResponse2.getResponseObject();

                if (type.equals(Constant.BUY)) {
                    balance = Global.frozenBalancesManager.removeFrozenAmount(balance, Global.frozenBalancesManager.getFrozenAmount());
                }


                double amount2 = balance.getQuantity();

                //check the calculated amount against the set maximum sell amount set in the options.json file

                if (type.equals(Constant.SELL) && sellWallHeight > 0) {
                    if (amount2 > (sellWallHeight / 2))
                        amount2 = sellWallHeight / 2;
                }

                if ((type.equals(Constant.BUY) && !Global.swappedPair)
                        || (type.equals(Constant.SELL) && Global.swappedPair)) {
                    //hotfix
                    amount2 = Utils.round(amount2 - (oneNBT * 0.9)); //multiply by .9 to keep it below one NBT
                    amount2 = Utils.round(amount2 / price);

                    //check the calculated amount against the max buy amount option, if any.
                    if (buyWallHeight > 0) {
                        if (amount2 > (buyWallHeight / 2))
                            amount2 = (buyWallHeight / 2);
                    }

                }

                //execute second order
                if (SessionManager.sessionInterrupted()) return false;

                success = Global.orderManager.executeOrder(type, Global.options.getPair(), amount2, price);
                if (!success)
                    return false;

            } else {
                LOG.error("Error while reading the balance the second time " + balancesResponse2.getError().toString());
                success = false;
            }
        }

        return success;
    }

    public void checkBalancesAndOrders() {
        strategy.setOrdersAndBalancesOK(true);

        double t1SellLiquidity = PairBalance.getTier1Liquidity(Constant.SELL);
        LOG.info("Sell-Wall size :  : " + t1SellLiquidity + " NBT");
        wallStatusAction(t1SellLiquidity, Global.sellWallOrderSize, Constant.SELL);

        if (Global.options.isDualSide()) {
            double t1BuyLiquidity = PairBalance.getTier1Liquidity(Constant.BUY);
            LOG.info("Buy-Wall size :  : " + t1BuyLiquidity + " NBT");
            wallStatusAction(t1BuyLiquidity, Global.buyWallOrderSize, Constant.BUY);
        }


        if (SessionManager.sessionInterrupted()) return; //external interruption

        if (Global.options.getKeepProceeds() > 0 && Global.options.getPair().getPaymentCurrency().isFiat()) {
            checkProceeds();
        }
    }

    /*
    wallH is in NBT
     */
    private void wallStatusAction(double wallH, double initialSize, String type) {

        double minutesAllowedUndertreshold = Settings.ALLOW_T1LIQUIDITY_UNDERTRESHOLD_MINUES_MIDHRISK;
        if (Global.options.getPair().getPaymentCurrency().isFiat()) {
            minutesAllowedUndertreshold = Settings.ALLOW_T1LIQUIDITY_UNDERTRESHOLD_MINUES_HIGHRISK;
        }

        //TODO
        //1. Make it a function of exchange volume
        //2. Make it a fucntion of how many times t1 it needed to be replenished recently

        double declaredWallHInOptions = 0;
        String wallOrderID = "";
        if (type.equals(Constant.SELL)) {
            declaredWallHInOptions = Global.options.getBookSellwall();
            wallOrderID = Global.sellWallOrderID;
        } else {
            declaredWallHInOptions = Global.options.getBookBuywall();
            wallOrderID = Global.buyWallOrderID;

        }

        if (declaredWallHInOptions != 0) //t1 liquidity is declared to be allowed to stay at 0
        {
            if (!wallOrderID.equals(""))  //never placed in a first place
            {
                double treshold = initialSize * Settings.T1LIQUIDITY_PERCENTAGE_THRESHOLD;
                LOG.debug(type + "-wallh: " + wallH + " initialH:" + initialSize + " threshold : " + treshold);
                if (wallH < treshold) {
                    if (strategy.getT1underthresholdTimestamp(type) == 0) {
                        strategy.setT1underthresholdTimestamp(type, System.currentTimeMillis());
                    }
                    long msUnderTreshold = System.currentTimeMillis() - strategy.getT1underthresholdTimestamp(type);
                    double minutesUnderTreshold = msUnderTreshold / 1000 / 60;
                    LOG.debug(type + " Liquidity below threshold for " + minutesUnderTreshold + "/" + minutesAllowedUndertreshold + " min");

                    if (minutesUnderTreshold >= minutesAllowedUndertreshold) {
                        //Need to replenish tiers, reset orders
                        if (!strategy.isResettingOrders() && !strategy.isInitializingOrders(type)) {
                            LOG.warn("Replenishing " + type + " wall. Since its size has been < " + treshold + " NBT for more than " + minutesAllowedUndertreshold + " minutes.");
                            initOrders(type, Global.conversion); //Replenish T1
                        } else {
                            LOG.warn("Cannot replenish " + type + " T1 now, orders already resetting/reinitiating. Resetting timer");
                        }
                        strategy.setT1underthresholdTimestamp(type, 0); //Reset timer either way, t1 should be back
                    }
                } else {
                    strategy.setT1underthresholdTimestamp(type, 0);
                }
            }
        }

    }

    private void checkProceeds() {
        ApiResponse balancesResponse = Global.trade.getAvailableBalances(Global.options.getPair());
        if (balancesResponse.isPositive()) {
            PairBalance balance = (PairBalance) balancesResponse.getResponseObject();
            double balancePEG = (Global.frozenBalancesManager.removeFrozenAmount(balance.getPEGAvailableBalance(), Global.frozenBalancesManager.getFrozenAmount())).getQuantity();

            double twoNBT = Utils.round(2 / Global.conversion);
            if (SessionManager.sessionInterrupted()) return; //external interruption
            if (Global.options.isDualSide()) {
                if (balancePEG > twoNBT
                        && Global.options.getPair().getPaymentCurrency().isFiat()
                        && !strategy.isFirstTime()
                        && Global.options.getBookBuywall() != 0) { //Only for EUR...CNY etc
                    LOG.warn("The " + balance.getPEGAvailableBalance().getCurrency().getCode() + " balance is not zero (" + balancePEG + " ). If the balance represent proceedings "
                            + "from a sale the bot will notice.  On the other hand, If you keep seying this message repeatedly over and over, you should restart the bot. ");
                    strategy.setProceedsInBalance(true);
                } else {
                    strategy.setProceedsInBalance(false);
                }
            }
        } else {
            LOG.error(balancesResponse.getError().toString());
        }
    }
    /*
    * Deprecated.
    * This method was used before nubot 0.3.2, placing two orders per side.
    */

    public void recount_DEPRECATED() {
        if (SessionManager.sessionInterrupted()) return; //external interruption

        ApiResponse balancesResponse = Global.trade.getAvailableBalances(Global.options.getPair());
        if (balancesResponse.isPositive()) {
            PairBalance balance = (PairBalance) balancesResponse.getResponseObject();
            double balanceNBT = balance.getNBTAvailable().getQuantity();
            double balancePEG = (Global.frozenBalancesManager.removeFrozenAmount(balance.getPEGAvailableBalance(), Global.frozenBalancesManager.getFrozenAmount())).getQuantity();

            strategy.setOrdersAndBalancesOK(false);

            double twoNBT = Utils.round(2 / Global.conversion);
            if (SessionManager.sessionInterrupted()) return; //external interruption

            Global.orderManager.fetchOrders();
            int activeSellOrders = Global.orderManager.getNumActiveSellOrders();
            int activeBuyOrders = Global.orderManager.getNumActiveBuyOrders();

            if (Global.options.isDualSide()) {

                boolean correctOrders = (activeSellOrders == 2 && activeBuyOrders == 2)
                        || (activeSellOrders == 2 && activeBuyOrders == 0 && balancePEG < twoNBT)
                        || (activeSellOrders == 0 && activeBuyOrders == 2 && balanceNBT <= 2)
                        || (activeSellOrders == 0 && activeBuyOrders == 0 && balanceNBT <= 2 && balancePEG <= twoNBT);
                LOG.debug("correct orders: " + correctOrders);
                strategy.setOrdersAndBalancesOK(correctOrders);

                if (balancePEG > twoNBT
                        && Global.options.getPair().getPaymentCurrency().isFiat()
                        && !strategy.isFirstTime()
                        && Global.options.getBookBuywall() != 0) { //Only for EUR...CNY etc
                    LOG.warn("The " + balance.getPEGAvailableBalance().getCurrency().getCode() + " balance is not zero (" + balancePEG + " ). If the balance represent proceedings "
                            + "from a sale the bot will notice.  On the other hand, If you keep seying this message repeatedly over and over, you should restart the bot. ");
                    strategy.setProceedsInBalance(true);
                } else {
                    strategy.setProceedsInBalance(false);
                }
            } else {
                boolean correctOrders = (activeSellOrders == 2 && activeBuyOrders == 0)
                        || (activeSellOrders == 0 && activeBuyOrders == 0 && balanceNBT < 1);
                LOG.debug("correctOrders: " + correctOrders);
                strategy.setOrdersAndBalancesOK(correctOrders); // Ignore the balance
            }
        } else {
            LOG.error(balancesResponse.getError().toString());
        }
    }


}