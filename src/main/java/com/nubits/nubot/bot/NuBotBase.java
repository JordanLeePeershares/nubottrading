/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.bot;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.joran.util.ConfigurationWatchListUtil;
import com.nubits.nubot.ALPClient.ALPService;
import com.nubits.nubot.ALPClient.ALPStatsManager;
import com.nubits.nubot.RPC.NudClient;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.exchanges.ExchangeLiveData;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.launch.MainLaunch;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.notifications.GitterNotifications;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.tasks.TaskManager;
import com.nubits.nubot.utils.SaveTradesAndFinalize;
import com.nubits.nubot.trading.TradeInterface;
import com.nubits.nubot.trading.keys.ApiKeys;
import com.nubits.nubot.trading.wrappers.CcexWrapper;
import com.nubits.nubot.utils.*;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * Abstract NuBot. implements all primitives without the strategy itself
 */
public abstract class NuBotBase {

    /**
     * the strategy setup for specific NuBots to implement
     */
    abstract public void configureStrategy() throws NuBotConfigException;

    abstract public void terminateStreamingSocket();


    final static Logger LOG = LoggerFactory.getLogger(NuBotBase.class);

    protected String mode;

    protected boolean liveTrading;


    /**
     * all setups
     */
    protected void setupAllConfig() {

        //Generate Bot Session unique id
        SessionManager.setSessionId(Utils.generateSessionID());

        //setSub-logging folder
        Global.defineLogPath(Settings.SESSIONS_LOGFOLDER + "/" + SessionManager.getSessionId());

        LOG.info("Session ID = " + SessionManager.getSessionId());

        this.mode = "sell-side";
        if (Global.options.isDualSide()) {
            this.mode = "dual-side";
        }

        setupLog();

        setupSSL();

        setupExchange();


    }


    /**
     * setup pool mode
     */
    private void setupPoolMode() {
        LOG.info("Pool-mode : trying to register to ALP server (" + Global.options.poolURI + ") with key :" + Global.options.apiKey);

        //Setup the ALPService object
        Global.alpManager = new ALPService(Global.options.poolURI);
        Global.alpStatsManager = new ALPStatsManager(Global.alpManager);

        //Try to register to ALP--------------------------------------------
        ApiResponse registerResponse = Global.alpManager.register(Global.options.apiKey,
                Global.options.poolPayoutAddress,
                Global.options.exchangeName,
                Global.options.getPair().getPaymentCurrency().getCode().toLowerCase()
        );

        if (registerResponse.isPositive()) {
            LOG.debug("Positive answer from alpManager.register: " + registerResponse.getResponseObject().toString());


            LOG.debug("exchange: " + Global.options.exchangeName + "\n" +
                    "poolURI: " + Global.options.poolURI + "\n" +
                    "apiKey: " + Global.options.apiKey + "\n" +
                    "poolPayoutAddress: " + Global.options.poolPayoutAddress + "\n" +
                    "unit: " + Global.options.getPair().getPaymentCurrency().getCode().toLowerCase());

            LOG.warn("Pool-mode activated, user registered on server.");

            Global.taskManager.getALPTask().start(Settings.CHECK_PRICE_INTERVAL * 2); //Schedule task to submit ALPinfo

        } else {
            MainLaunch.exitWithNotice("Cannot register with ALP \nexchange: " + Global.options.exchangeName + "\n" +
                    "poolURI: " + Global.options.poolURI + "\n" +
                    "apiKey: " + Global.options.apiKey + "\n" +
                    "poolPayoutAddress: " + Global.options.poolPayoutAddress + "\n" +
                    "unit: " + Global.options.getPair().getPaymentCurrency().getCode().toLowerCase() +
                    "\nTry disabling pool-mode or review your ALP configuration. \n\nError from alpService.register: "
                    + registerResponse.getError().toString());
        }
    }


    /**
     * setup logging
     */
    protected void setupLog() {

        //for debug purposes: determine the logback.xml file used
        LoggerContext loggerContext = ((ch.qos.logback.classic.Logger) LOG).getLoggerContext();
        URL mainURL = ConfigurationWatchListUtil.getMainWatchURL(loggerContext);
        LOG.debug("Logback used '{}' as the configuration file.", mainURL);

        //Disable hipchat debug logging https://github.com/evanwong/hipchat-java/issues/16
        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "error");

        List<ch.qos.logback.classic.Logger> llist = loggerContext.getLoggerList();

        Iterator<ch.qos.logback.classic.Logger> it = llist.iterator();
        while (it.hasNext()) {
            ch.qos.logback.classic.Logger l = it.next();
            LOG.trace("" + l);
        }
    }

    protected void setupSSL() {
        LOG.info("Set up SSL certificates");
        boolean trustAllCertificates = false;
        if (Global.options.getExchangeName().equalsIgnoreCase(ExchangeFacade.INTERNAL_EXCHANGE_PEATIO)) {
            trustAllCertificates = true;
        }
        Utils.installKeystore(trustAllCertificates);
    }


    protected void setupExchange() {

        LOG.debug("setup Exchange object");

        LOG.debug("Wrap the keys into a new ApiKeys object");
        ApiKeys keys = new ApiKeys(Global.options.getApiSecret(), Global.options.getApiKey());

        TradeInterface ti = null;
        try {
            ti = ExchangeFacade.getInterfaceByName(Global.options.getExchangeName(), keys);
        } catch (Exception e) {
            MainLaunch.exitWithNotice("exchange unknown");
        }
        Global.trade = ti;
        Global.exchangeLiveData = new ExchangeLiveData();


        //TODO handle on exchange level, not bot level
        if (Global.options.getExchangeName().equals(ExchangeFacade.CCEX)) {
            ((CcexWrapper) (ti)).initBaseUrl();
        }

        if (Global.options.getPair().getPaymentCurrency().equals(CurrencyList.NBT)) {
            Global.swappedPair = true;
        } else {
            Global.swappedPair = false;
        }

        LOG.info("Swapped pair mode : " + Global.swappedPair);

        String apibase = "";
        //TODO handle on exchange level, not bot level
        if (Global.options.getExchangeName().equalsIgnoreCase(ExchangeFacade.INTERNAL_EXCHANGE_PEATIO)) {
            ti.setApiBaseUrl(ExchangeFacade.INTERNAL_EXCHANGE_PEATIO_API_BASE);
        }

    }

    protected void checkNuConn() throws NuBotConnectionException {
        if (Global.rpcClient.isConnected()) {
            LOG.info("Nud RPC connection ok.");
        } else {
            //TODO: recover?
            throw new NuBotConnectionException("Problem with nud connectivity");
        }
    }

    /**
     * test setup exchange
     *
     * @throws NuBotRunException
     */
    public void testExchange() throws NuBotRunException {

        ApiResponse response = Global.trade.getActiveOrders(Global.options.getPair());
        if (response.isPositive()) {
        } else {
            throw new NuBotRunException("Problems while validating communication with exchange : [ " + response.getError() + " ]");
        }
    }

    /**
     * execute the NuBot based on a configuration
     */
    public void execute(NuBotOptions opt) throws NuBotRunException {

        LOG.info("Setting up NuBot version : " + VersionInfo.getVersionName());

        if (opt.isExecuteOrders()) {
            liveTrading = true;
            LOG.info("Live mode : Trades will be executed");
        } else {
            LOG.info("Demo mode: Trades will not be executed [executetrade:false]");
            liveTrading = false;
        }

        Global.options = opt;

        setupAllConfig();

        LOG.debug("Create a TaskManager ");
        Global.taskManager = new TaskManager();
        Global.taskManager.setTasks();

        if (Global.options.isSubmitLiquidity()) {
            Global.taskManager.setupNuRPCTask();
            Global.taskManager.startTaskNu();
        }

        LOG.debug("Starting task : Check connection with exchange");

        //For a 0 tx fee market, force a price-offset of 0.1%
        ApiResponse txFeeResponse = Global.trade.getTxFee(Global.options.getPair());
        if (txFeeResponse.isPositive()) {
            double txfee = (Double) txFeeResponse.getResponseObject();
            if (txfee == 0) {
                double minoffset = 0.001;
                LOG.warn("The bot detected a 0 TX fee : forcing a priceoffset of " + Utils.formatNumber(minoffset / 2, 6) + " per side [if required]");
                double spread = Global.options.getBookBuyOffset() + Global.options.getBookSellOffset();
                if (spread < minoffset) {
                    Global.options.setBookSellOffset(minoffset / 2);
                    Global.options.setBookBuyOffset(minoffset / 2);
                }
            }
        }

        testExchange();

        //Start task to check orders
        try {
            Global.taskManager.getCheckOrdersTask().start(Settings.DELAY_ORDERCHECK);
        } catch (Exception e) {
            throw new NuBotRunException("" + e);
        }

        if (Global.options.isSubmitLiquidity()) {
            try {
                checkNuConn();
            } catch (NuBotConnectionException e) {
                MainLaunch.exitWithNotice("can't connect to Nu " + e);
            }
        }

        if (Global.options.poolModeActive) {
            setupPoolMode(); //Try to register with ALP activating object Global.alpManager , exit if registration fails
        }

        LOG.info("Start trading Strategy specific for " + Global.options.getPair().toString());

        LOG.info("Options loaded : " + Global.options.toString());

        // Set the frozen balance manager in the global variable

        Global.frozenBalancesManager = new FrozenBalancesManager(Global.options.getExchangeName(), Global.options.getPair());

        try {
            configureStrategy();
        } catch (Exception e) {
            throw new NuBotRunException("" + e);
        }

        notifyOnline();

    }

    protected void notifyOnline() {
        String exc = Global.options.getExchangeName();
        String p = Global.options.getPair().toStringSep();
        String msg = "A new " + mode + " bot just came online on **" + exc + "** pair (" + p + ")";
        if (Global.options.poolModeActive)
            msg += " - (ALP pool: " + Global.options.poolURI + ")";
        LOG.debug("notify online " + msg);
        GitterNotifications.sendMessage(msg);
    }

    private void logSessionStatistics() {

        LOG.info("session statistics");
        //log closing statistics
        LOG.info("totalOrdersSubmitted " + Global.orderManager.getTotalOrdersSubmitted());

        String openStrongTaging = "**";
        String closingStrongTaging = "**";

        String additionalInfo = "after " + TimeUtils.getBotUptimeDate() + " uptime on "
                + openStrongTaging + Global.options.getExchangeName() + closingStrongTaging + " ["
                + Global.options.getPair().toStringSep() + "]";

        LOG.info(additionalInfo.replace(closingStrongTaging, "").replace(openStrongTaging, "")); //Remove html tags
        GitterNotifications.sendMessageCritical("Bot shut-down " + additionalInfo);

    }

    public void shutdownBot() {

        LOG.info("Bot shutting down sequence started.");

        //Delete temporary log files

        try {
            FilesystemUtils.deleteFile(Global.sessionPath + "/" + Settings.LOG_STANDARD_LIVE_FILENAME + ".1.zip");
            FilesystemUtils.deleteFile(Global.sessionPath + "/" + Settings.LOG_STANDARD_LIVE_FILENAME + ".2.zip");
            FilesystemUtils.deleteFile(Global.sessionPath + "/" + Settings.LOG_VERBOSE_LIVE_FILENAME + ".1.zip");
            FilesystemUtils.deleteFile(Global.sessionPath + "/" + Settings.LOG_VERBOSE_LIVE_FILENAME + ".2.zip");
        } catch (Exception e) {
            LOG.error("Problem while deleting files");
        }
        //Interrupt all BotTasks

        if (Global.taskManager != null) {
            if (Global.taskManager.isInitialized()) {
                try {
                    LOG.info("try to shutdown all tasks");
                    Global.taskManager.stopAll();
                } catch (IllegalStateException e) {
                    LOG.error(e.toString());
                }
            }
        }

        //Save executed trades and report to file and delegate it to runnable

        ScheduledExecutorService scheduler =
                Executors.newScheduledThreadPool(1);

        final Runnable saveTradesTask = new SaveTradesAndFinalize(Global.options.getPair(), SessionManager.sessionStarted, Global.sessionPath);

        final ScheduledFuture<?> terminatorHandle =
                scheduler.schedule(saveTradesTask, 100, MILLISECONDS);

        //Try to cancel all orders, if any
        if (Global.trade != null && Global.options.getPair() != null) {

            LOG.info("Clearing out active orders ... ");

            ApiResponse deleteOrdersResponse = Global.trade.clearOrders(Global.options.getPair());
            if (deleteOrdersResponse.isPositive()) {
                boolean deleted = (boolean) deleteOrdersResponse.getResponseObject();

                if (deleted) {
                    LOG.info("Order clear request successful");
                } else {
                    LOG.error("Could not submit request to clear orders");
                }
            } else {
                LOG.error("error canceling orders: " + deleteOrdersResponse.getError().toString());
            }
        }

        //Send disconnection signal to Streamer server
        if (!Global.options.isBypassStreaming() && Global.isSubscribed) {
            LOG.info("Sending disconnect message");
            Global.subscriber.sendDisconnectMessage();
        }

        terminateStreamingSocket();


        //reset liquidity info
        if (Global.options.isSubmitLiquidity()) {
            if (Global.rpcClient.isConnected()) {
                //tier 1
                LOG.info("Resetting Liquidity Info before quit");


                if (Global.rpcClient.isConnected()) {
                    //Reset tier1
                    ApiResponse resp = Global.rpcClient.submitLiquidityInfo(Global.rpcClient.USDchar, 0, 0, 1);
                    if (resp.isPositive()) {
                        boolean submitted = (boolean) resp.getResponseObject();
                        if (submitted) {
                            LOG.info("Tier1 liquidityinfo reset OK ");
                        } else {
                            LOG.error("Something went wrong while sending liquidityinfo : " + resp.getError());
                        }
                    } else {
                        LOG.error("Something went wrong while sending liquidityinfo");
                    }
                    //Reset tier2

                    resp = Global.rpcClient.submitLiquidityInfo(Global.rpcClient.USDchar, 0, 0, 2);
                    if (resp.isPositive()) {
                        boolean submitted = (boolean) resp.getResponseObject();
                        if (submitted) {
                            LOG.info("Tier2 liquidityinfo reset OK ");
                        } else {
                            LOG.error("Something went wrong while sending liquidityinfo : " + resp.getError());
                        }

                    } else {
                        LOG.error("Something went wrong while sending liquidityinfo : " + resp.getError());
                    }

                } else {
                    LOG.error("Nu Client offline. ");
                }

            }
        }


        logSessionStatistics();


        LOG.info("Logs of this session saved in " + Global.sessionPath + ".zip");
        SessionManager.setModeHalted();
        SessionManager.sessionStopped = System.currentTimeMillis();
        LOG.info("** end of the session **");

        Global.defineLogPath(Settings.ORPHANS_LOGFOLDER + "/" + Global.logFolder); //Set logging folder to IDLES

    }

}
