/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.utils.InitTests;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Built with netbeans
 */
public class TestWrappers extends javax.swing.JFrame {


    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TestWrappers.class.getName());

    /**
     * Configure tests
     */
    private static final String TEST_OPTIONS_PATH = "config/myconfig/latest/poloniex.json"; //TODO edit here

    private final String[] pairs = {"NBT_BTC", "NBT_USD", "NBT_EUR", "NBT_PPC", "BTC_NBT", "BTC_USD"};
    private final String[] currencies = {"NBT", "BTC", "USD", "EUR"};

    public static void main(String[] args) {
        init();
        TestWrappers ui = new TestWrappers();
        ui.buildPanel();
        ui.selectRightPair();

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ui.setVisible(true);
            }
        });
    }


    private void getBalancesBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetAvailableBalances(getPairObjectFromCombo(getBalancesCombo));
    }

    private CurrencyPair getPairObjectFromCombo(JComboBox<String> combo) {
        return CurrencyPair.getCurrencyPairFromString(combo.getSelectedItem().toString(), "_");
    }

    private void getAvailableBalanceBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetAvailableBalance(Currency.createCurrency(getAvailableBalanceCombo.getSelectedItem().toString()));
    }


    private void sellBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testSell(Utils.getDouble(sellAmountField.getText()),
                Utils.getDouble(sellPriceField.getText()),
                getPairObjectFromCombo(sellCombo));
    }

    private void buyBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testBuy(Utils.getDouble(buyAmountText.getText()),
                Utils.getDouble(buyPriceField.getText()),
                getPairObjectFromCombo(buyCombo));
    }

    private void getActiveOrdersBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetActiveOrders(getPairObjectFromCombo(getActiveOrdersCombo), getActiveOrdersCheckbox.isSelected());
    }

    private void clearAllOrdersBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testClearAllOrders(getPairObjectFromCombo(clearAllOrdersCombo));
    }

    private void cancelOrderBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testCancelOrder(cancelOrdersText.getText(), getPairObjectFromCombo(cancelOrderCombo));
    }

    private void isOrderActiveBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testIsOrderActive(isOrderActiveText.getText());
    }

    private void getLastTradesBtnActionPerformed(java.awt.event.ActionEvent evt) {
        if (!getLastTradesText.getText().equals("")) {
            WrapperTestUtils.testGetLastTrades(getPairObjectFromCombo(getLastTradesCombo), Long.parseLong(getLastTradesText.getText()));
        } else {
            WrapperTestUtils.testGetLastTrades(getPairObjectFromCombo(getLastTradesCombo));

        }
    }

    private void getBalancesComboActionPerformed(ActionEvent evt) {
    }


    private void getTickerBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetLastPrice(getPairObjectFromCombo(getTickerCombo));
    }

    private void getOrderBookBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetOrderBook(getPairObjectFromCombo(getOrderBookCombo), false);
    }

    private void getTxFeeBtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetTxFeeWithArgs(getPairObjectFromCombo((getFreeCombo)));
    }

    private void placeOrdersBtnActionPerformed(java.awt.event.ActionEvent evt) {
        CurrencyPair testPair = CurrencyList.NBT_BTC;
        ArrayList<OrderToPlace> ordersToPlace = new ArrayList<>();
        for (int i = 0; i <= 11; i++)
            ordersToPlace.add(new OrderToPlace(Constant.SELL, testPair, 0.1 + Math.random() * 0.1, 1));

        WrapperTestUtils.testPlaceOrders(new OrderBatch(ordersToPlace, 0), testPair);


        ordersToPlace = new ArrayList<>();
        for (int i = 0; i <= 11; i++)
            ordersToPlace.add(new OrderToPlace(Constant.BUY, testPair, 0.1 + Math.random() * 0.1, 0.00001));

        WrapperTestUtils.testPlaceOrders(new OrderBatch(ordersToPlace, 0), testPair);

    }

    private void customTest2BtnActionPerformed(java.awt.event.ActionEvent evt) {
        WrapperTestUtils.testGetOrderDetail(isOrderActiveText.getText());
    }


    private void buildPanel() {
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        exchangeNameLabel = new javax.swing.JLabel();
        getAvailableBalanceBtn = new javax.swing.JButton();
        getActiveOrdersCombo = new javax.swing.JComboBox();
        getBalancesBtn = new javax.swing.JButton();
        getBalancesCombo = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        sellBtn = new javax.swing.JButton();
        buyBtn = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        sellAmountField = new javax.swing.JTextField();
        buyAmountText = new javax.swing.JTextField();
        getActiveOrdersBtn = new javax.swing.JButton();
        clearAllOrdersBtn = new javax.swing.JButton();
        cancelOrderBtn = new javax.swing.JButton();
        isOrderActiveBtn = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        getLastTradesBtn = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        getTickerBtn = new javax.swing.JButton();
        getOrderBookBtn = new javax.swing.JButton();
        getTxFeeBtn = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        placeOrdersBtn = new javax.swing.JButton();
        customTest2Btn = new javax.swing.JButton();
        getAvailableBalanceCombo = new javax.swing.JComboBox();
        getActiveOrdersCheckbox = new javax.swing.JCheckBox();
        clearAllOrdersCombo = new javax.swing.JComboBox();
        getLastTradesCombo = new javax.swing.JComboBox();
        cancelOrdersText = new javax.swing.JTextField();
        isOrderActiveText = new javax.swing.JTextField();
        getLastTradesText = new javax.swing.JTextField();
        getTickerCombo = new javax.swing.JComboBox();
        getOrderBookCombo = new javax.swing.JComboBox();
        getFreeCombo = new javax.swing.JComboBox();
        clearOutputBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        buyPriceField = new javax.swing.JTextField();
        sellPriceField = new javax.swing.JTextField();
        sellCombo = new javax.swing.JComboBox();
        buyCombo = new javax.swing.JComboBox();
        cancelOrderCombo = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jScrollPane1.setViewportView(jTextPane1);

        exchangeNameLabel.setFont(new java.awt.Font("Lucida Grande", 0, 22));
        exchangeNameLabel.setText(Global.options.getExchangeName());

        getAvailableBalanceBtn.setText("getAvailableBalance");
        sellBtn.setText("sell");
        buyBtn.setText("buy");
        getBalancesBtn.setText("getBalances");
        sellAmountField.setToolTipText("amountnbt");
        buyAmountText.setToolTipText("amount nbt");
        getActiveOrdersBtn.setText("getActiveOrders");
        clearAllOrdersBtn.setText("clearAllOrders");
        cancelOrderBtn.setText("cancelOrder");
        isOrderActiveBtn.setText("isOrderActive");
        getLastTradesBtn.setText("getLastTrades");
        getTickerBtn.setText("getTicker");
        getOrderBookBtn.setText("getOrderBook");
        getTxFeeBtn.setText("getTxFee");
        placeOrdersBtn.setText("placeMultipleOrders");
        customTest2Btn.setText("getOrderDetail");

        getActiveOrdersCheckbox.setText("verbose");

        cancelOrdersText.setToolTipText("orderid");
        isOrderActiveText.setToolTipText("orderid");


        getAvailableBalanceCombo.setModel(new javax.swing.DefaultComboBoxModel(currencies));

        clearAllOrdersCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getLastTradesCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getTickerCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getOrderBookCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getFreeCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        cancelOrderCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getBalancesCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        sellCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        buyCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));
        getActiveOrdersCombo.setModel(new javax.swing.DefaultComboBoxModel(pairs));


        //Assign action listeners
        getAvailableBalanceBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getAvailableBalanceBtnActionPerformed(evt);
            }
        });

        getBalancesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getBalancesBtnActionPerformed(evt);
            }
        });

        getBalancesCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getBalancesComboActionPerformed(evt);
            }
        });

        sellBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sellBtnActionPerformed(evt);
            }
        });

        buyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buyBtnActionPerformed(evt);
            }
        });

        getActiveOrdersBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getActiveOrdersBtnActionPerformed(evt);
            }
        });

        clearAllOrdersBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearAllOrdersBtnActionPerformed(evt);
            }
        });

        cancelOrderBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelOrderBtnActionPerformed(evt);
            }
        });
        isOrderActiveBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                isOrderActiveBtnActionPerformed(evt);
            }
        });

        getLastTradesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getLastTradesBtnActionPerformed(evt);
            }
        });

        getTickerBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getTickerBtnActionPerformed(evt);
            }
        });

        getOrderBookBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getOrderBookBtnActionPerformed(evt);
            }
        });

        getTxFeeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getTxFeeBtnActionPerformed(evt);
            }
        });

        placeOrdersBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                placeOrdersBtnActionPerformed(evt);
            }
        });

        customTest2Btn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customTest2BtnActionPerformed(evt);
            }
        });


        jLabel1.setText("NBT @");
        jLabel2.setText("NBT @");

        buyPriceField.setToolTipText("amount nbt");
        sellPriceField.setToolTipText("amount nbt");


        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(40, 40, 40)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(getOrderBookBtn)
                                                        .addComponent(getTxFeeBtn, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(getTickerBtn, javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(placeOrdersBtn, javax.swing.GroupLayout.Alignment.LEADING))
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(layout.createSequentialGroup()
                                                                .addGap(30, 30, 30)
                                                                .addComponent(customTest2Btn))
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                        .addComponent(getFreeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(getOrderBookCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(getTickerCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGap(11, 11, 11))))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGap(37, 37, 37)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                                .addGroup(layout.createSequentialGroup()
                                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                                .addGroup(layout.createSequentialGroup()
                                                                                                        .addComponent(cancelOrderBtn)
                                                                                                        .addGap(14, 14, 14))
                                                                                                .addComponent(clearAllOrdersBtn, javax.swing.GroupLayout.Alignment.LEADING))
                                                                                        .addGap(30, 30, 30)
                                                                                        .addComponent(cancelOrdersText, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addGroup(layout.createSequentialGroup()
                                                                                        .addComponent(isOrderActiveBtn)
                                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                        .addComponent(isOrderActiveText, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                                .addComponent(getActiveOrdersBtn))
                                                                        .addGap(18, 18, 18)
                                                                        .addComponent(cancelOrderCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                                .addComponent(getActiveOrdersCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                                                        .addGap(157, 157, 157)
                                                                                        .addComponent(clearAllOrdersCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(getActiveOrdersCheckbox))))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGap(38, 38, 38)
                                                        .addComponent(getLastTradesBtn)
                                                        .addGap(30, 30, 30)
                                                        .addComponent(getLastTradesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addComponent(getLastTradesText, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGap(37, 37, 37)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(getAvailableBalanceBtn)
                                                                .addComponent(getBalancesBtn))
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(getBalancesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                                        .addGap(6, 6, 6)
                                                                        .addComponent(getAvailableBalanceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGroup(layout.createSequentialGroup()
                                                        .addGap(36, 36, 36)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                                .addComponent(buyAmountText, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addComponent(sellBtn)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(sellAmountField, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addComponent(jLabel2)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(buyPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(buyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                .addGroup(layout.createSequentialGroup()
                                                                        .addComponent(jLabel1)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(sellPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                        .addComponent(sellCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(182, 182, 182)
                                                .addComponent(exchangeNameLabel)))
                                .addContainerGap(29, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(buyBtn)
                                        .addContainerGap(339, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(exchangeNameLabel)
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getBalancesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(getBalancesBtn))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getAvailableBalanceBtn)
                                        .addComponent(getAvailableBalanceCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(45, 45, 45)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(buyAmountText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel2)
                                        .addComponent(buyPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(buyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(sellBtn)
                                        .addComponent(sellAmountField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1)
                                        .addComponent(sellPriceField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(sellCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(34, 34, 34)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(getActiveOrdersBtn)
                                                        .addComponent(getActiveOrdersCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(getActiveOrdersCheckbox))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(clearAllOrdersBtn)
                                                        .addComponent(clearAllOrdersCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cancelOrderBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(cancelOrdersText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(cancelOrderCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(isOrderActiveBtn)
                                        .addComponent(isOrderActiveText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getLastTradesBtn)
                                        .addComponent(getLastTradesCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(getLastTradesText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getTickerBtn)
                                        .addComponent(getTickerCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getOrderBookBtn)
                                        .addComponent(getOrderBookCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(getTxFeeBtn)
                                        .addComponent(getFreeCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(placeOrdersBtn)
                                        .addComponent(customTest2Btn))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(176, 176, 176)
                                        .addComponent(buyBtn)
                                        .addContainerGap(451, Short.MAX_VALUE)))
        );

        pack();
    }


    private static void init() {
        InitTests.setLoggingFilename(TestWrappers.class.getSimpleName());
        InitTests.loadCredentialManager(false);
        InitTests.loadConfig(TEST_OPTIONS_PATH);  //Load settings

        SessionManager.setModeRunning();

        //Load keystore
        boolean trustAll = false;
        if (Global.options.getExchangeName().equalsIgnoreCase(ExchangeFacade.INTERNAL_EXCHANGE_PEATIO)) {
            trustAll = true;
        }
        InitTests.loadKeystore(trustAll);

        try {
            LOG.info("Public API key: " + Global.options.getApiKey());
            LOG.info("Exchange: " + Global.options.getExchangeName());
            WrapperTestUtils.configureExchange(Global.options.getExchangeName());

        } catch (NuBotConfigException ex) {
            LOG.error(ex.toString());
        }

        Global.sessionPath = Settings.TEST_LOGFOLDER;
    }


    private void selectRightPair() {
        comboList = new ArrayList<>();
        comboList.add(getTickerCombo);
        comboList.add(getOrderBookCombo);
        comboList.add(getLastTradesCombo);
        comboList.add(getFreeCombo);
        comboList.add(getBalancesCombo);
        comboList.add(getActiveOrdersCombo);
        comboList.add(clearAllOrdersCombo);
        comboList.add(sellCombo);
        comboList.add(buyCombo);
        comboList.add(cancelOrderCombo);

        changeAllPairCombos(Global.options.pair);

    }

    private void changeAllPairCombos(String pair) {
        pair = pair.toUpperCase();
        boolean found = false;
        for (int i = 0; i < pairs.length; i++) {
            if (pairs[i].equals(pair)) {
                found = true;
                break;
            }
        }

        if (found)
            for (int i = 0; i < comboList.size(); i++) {
                comboList.get(i).setSelectedItem(pair);
            }

        else {
            LOG.error("Cannot select pair=" + pair);
            System.exit(0);
        }
    }

    // Variables declaration - do not modify
    private javax.swing.JTextField buyAmountText;
    private javax.swing.JButton buyBtn;
    private javax.swing.JTextField buyPriceField;
    private javax.swing.JButton cancelOrderBtn;
    private javax.swing.JTextField cancelOrdersText;
    private javax.swing.JButton clearAllOrdersBtn;
    private javax.swing.JComboBox clearAllOrdersCombo;
    private javax.swing.JButton clearOutputBtn;
    private javax.swing.JButton placeOrdersBtn;
    private javax.swing.JButton customTest2Btn;
    private javax.swing.JLabel exchangeNameLabel;
    private javax.swing.JButton getActiveOrdersBtn;
    private javax.swing.JCheckBox getActiveOrdersCheckbox;
    private javax.swing.JComboBox getActiveOrdersCombo;
    private javax.swing.JButton getAvailableBalanceBtn;
    private javax.swing.JComboBox getAvailableBalanceCombo;
    private javax.swing.JButton getBalancesBtn;
    private javax.swing.JComboBox getBalancesCombo;
    private javax.swing.JComboBox getFreeCombo;
    private javax.swing.JButton getLastTradesBtn;
    private javax.swing.JComboBox getLastTradesCombo;
    private javax.swing.JButton getOrderBookBtn;
    private javax.swing.JComboBox getOrderBookCombo;
    private javax.swing.JButton getTickerBtn;
    private javax.swing.JComboBox getTickerCombo;
    private javax.swing.JComboBox sellCombo;
    private javax.swing.JComboBox buyCombo;
    private javax.swing.JComboBox cancelOrderCombo;
    private javax.swing.JButton getTxFeeBtn;
    private javax.swing.JButton isOrderActiveBtn;
    private javax.swing.JTextField isOrderActiveText;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextField sellAmountField;
    private javax.swing.JButton sellBtn;
    private javax.swing.JTextField sellPriceField;
    private javax.swing.JTextField getLastTradesText;

    private ArrayList<javax.swing.JComboBox> comboList;
    // End of variables declaration

}
