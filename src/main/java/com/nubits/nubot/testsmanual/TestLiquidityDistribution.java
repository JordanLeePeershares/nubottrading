/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;


import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityCurve;
import com.nubits.nubot.trading.LiquidityDistribution.LiquidityDistributionModel;
import com.nubits.nubot.trading.LiquidityDistribution.ModelParameters;
import com.nubits.nubot.trading.PreviewOrderbook;
import com.nubits.nubot.utils.InitTests;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class TestLiquidityDistribution {

    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TestLiquidityDistribution.class.getName());
    private static final String TEST_OPTIONS_PATH = "config/myconfig/latest/poloniex.json";
    private LiquidityDistributionModel ldm;
    private ModelParameters sellParams;
    private ModelParameters buyParams;
    private Amount balanceNBT;
    private Amount balancePEG;
    private boolean exec = false; //TODO change here to execute orders

    private CurrencyPair pair;
    private double txFee;
    double pegPrice;

    public static void main(String a[]) {
        InitTests.setLoggingFilename(TestLiquidityDistribution.class.getSimpleName());

        TestLiquidityDistribution test = new TestLiquidityDistribution();

        InitTests.loadConfig(TEST_OPTIONS_PATH);

        test.init(""); //Pass an empty string to avoid placing the orders, or exchange name
        test.configureTest();
        test.exec();
    }

    private void init(String exchangeName) {

        pair = CurrencyList.NBT_BTC;
        if (!exchangeName.equals("")) {
            //Setup the exchange
            try {
                WrapperTestUtils.configureExchange(exchangeName);
            } catch (NuBotConfigException e) {

            }
            WrapperTestUtils.testClearAllOrders(pair);
        }

    }

    private void configureTest() {
        LOG.info("Configuring test");

        //Custodian balance simulation
        balanceNBT = new Amount(10000, CurrencyList.NBT); //expressed in NBT
        balancePEG = new Amount(40, CurrencyList.BTC); //expressed in PEG
        if (exec) {
            configureBalances(pair);
        }

        pegPrice = 300; // value of 1 unit expressed in USD

        txFee = 0.2; // %

        sellParams = new ModelParameters(Global.options.bookSellOffset, Global.options.bookSellwall,
                Global.options.bookSellInterval, LiquidityCurve.generateCurve(Global.options.bookSellType, Global.options.bookSellSteepness), Global.options.bookSellMaxVolumeCumulative);
        buyParams = new ModelParameters(Global.options.bookBuyOffset, Global.options.bookBuywall,
                Global.options.bookBuyInterval, LiquidityCurve.generateCurve(Global.options.bookBuyType, Global.options.bookBuySteepness), Global.options.bookBuyMaxVolumeCumulative);


        String config = "Sell order book configuration : " + sellParams.toString();
        config += "Buy order book configuration : " + buyParams.toString();
        config += "Pair : " + pair.toString();
        config += "\nbalanceNBT : " + balanceNBT.getQuantity();
        config += "\nbalancePEG : " + balancePEG.getQuantity();
        config += "\npegPrice : " + pegPrice;
        config += "\ntxFee : " + txFee;
        config += "\n\n -------------------";

        LOG.info(config);

    }

    private void exec() {
        ldm = new LiquidityDistributionModel(sellParams, buyParams);

        boolean print = true;
        boolean draw = true;


        //Compute orders to place t1+t2
        ArrayList<OrderToPlace> sellOrders = ldm.getOrdersToPlace(Constant.SELL, balanceNBT, pegPrice, pair, txFee, sellParams.getCap(), false).getOrders();
        ArrayList<OrderToPlace> buyOrders = ldm.getOrdersToPlace(Constant.BUY, balancePEG, pegPrice, pair, txFee, buyParams.getCap(), false).getOrders();


        if (draw) {
            PreviewOrderbook.drawOrderBooks(sellOrders, buyOrders, pegPrice, true, pair.getPaymentCurrency().equals(CurrencyList.NBT));
        }
        if (exec) {
            placeOrders(sellOrders, buyOrders);
        }


    }


    private void placeOrders(ArrayList<OrderToPlace> sellOrders, ArrayList<OrderToPlace> buyOrders) {

        long startTime = System.nanoTime(); //TIC

        LOG.info("Placing sell orders on " + Global.options.exchangeName);
        WrapperTestUtils.testPlaceOrders(new OrderBatch(sellOrders, 0), pair);

        LOG.info("Placing buy orders on " + Global.options.exchangeName);
        WrapperTestUtils.testPlaceOrders(new OrderBatch(buyOrders, 0), pair);

        LOG.info("Total Time: " + (System.nanoTime() - startTime) / 1000000 + " ms"); //TOC

    }

    private boolean configureBalances(CurrencyPair pair) {
        boolean success = true;
        ApiResponse balanceNBTResponse = Global.trade.getAvailableBalance(CurrencyList.NBT);
        if (balanceNBTResponse.isPositive()) {
            Amount balance = (Amount) balanceNBTResponse.getResponseObject();
            LOG.info("NBT Balance : " + balance.toString());
            balanceNBT = balance;
        } else {
            LOG.error(balanceNBTResponse.getError().toString());
            success = false;
        }

        ApiResponse balancePEGResponse = Global.trade.getAvailableBalance(pair.getPaymentCurrency());
        if (balancePEGResponse.isPositive()) {
            Amount balance = (Amount) balancePEGResponse.getResponseObject();
            LOG.info(pair.getPaymentCurrency().getCode() + " Balance : " + balance.toString());
            balancePEG = balance;
        } else {
            LOG.error(balancePEGResponse.getError().toString());
            success = false;
        }

        return success;
    }
}
