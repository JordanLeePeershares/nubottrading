/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;

import com.nubits.nubot.RPC.NudClient;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.models.CurrencyList;
import com.nubits.nubot.tasks.TaskManager;
import com.nubits.nubot.utils.InitTests;
import com.nubits.nubot.utils.Utils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestRPCLiquidityInfo {

    //define Logging by using predefined Settings which points to an XML
    static {
        System.setProperty("logback.configurationFile", Settings.TEST_LOGXML);
    }

    private static final Logger LOG = LoggerFactory.getLogger(TestRPCLiquidityInfo.class.getName());

    private static final String NUD_RPC_USER = "rpctestuser";
    private static final String NUD_RPC_PASS = "rpctestpassphras0r";
    private static final String CUSTODIAN_PUBLIC_ADDRESS = "bVcXrdTgrMSg6J2YqsLedCbi6Ubek9eTe5"; //testnet elected custodian

    /**
     * Configure tests
     */
    private static final String TEST_OPTIONS_PATH = "config/myconfig/latest/southxchange.json";


    public static void main(String[] args) {

        InitTests.setLoggingFilename(TestRPCLiquidityInfo.class.getSimpleName());

        InitTests.loadConfig(TEST_OPTIONS_PATH);  //Load settings

        //Default values

        String custodian = CUSTODIAN_PUBLIC_ADDRESS;
        String user = NUD_RPC_USER;
        String pass = NUD_RPC_PASS;
        double sell = 0;
        double buy = 0;

        //java -jar testRPC user pass custodian sell buy
        if (args.length == 5) {
            LOG.info("Reading input parameters");
            user = args[0];
            pass = args[1];
            custodian = args[2];
            sell = Double.parseDouble(args[3]);
            buy = Double.parseDouble(args[4]);
        }

        Global.rpcClient = new NudClient("127.0.0.1", 9091, user, pass, true, custodian, CurrencyList.NBT_BTC, "");
        SessionManager.setModeRunning();

        TestRPCLiquidityInfo test = new TestRPCLiquidityInfo();

        test.testCheckNudTask();

        Utils.installKeystore(true);

        test.testGetInfo();
        test.testIsConnected();

        test.testSendLiquidityInfo(buy, sell, 1);

        test.testGetLiquidityInfo();

        test.testGetLiquidityInfo(Constant.BUY, "BTRnV9uLSPVJw4jn1JMV2Ki2cfFqPYip9o");


    }

    private void testSendLiquidityInfo(double amountBuy, double amountSell, int tier) {
        if (Global.rpcClient.isConnected()) {
            ApiResponse resp = Global.rpcClient.submitLiquidityInfo(Global.rpcClient.USDchar, amountBuy, amountSell, tier);
            if (resp.isPositive()) {
                boolean submitted = (boolean) resp.getResponseObject();
                if (submitted) {
                    LOG.info("Liquidity submitted, calling getliquidityinfo");
                    ApiResponse r = Global.rpcClient.getLiquidityInfo(NudClient.USDchar);
                    if (r.isPositive()) {
                        JSONObject result = (JSONObject) r.getResponseObject();
                        LOG.info(result.toJSONString());
                    } else {
                        LOG.error("Can't get liquidityinfo : " + r.getError().toString());
                    }

                } else {
                    LOG.error("Something went wrong while sending liquidityinfo : " + resp.getError());
                }

            } else {
                LOG.error("Something went wrong while sending liquidityinfo : " + resp.getError().toString());
            }

        } else {
            LOG.error("Nu Client offline. ");
        }

    }

    private void testGetInfo() {
        if (Global.rpcClient.isConnected()) {
            ApiResponse resp = Global.rpcClient.getInfo();
            if (resp.isPositive()) {
                JSONObject responseObject = (JSONObject) resp.getResponseObject();
                LOG.info(responseObject.toJSONString());
            } else {
                LOG.error(resp.getError().toString());
            }
        } else {
            LOG.error("Nu Client offline. ");
        }
    }

    private void testIsConnected() {
        String connectedString = "offline";
        if (Global.rpcClient.isConnected()) {
            connectedString = "online";
        }
        LOG.info("Nud is " + connectedString + " @ " + Global.rpcClient.getIp() + ":" + Global.rpcClient.getPort());
    }

    private void testCheckNudTask() {
        //Create a TaskManager and
        Global.taskManager = new TaskManager();
        Global.taskManager.setNudTask();
        //Start checking for connection
        Global.taskManager.getCheckNudTask().start();

        //Wait a couple of seconds for the connectionThread to get live
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            LOG.error("" + ex);
        }
    }

    private void testGetLiquidityInfo() {
        if (Global.rpcClient.isConnected()) {
            ApiResponse r = Global.rpcClient.getLiquidityInfo(NudClient.USDchar);
            if (r.isPositive()) {
                JSONObject result = (JSONObject) r.getResponseObject();
                LOG.info(result.toJSONString());
            } else {
                LOG.error("Can't get liquidityinfo : " + r.getError().toString());
            }
        } else {
            LOG.error("Nu Client offline. ");
        }
    }

    private void testGetLiquidityInfo(String type, String address) {
        if (Global.rpcClient.isConnected()) {
            double response = Global.rpcClient.getLiquidityInfo(NudClient.USDchar, type, address);
            if (response == -1) {
                LOG.error("Something went wrong while sending liquidityinfo");
            } else {
                LOG.info("Total " + type + " liquidity : " + response + " " + CurrencyList.NBT.getCode());
            }
        } else {
            LOG.error("Nu Client offline. ");
        }

    }
}
