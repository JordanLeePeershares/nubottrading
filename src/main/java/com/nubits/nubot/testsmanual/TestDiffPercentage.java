/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.testsmanual;

import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class TestDiffPercentage {
    private static final Logger LOG = LoggerFactory.getLogger(TestDiffPercentage.class.getName());

    public static void main(String[] args) {
        LOG.info("PriceVariation 10, 100 :" + Utils.percentagePriceVariation(10, 100));
        LOG.info("PriceVariation 100, 10  :" + Utils.percentagePriceVariation(100, 10));
        LOG.info("PriceVariation 30, 40 :" + Utils.percentagePriceVariation(30, 40));
        LOG.info("PriceVariation 40, 30 :" + Utils.percentagePriceVariation(40, 30));
        LOG.info("PriceVariation 226, 230 : " + Utils.percentagePriceVariation(226, 230));
        LOG.info("PriceVariation 230, 226 : " + Utils.percentagePriceVariation(230, 226));
        Utils.printSeparator();

        LOG.info("Spread 10, 100  :" + Utils.percentageDifferenceSpread(10, 100));
        LOG.info("Spread 100, 10  :" + Utils.percentageDifferenceSpread(100, 10));
        LOG.info("Spread 30, 40 :" + Utils.percentageDifferenceSpread(30, 40));
        LOG.info("Spread 40, 30 :" + Utils.percentageDifferenceSpread(40, 30));
        LOG.info("Spread 226, 230 : " + Utils.percentageDifferenceSpread(226, 230));
        LOG.info("Spread 230, 226  :" + Utils.percentageDifferenceSpread(230, 226));

    }
}
