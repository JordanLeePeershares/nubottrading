package com.nubits.nubot.webui;


import com.google.gson.Gson;
import com.nubits.nubot.bot.Global;
import com.nubits.nubot.bot.NuBotRunException;
import com.nubits.nubot.bot.SessionManager;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.options.ParseOptions;
import com.nubits.nubot.options.SaveOptions;
import com.nubits.nubot.trading.PreviewOrderbook;
import com.nubits.nubot.utils.JSONUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static spark.Spark.get;
import static spark.Spark.post;

/**
 * controller for changing Configurations.
 * GET from predefined file (which is the same as global.options)
 * POST user options. is loaded to global.options also
 */
public class ConfigController {

    //default config folder
    private String configDir = Settings.CONFIG_DIR;
    private String configfile;

    final static Logger LOG = LoggerFactory.getLogger(ConfigController.class);

    private void saveConfig(NuBotOptions newopt, String saveTo) {

        try {
            SaveOptions.backupOptions(this.configDir + File.separator + this.configfile);
        } catch (IOException e) {
            LOG.info("error with backup " + e);
        }

        String js = SaveOptions.jsonPretty(newopt);

        boolean savesuccess = true;
        try {
            SaveOptions.saveOptionsPretty(newopt, saveTo);
        } catch (Exception e) {
            LOG.info("error saving " + e);
            savesuccess = false;
        }

        if (savesuccess) {
            Global.options = newopt;
            Global.updateLiquidityDistManger();
        }

    }

    public ConfigController(String configfile) {

        this.configfile = configfile;

        post("/configreset", (request, response) -> {
            LOG.debug("/configreset called");
            Global.currentOptionsFile = Settings.DEFAULT_CONFIG_FILE_PATH;
            boolean result = SaveOptions.optionsReset(Settings.DEFAULT_CONFIG_FILE_PATH);
            //return jsonString;

            Map opmap = new HashMap();
            opmap.put("success", result);
            String json = new Gson().toJson(opmap);
            return json;
        });


        post("/previeworderbook", (request, response) -> {
            LOG.debug("/previeworderbook called");


            //Read options
            JSONObject postJson = null;
            Map opmap = new HashMap();
            String error = "none";
            boolean success = true;


            JSONObject orderbookJSON = new JSONObject();
            String volumeCurrency = "NBT";
            try {
                postJson = getJsonPost(request);
                //Create a NuBot option object

                NuBotOptions newopt = null;

                try {
                    newopt = ParseOptions.parsePost(postJson, false);
                } catch (Exception e) {
                    LOG.error("error parsing " + postJson + "\n" + e);
                    //handle errors
                    success = false;
                    error = "error parsing options. " + e;
                }

                if (success) {
                    //preview
                    try {
                        JSONObject object = PreviewOrderbook.previewOrderBook(newopt, false);
                        if ((boolean) object.get("error")) {
                            error = (String) object.get("errorMessage");
                            success = false;
                        } else {
                            orderbookJSON = (JSONObject) object.get("orderbook");
                            volumeCurrency = (String) object.get("volumeCurrency");
                        }

                    } catch (Exception e) {
                        error = e.toString();
                        success = false;
                    }
                }

            } catch (ParseException e) {
                LOG.debug(e.toString());
                success = false;
                error = e.toString();
            }


            opmap.put("success", success);
            opmap.put("error", error);
            opmap.put("orderbook", orderbookJSON);
            opmap.put("volumeCurrency", volumeCurrency);


            String json = new Gson().toJson(opmap);
            return json;
        });


        get("/configfile", "application/json", (request, response) -> {
            LOG.debug("/configfile called");

            Map opmap = new HashMap();
            opmap.put("configfile", Global.currentOptionsFile);
            String json = new Gson().toJson(opmap);
            return json;
        });

        get("/config", "application/json", (request, response) -> {
            LOG.debug("GET /config called");

            //get from memory. any change in the file is reflected in the global options
            String jsonString = NuBotOptions.optionsToJSONString(Global.options);
            return jsonString;
        });

        post("/config", "application/json", (request, response) -> {
            //check if bot is running
            LOG.debug("POST /config called");

            boolean active = SessionManager.isSessionRunning();
            LOG.trace("session currently active " + active);

            if (active) {
                LOG.debug("Trying to stop the bot before updating options");
                //if bot is running, STOP it, change options and RESTART it
                if (SessionManager.tryToStopBot()) {
                    Thread.sleep(6 * 1000); //Sleep some seconds to give the time to the bot to shutdown.
                    if (SessionManager.isModeHalted()) {
                        LOG.debug("Bot is halted");

                        //Read post options
                        JSONObject postJson = null;

                        try {
                            postJson = getJsonPost(request);
                        } catch (ParseException e) {
                            LOG.debug(e.toString());
                        }

                        //Change options
                        return changeOptions(postJson, true); //and restart

                    } else {
                        return createJSONError("Cannot halt the bot. Config is unchanged");
                    }
                } else {
                    return createJSONError("Cannot halt the bot. Config is unchanged");
                }
            }

            JSONObject postJson = null;

            try {
                postJson = getJsonPost(request);
            } catch (ParseException e) {
                LOG.debug(e.toString());
            }

            return changeOptions(postJson, false);

        });


        post("/partialconfig", "application/json", (request, response) -> {
                    LOG.debug("POST /partialconfig called");

                    boolean active = SessionManager.isSessionRunning();
                    LOG.trace("session currently active " + active);

                    //Check validity of posted options
                    //Read post options
                    JSONObject postJson = null;

                    try {
                        postJson = getJsonPost(request);
                    } catch (ParseException e) {
                        return createJSONError("Cannot Parse post parameters:" + e.toString());
                    }

                    //READ existing options
                    JSONObject fullOptions = ParseOptions.parseSingleJsonFile(Global.currentOptionsFile);

                    //REPLACE VALUES and push to fullOptions
                    Set<String> keysToReplace = postJson.keySet();
                    for (String key : keysToReplace) {
                        if (JSONUtils.containsIgnoreCase(fullOptions, key)) {
                            fullOptions = JSONUtils.replaceIgnoreCase(fullOptions, key, postJson.get(key));
                        }
                    }

                    if (active) {
                        LOG.debug("Trying to stop the bot before updating options");
                        //if bot is running, STOP it, change options and RESTART it
                        if (SessionManager.tryToStopBot()) {
                            Thread.sleep(4 * 1000); //Sleep some seconds to give the time to the bot to shutdown.
                            if (SessionManager.isModeHalted()) {
                                LOG.debug("Bot is halted");
                                //Change options
                                return changeOptions(fullOptions, true); //and restart
                            } else {
                                return createJSONError("Cannot halt the bot. Config is unchanged");
                            }

                        } else {
                            return createJSONError("Cannot stop the bot. Config is unchanged");
                        }
                    }


                    return changeOptions(fullOptions, false);

                }
        );

    }


    private Object changeOptions(JSONObject postJson, boolean restart) {
        boolean success = true;

        NuBotOptions newopt = null;
        Map opmap = new HashMap();
        String error = "none";
        try {
            newopt = ParseOptions.parsePost(postJson, false);
        } catch (Exception e) {
            LOG.error("error parsing " + postJson + "\n" + e);
            //handle errors
            success = false;
            error = "error parsing options. " + e;
        }


        if (success) {
            String saveTo = Global.currentOptionsFile;
            saveConfig(newopt, saveTo);
            if (restart) {
                try {
                    if (ParseOptions.isValidOptions(Global.options)) {
                        LOG.info("trying to restart bot");
                        try {
                            SessionManager.setModeStarting();
                            SessionManager.launchBot(Global.options);
                        } catch (NuBotRunException e) {
                            success = false;
                            LOG.error("could not start bot " + e);
                            error = "could not start bot: " + e;
                        }
                    } else {
                        success = false;
                        LOG.error("could not start bot. invalid options");
                        error = "could not start bot. invalid options";
                    }
                } catch (NuBotConfigException nce) {
                    success = false;
                    LOG.error("could not start bot " + nce);
                    opmap.put("error", "could not start bot: " + nce);
                }
                LOG.info("start bot success? " + success);
            }
        }

        opmap.put("success", success);
        opmap.put("error", error);

        String json = new Gson().toJson(opmap);

        return json;
    }

    private JSONObject getJsonPost(Request request) throws ParseException {
        LOG.debug("config received post" + request);
        String json_body = request.body();

        JSONParser parser = new JSONParser();
        JSONObject postJson = null;

        postJson = (JSONObject) (parser.parse(json_body));
        return postJson;

    }

    private String createJSONError(String message) {
        LOG.error(message);
        Map opmap = new HashMap();
        opmap.put("success", false);
        opmap.put("error", message);
        String json = new Gson().toJson(opmap);
        return json;
    }
}
