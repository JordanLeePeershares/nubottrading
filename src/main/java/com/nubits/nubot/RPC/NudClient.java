/*
 * Copyright (c) 2015. Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubot.RPC;

import com.nubits.nubot.bot.Global;
import com.nubits.nubot.global.Constant;
import com.nubits.nubot.models.ApiError;
import com.nubits.nubot.models.ApiResponse;
import com.nubits.nubot.models.CurrencyPair;
import com.nubits.nubot.utils.Utils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.*;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.NoRouteToHostException;
import java.net.SocketException;
import java.util.*;

public class NudClient {
    private static final Logger LOG = LoggerFactory.getLogger(NudClient.class.getName());

    public static final String USDchar = "B";
    private static final String COMMAND_GET_INFO = "getinfo";
    private static final String COMMAND_LIQUIDITYINFO = "liquidityinfo";
    private static final String COMMAND_GETLIQUIDITYINFO = "getliquidityinfo";

    private String ip;
    private int port;
    private String rpcUsername;
    private String rpcPassword;
    private boolean connected;
    private boolean useIdentifier;
    private String custodianPublicAddress;
    private String exchangeName;
    private CurrencyPair pair;

    public NudClient(String ip, int port, String rpcUser, String rpcPass, boolean useIdentifier, String custodianPublicAddress, CurrencyPair pair, String exchangeName) {
        this.ip = ip;
        this.port = port + 1;
        this.rpcUsername = rpcUser;
        this.rpcPassword = rpcPass;
        this.useIdentifier = useIdentifier;
        this.custodianPublicAddress = custodianPublicAddress;
        this.exchangeName = exchangeName;
        this.pair = pair;
        this.connected = false;
    }


    public ApiResponse getInfo() {
        ApiResponse toRet = new ApiResponse();

        ApiResponse response = invokeRPC(UUID.randomUUID().toString(), COMMAND_GET_INFO, null);
        if (response.isPositive()) {
            JSONObject json = (JSONObject) response.getResponseObject();
            toRet.setResponseObject(json.get("result"));
        } else {
            toRet = response;
        }

        return toRet;

    }

    public void checkConnection() {
        boolean conn = false;
        ApiResponse response = this.getInfo();
        if (response.isPositive()) {
            JSONObject responseObject = (JSONObject) response.getResponseObject();

            if (responseObject.containsKey("blocks")) {
                conn = true;
            }

            boolean locked = false;
            if (responseObject.containsKey("unlocked_until")) {
                long lockedUntil = (long) responseObject.get("unlocked_until");
                if (lockedUntil == 0) {
                    LOG.warn("Nu client is locked and will not be able to submit liquidity info."
                            + "\nUse walletpassphrase <yourpassphrase> 9999999 to unlock it");
                }
            } else {
                LOG.warn("Nu client is locked and will not be able to submit liquidity info."
                        + "\nUse walletpassphrase <yourpassphrase> 9999999 to unlock it");
            }

        } else {
            LOG.error("There was a problem checking connection with Nud " + response.getError());
        }


        this.setConnected(conn);
    }


    public boolean isConnected() {
        return this.connected;
    }

    private void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;

    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;

    }

    public String getRpcUsername() {
        return rpcUsername;
    }

    public void setRpcUsername(String rpcUsername) {
        this.rpcUsername = rpcUsername;

    }

    public String getRpcPassword() {
        return rpcPassword;
    }

    public void setRpcPassword(String rpcPassword) {
        this.rpcPassword = rpcPassword;

    }

    public ApiResponse submitLiquidityInfo(String currencyChar, double buyamount, double sellamount, int tier) {
        /*
         * String[] params = { USDchar,buyamount,sellamount,custodianPublicAddress, identifier* };
         * identifier default empty string
         */

        ApiResponse toRet = new ApiResponse();

        List params;
        if (useIdentifier) {
            params = Arrays.asList(currencyChar, buyamount, sellamount, custodianPublicAddress, Utils.generateIdentifier(tier, Global.options.getPair(), Global.options.getExchangeName()));
        } else {
            params = Arrays.asList(currencyChar, buyamount, sellamount, custodianPublicAddress);
        }

        LOG.debug("RPC parameters " + params.toString());
        ApiResponse resp = invokeRPC(UUID.randomUUID().toString(), COMMAND_LIQUIDITYINFO, params);
        if (resp.isPositive()) {
            JSONObject json = (JSONObject) resp.getResponseObject();
            LOG.debug("RPC : Liquidity info submitted correctly.");

            String resultStr = (String) json.get("result");
            if (resultStr != null) {
                toRet.setResponseObject(true);
            } else {
                JSONObject errorObj = (JSONObject) json.get("error");
                int errorCode = (int) ((long) errorObj.get("code"));
                toRet.setError(new ApiError(errorCode, (String) errorObj.get("message")));
            }
        } else {
            toRet.setResponseObject(false);
        }

        return toRet;
    }

    public ApiResponse getLiquidityInfo(String currency) {
        List params = Arrays.asList(currency);

        ApiResponse toRet = new ApiResponse();
        ApiResponse resp = invokeRPC(UUID.randomUUID().toString(), COMMAND_GETLIQUIDITYINFO, params);
        if (resp.isPositive()) {
            JSONObject json = (JSONObject) resp.getResponseObject();
            if (json.containsKey("result") && json.get("result") != "null") {
                toRet.setResponseObject(json.get("result"));
            } else {
                JSONObject errorObj = (JSONObject) json.get("error");
                int errorCode = (int) ((long) errorObj.get("code"));
                toRet.setError(new ApiError(errorCode, (String) errorObj.get("message")));
            }
        } else {
            toRet = resp;
        }

        return toRet;
    }

    public double getLiquidityInfo(String currency, String type, String address) {
        //String[] params = { USDchar,buyamount,sellamount,custodianPublicAddress };
        List params = Arrays.asList(currency);
        double toRet = -1;

        ApiResponse liquidityResp = getLiquidityInfo(currency);
        if (liquidityResp.isPositive()) {
            JSONObject result = (JSONObject) liquidityResp.getResponseObject();
            JSONObject total = (JSONObject) result.get("total");
            if (type.equalsIgnoreCase(Constant.SELL)) {
                toRet = (double) total.get("sell");
            } else if (type.equalsIgnoreCase(Constant.BUY)) {
                toRet = (double) total.get("buy");
            } else {
                LOG.error("The type can be either buy or sell");
            }
        } else {
            LOG.error(liquidityResp.getError().toString());
        }

        return toRet;

    }

    private ApiResponse invokeRPC(String id, String method, List params) {
        ApiResponse toRet = new ApiResponse();
        String answer;

        String url = "http://" + this.ip + ":" + this.port;
        String postJSONString = "{\"id\" : \"" + id + "\", \"method\" : \"" + method + "\"";

        if (params != null) {
            postJSONString += ",";

            JSONArray array = new JSONArray();
            array.addAll(params);

            postJSONString += "\"params\" : " + array.toJSONString();
        }
        postJSONString += "}";

        LOG.debug("postData = " + postJSONString);

        // add header
        Header[] headers = new Header[1];
        headers[0] = new BasicHeader("Content-Type", "application/json");

        HttpPost post = null;
        HttpResponse response = null;


        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        credsProvider.setCredentials(
                new AuthScope(this.ip, this.port),
                new UsernamePasswordCredentials(this.rpcUsername, this.rpcPassword));

        CloseableHttpClient client = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build();

        try {
            post = new HttpPost(url);
            StringEntity paramsEntyty = new StringEntity(postJSONString);
            post.setEntity(paramsEntyty);
            post.setHeaders(headers);
            response = client.execute(post);

        } catch (IOException e) {
            post.abort();
            LOG.error(e.toString());
            toRet.setError(new ApiError(-1, e.getMessage()));
        }

        BufferedReader rd;

        if (response.getStatusLine().getStatusCode() > 400) {
            LOG.debug("HttpCode : " + response.getStatusLine().getStatusCode() + ":" + response.getStatusLine().getReasonPhrase());
            toRet.setError(new ApiError(response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase()));
        } else {

            try {
                rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    buffer.append(line);
                }

                answer = buffer.toString();

                try {
                    JSONObject answerJSON = (JSONObject) new JSONParser().parse(answer);
                    toRet.setResponseObject(answerJSON);
                    LOG.debug("Nud responded : " + answerJSON.toString());

                } catch (ParseException e) {
                    LOG.error(e.getMessage());
                    toRet.setError(new ApiError(-1, e.getMessage()));
                }

            } catch (IOException ex) {
                LOG.error(ex.toString());
                toRet.setError(new ApiError(-1, ex.getMessage()));

            } catch (IllegalStateException ex) {
                LOG.error(ex.toString());
                toRet.setError(new ApiError(-1, ex.getMessage()));

            }

            LOG.trace("\nSending request to URL : " + url);
            LOG.trace("Response Code : " + response.getStatusLine().getStatusCode());
            LOG.trace("Response :" + response);
        }

        return toRet;
    }

}
