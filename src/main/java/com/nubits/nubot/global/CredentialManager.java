package com.nubits.nubot.global;

/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


import com.nubits.nubot.launch.CLIOptions;
import com.nubits.nubot.utils.FilesystemUtils;
import com.nubits.nustringencrypt.StringEncrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class CredentialManager {

    private static final Logger LOG = LoggerFactory.getLogger(CredentialManager.class.getName());
    StringEncrypt stringEncrypt = new StringEncrypt();
    private String encryptionKey = "";
    private boolean bypassNuAuth = false;

    private final String TEST_VALIDITY_KEY_ENCRYPTED = "q1n3xHUdDDQ=:r35Jd5lylhpTizuBdSry6A=="; //Use this to test the decryption key
    private final String TEST_VALIDITY_KEY_DECRYPTED = "nupegk33per";


    private Map<String, String> keyMap = new HashMap<>();

    public static final String KEYSTORE_ENCRYPTION_PASS_KEY = "KEYSTORE_ENCRYPTION_PASS";
    public static final String SMTP_USERNAME_KEY = "SMTP_USERNAME";
    public static final String SMTP_PASSWORD_KEY = "SMTP_PASSWORD";
    public static final String SMTP_HOST_KEY = "SMTP_HOST";
    public static final String HIPCHAT_NOTIFICATIONS_ROOM_ID_KEY = "HIPCHAT_NOTIFICATIONS_ROOM_ID";
    public static final String HIPCHAT_NOTIFICATIONS_ROOM_TOKEN_KEY = "HIPCHAT_NOTIFICATIONS_ROOM_TOKEN";
    public static final String HIPCHAT_CRITICAL_ROOM_ID_KEY = "HIPCHAT_CRITICAL_ROOM_ID"; //use for critical notifications
    public static final String HIPCHAT_CRITICAL_ROOM_TOKEN_KEY = "HIPCHAT_CRITICAL_ROOM_TOKEN";
    public static final String OPEN_EXCHANGE_RATES_APP_ID_KEY = "OPEN_EXCHANGE_RATES_APP_ID";
    public static final String EXCHANGE_RATE_LAB_KEY = "EXCHANGE_RATE_LAB";
    public static final String STREAMER_TOKEN_KEY = "STREAMER_TOKEN";
    public static final String GITTER_ACCESS_TOKEN = "GITTER_ACCESS_TOKEN";
    public static final String GITTER_NOTIFICATIONS_ROOM_ID = "GITTER_NOTIFICATIONS_ROOM_ID";
    public static final String GITTER_CRITICAL_ROOM_ID = "GITTER_CRITICAL_ROOM_ID";
    public static final String LIQUIDITY_PROVIDER_KEY = "LIQUIDITY_PROVIDER";


    public static CredentialManager createCredentialManager(boolean bypassNuAuth) throws Exception {
        if (bypassNuAuth) {
            return new CredentialManager();//will use credentials defined in this constructor
        } else {
            String masterkey = null;
            try {
                masterkey = readEncryptionKeyFromFile(Settings.PATH_TO_AUTH_FILE);
            } catch (Exception e) {
                LOG.error("Error in reading encryption key from auth file :" + Settings.PATH_TO_AUTH_FILE);
                throw e;
            }

            CredentialManager am = new CredentialManager(masterkey);
            if (am.isAuthValid()) {
                return new CredentialManager(masterkey); //will use credentials provided by nu dev team, with a valid auth file
            } else {
                throw new Exception("Invalid Authorization key found in " + Settings.PATH_TO_AUTH_FILE);
            }
        }
    }

    private static String readEncryptionKeyFromFile(String pathToAuthFile) throws Exception {
        String toReturn = null;
        try {
            toReturn = FilesystemUtils.readFromFile(pathToAuthFile);
        } catch (Exception e) {
            LOG.error("You need a valid " + pathToAuthFile + "file  to run NuBot, or launch from CLI adding the -" + CLIOptions.BYPASS_NUDEV_AUTH + " flag. Get in touch with the Nu Dev team via http://discuss.nubits.com request a token by filling form @ " + Settings.REQUEST_TOKEN_FORM);
            throw e;
        }
        return toReturn;
    }


    private CredentialManager(String encryptionKey) { //use createCredentialManager() to instantiate a new object
        this.encryptionKey = encryptionKey;
        /***
         *   NuDev's own ecnrypted keys. ask for decryption key http://discuss.nubits.com
         ***/
        keyMap.put(KEYSTORE_ENCRYPTION_PASS_KEY, "+vOZwZyDLsw=:2n8CS6OZNzkI6Cc4iCNAPw==");
        keyMap.put(SMTP_USERNAME_KEY, "CJAIW9ACURI=:1rP6TBnw+yi1IkOoPBSvM8dAvpmOnqOOtNBLlJfWEOs=");//used to send mail notifications
        keyMap.put(SMTP_PASSWORD_KEY, "HVcVMwylzZs=:pQ65LhKTmo7GdYOesr5tSg==");//used to send mail notifications
        keyMap.put(SMTP_HOST_KEY, "LSE1/QwoGJE=:wgqKtDcqRVHCC/b4MAlFJxcKzltAwaDiCPJu52BWFUk=");
        keyMap.put(HIPCHAT_NOTIFICATIONS_ROOM_ID_KEY, "1EdhWLdm4tg=:+wPTB0xf6IKUnkuZx9PQGQ==");
        keyMap.put(HIPCHAT_NOTIFICATIONS_ROOM_TOKEN_KEY, "C5Wqtj3RsTk=:W7IQzuG9rAwj4b4o6wN0m1c2tF2NGh+gP6/SU8Q3OPbfVQ23i4qQTcxA7fVotsS0");
        keyMap.put(HIPCHAT_CRITICAL_ROOM_ID_KEY, "hECRRW0eiyY=:RLWECsmClSIFhE5+NDA0Hw=="); //use for critical notifications
        keyMap.put(HIPCHAT_CRITICAL_ROOM_TOKEN_KEY, "xDLv0OTMU/0=:tzKIcjkipSGxqtfRLDoZmww6JOm3NhDfoCEVjH33BOnKpLMW7xme8DjnQLcsjyH2"); //use for critical notifications
        keyMap.put(OPEN_EXCHANGE_RATES_APP_ID_KEY, "COXwc/x/CHg=:Rt+kdGSM9vN+abij8xw1FXftuPAf+2cnEWULKGD2Fdx7x9W2asKCFKX5kF/an4BY"); //https://openexchangerates.org/api/latest.json?app_id=<here>
        keyMap.put(EXCHANGE_RATE_LAB_KEY, "aF0wwy5oOKw=:XIhL2IPsaZcXlTmhlFWc3jheybwUGNhx+RdcGpA+LLRblHA5QOb3Ff6+XJb3XJ6l");//http://api.exchangeratelab.com/api/current?apikey=<here>
        keyMap.put(STREAMER_TOKEN_KEY, "ZOp6ScGwTHM=:/RxSox5BpQDtQL0ANME5KXp8UjpjXTXBUYE9VawVQos="); //Streamer token
        keyMap.put(GITTER_ACCESS_TOKEN, "WSlce6UQk6k=:PgAzJc8pOtOQtk4kxUnaDZUD91IKFHKr/6oCyWtvwsz5aRPfuZGiXSNsU9wZIVAA"); //Access token for gitter API
        keyMap.put(GITTER_NOTIFICATIONS_ROOM_ID, "kxPHhVCD00w=:FkbuoKI9VBZn90wEMY1FfC+G0YK8iixahHgSWb2/AAw="); //Gitter ID of notification room
        keyMap.put(GITTER_CRITICAL_ROOM_ID, "x+bzoE6YBAM=:UeA0Croj6dc/K7T9MutSTAmUkrJiMXnBTbr+1/kFNFw="); //Gitter ID of critical notification room
        keyMap.put(LIQUIDITY_PROVIDER_KEY, "hlLySG5q9Ks=:cmyKR2BI+zzpidLU5knbyKG5mynJfyWMdHDCijF22Gs="); //The token used by ALP

    }


    private CredentialManager() { //This constructor is used to bypass use of predefined keys
        bypassNuAuth = true;
        /***
         *   To run NuBot with your own keys, fill the values below.
         ***/
        keyMap.put(KEYSTORE_ENCRYPTION_PASS_KEY, "");//java keystore password
        keyMap.put(SMTP_USERNAME_KEY, "");//used to send mail notifications
        keyMap.put(SMTP_PASSWORD_KEY, "");//used to send mail notifications
        keyMap.put(SMTP_HOST_KEY, "");//used to send mail notifications
        keyMap.put(HIPCHAT_NOTIFICATIONS_ROOM_ID_KEY, "");//used to send hipchat notifications
        keyMap.put(HIPCHAT_NOTIFICATIONS_ROOM_TOKEN_KEY, "");
        keyMap.put(HIPCHAT_CRITICAL_ROOM_ID_KEY, ""); //use for critical notifications
        keyMap.put(HIPCHAT_CRITICAL_ROOM_TOKEN_KEY, ""); //use for critical notifications
        keyMap.put(OPEN_EXCHANGE_RATES_APP_ID_KEY, ""); //https://openexchangerates.org/api/latest.json?app_id=<here>
        keyMap.put(EXCHANGE_RATE_LAB_KEY, "");//http://api.exchangeratelab.com/api/current?apikey=<here>
        keyMap.put(STREAMER_TOKEN_KEY, ""); //Streamer token
        keyMap.put(GITTER_ACCESS_TOKEN, ""); //Access token for gitter API
        keyMap.put(GITTER_CRITICAL_ROOM_ID, "");//Gitter ID of critical notification room
        keyMap.put(GITTER_NOTIFICATIONS_ROOM_ID, ""); //Gitter ID of notification room
        keyMap.put(LIQUIDITY_PROVIDER_KEY, "");//The token used by ALP

    }

    public String get(String key) {

        if (keyMap.containsKey(key)) {
            if (bypassNuAuth) {
                return (keyMap.get(key));
            } else {
                return stringEncrypt.decrypt(keyMap.get(key), encryptionKey);
            }
        } else {
            LOG.error("CredentialManager doesn't have a value for " + key);
            return "";
        }
    }

    public boolean isAuthValid() {
        if (encryptionKey == null || encryptionKey == "") {
            LOG.error("decryption key has not being set");
            return false;
        } else {
            String encryptedTest = stringEncrypt.encrypt(TEST_VALIDITY_KEY_DECRYPTED, encryptionKey);
            String decryptedTest = stringEncrypt.decrypt(encryptedTest, encryptionKey);
            if (encryptedTest.equals(TEST_VALIDITY_KEY_ENCRYPTED)
                    && decryptedTest.equals(TEST_VALIDITY_KEY_DECRYPTED)) {
                return true;
            } else {
                LOG.error("The encryption key is not valid. Get in touch with the Nu Dev team via http://discuss.nubits.com or request a token by filling form @ " + Settings.REQUEST_TOKEN_FORM);
                return false;
            }
        }
    }

    public String getEncryptionKey() {
        return encryptionKey;
    }

    public void setEncryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
    }
}
