/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubotstream;

import com.nubits.nubot.global.Settings;
import com.nubits.nubot.models.*;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.pricefeeds.FeedQuality;
import com.nubits.nubot.pricefeeds.PriceBatch;
import com.nubits.nubot.pricefeeds.PriceFeedManager;
import com.nubits.nubot.utils.TimeUtils;
import com.nubits.nubot.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Used to encapsulate a currency tracker
 */
public class StreamerCurrencyTracker implements Runnable {
    final static Logger LOG = LoggerFactory.getLogger(StreamerCurrencyTracker.class);

    private Currency currency;
    private CurrencyPair pair;
    private int socketPort;
    private String token;
    private long fetchInterval;
    private long fetchIntervalInitial;
    private double spread;
    private double wallShiftTreshold;
    private ZMQ.Context context;
    private int firstSocketPort;

    private PriceFeedManager pricefeedManager;
    private ZMQ.Socket pricePublisher;
    private String lastMessageSent = "";
    private double lastPriceFetched = -1;
    private double lastPriceSent = -1;

    private int countConsecutiveFailures = 0;
    private Set<String> connectedClients;


    public StreamerCurrencyTracker(Currency currency, int socketPort, String token, double spread, double wallShiftTreshold, long fetchInterval, ZMQ.Context context) {
        this.currency = currency;
        this.pair = new CurrencyPair(currency, CurrencyList.USD);
        this.socketPort = socketPort;
        this.firstSocketPort = socketPort + 100;
        this.token = token;
        this.spread = spread;
        this.wallShiftTreshold = wallShiftTreshold;
        this.fetchInterval = fetchInterval;
        this.fetchIntervalInitial = fetchInterval;
        this.context = context;
        connectedClients = new HashSet<>();

        try {
            pricefeedManager = new PriceFeedManager(currency.getDefaultMainFeedName(), currency.getDefaultbackupFeedNames(), pair);
        } catch (NuBotConfigException e) {
            pricefeedManager = null;
            LOG.error(e.toString());
        }
    }

    @Override
    public void run() {
        LOG.debug("[" + currency.getCode() + "] Starting " + this.toString());
        startFirstReplier(context);
        String connectionStringPub = "tcp://*:" + socketPort;

        LOG.warn("[" + currency.getCode() + "] Opening socket to accept price subscriptions : " + connectionStringPub);

        pricePublisher = context.socket(ZMQ.PUB);
        pricePublisher.bind(connectionStringPub);

        //continuously stream

        while (!Thread.currentThread().isInterrupted()) {
            publishIfTriggered();
            try {
                Thread.sleep(fetchInterval);
            } catch (Exception e) {
                LOG.error(e.toString());
            }
        }

        pricePublisher.close();
    }

    private void startFirstReplier(ZMQ.Context context) {

        String connectionStringRep = "tcp://*:" + firstSocketPort;
        LOG.warn("[" + currency.getCode() + "] Opening socket to send prices for the first time on : " + connectionStringRep);
        ZMQ.Socket replier = context.socket(ZMQ.REP);
        replier.bind(connectionStringRep);
        //start a thread which will respond to the requests about the last message

        ScheduledExecutorService scheduler =
                Executors.newScheduledThreadPool(1);

        final Runnable responderTask = new Runnable() {
            public void run() {
                while (true) {

                    byte[] req = replier.recv(0);
                    String sreq = new String(req, ZMQ.CHARSET);

                    //Format : token id start|stop
                    LOG.debug("[" + currency.getCode() + "] Received " + sreq);
                    StringTokenizer st = new StringTokenizer(sreq);
                    String currencyToken = "";
                    String id = "";
                    String action = ""; //"start" indicate first connection, "stop" indicate shutdown

                    String reply;

                    //Check the message format
                    if (st.countTokens() == 3) {
                        //Parse
                        currencyToken = (String) st.nextElement();
                        id = (String) st.nextElement();
                        action = (String) st.nextElement();

                        if (currencyToken.equals(token)) {

                            if (action.equals("start")) {
                                //Check if a client with the same id is already connected
                                if (!connectedClients.contains(id)) {
                                    connectedClients.add(id);
                                    LOG.debug("[" + currency.getCode() + " ]new bot requested conection " + id);
                                    LOG.debug("[" + currency.getCode() + " ]Bot currently authorized: " + connectedClients.size());

                                    reply = lastMessageSent;

                                } else {
                                    LOG.warn("[" + currency.getCode() + " ]A client (" + id + " is trying to connect again");
                                    reply = "You are already connected with sessionID=" + id + ". Only one connection per session accepted";
                                }

                            } else if (action.equals("stop")) {
                                //Check if a client with the same id is already connected
                                if (connectedClients.contains(id)) {
                                    connectedClients.remove(id);
                                    LOG.debug("[" + currency.getCode() + " ]bot disconnected " + id);
                                    LOG.debug("[" + currency.getCode() + "] Clients currently authorized: " + connectedClients.size());
                                    reply = StreamMessageFacade.COMMAND_TERMINATE_CONNECTION;
                                } else {
                                    LOG.warn("[" + currency.getCode() + "] A client (" + id + " requested disconnection but wasn't in the active list");
                                    reply = StreamMessageFacade.COMMAND_TERMINATE_CONNECTION;
                                }
                            } else {
                                reply = "invalid action: " + action + ". Admitted values are 'start' and 'stop'";
                            }

                        } else {
                            reply = "invalid token: " + sreq;
                        }

                    } else {
                        LOG.warn("[" + currency.getCode() + "] Wrong message format received:" + sreq);
                        reply = "Wrong message format :" + sreq;
                    }
                    replier.send(reply.getBytes(ZMQ.CHARSET), 0);
                }
            }
        };


        final ScheduledFuture<?> taskHandle =
                scheduler.schedule(responderTask, 1, SECONDS);

    }


    /**
     * publish one round of updates
     */

    private void publishIfTriggered() {
        Utils.printSeparator();
        LOG.info("[" + currency.getCode() + "] Executing fetch routine for " + connectedClients.size() + " connected clients.");
        int i = 0;
        for (String bot : connectedClients) {
            i++;
            LOG.debug("client " + i + " : " + bot);
        }
        LOG.debug(connectedClients.toString());
        pricefeedManager.fetchLastPrices();
        ArrayList<LastPrice> currentPriceList = pricefeedManager.getPriceBatch().priceList;

        boolean gotall = currentPriceList.size() == pricefeedManager.getFeedList().size();
        LastPrice goodPrice;
        if (gotall) {
            goodPrice = FeedQuality.handleNormal(Settings.PRICE_DISTANCE_MAX_PERCENT, new PriceBatch(currentPriceList));
            LOG.debug("[" + currency.getCode() + "] goodprice: " + goodPrice);
        } else {
            goodPrice = FeedQuality.handleFail(Settings.PRICE_DISTANCE_MAX_PERCENT, new PriceBatch(currentPriceList));
            LOG.debug("[" + currency.getCode() + "] goodprice: " + goodPrice);
        }

        if (goodPrice != null) {
            countConsecutiveFailures = 0;
            //restore fetchInterval
            this.fetchInterval = fetchIntervalInitial;

            lastPriceFetched = goodPrice.getPrice().getQuantity();
            double currentGoodPrice = goodPrice.getPrice().getQuantity();
            boolean triggered = isTriggered(currentGoodPrice, lastPriceSent);
            LOG.debug("[" + currency.getCode() + "] triggered: " + triggered);
            if (triggered) {
                //now prepare the message to publish

                BidAskPair baUSD = calcDefaultSpreadUSD(currentGoodPrice);
                BidAskPair baPEG = calcDefaultSpreadPEG(currentGoodPrice);

                //Prepare the args
                String[] commandsArgs = new String[9];
                commandsArgs[0] = pair.toStringSep();
                commandsArgs[1] = Double.toString(currentGoodPrice);
                commandsArgs[2] = Double.toString(baUSD.getAsk());
                commandsArgs[3] = Double.toString(baUSD.getBid());
                commandsArgs[4] = Double.toString(baPEG.getAsk());
                commandsArgs[5] = Double.toString(baPEG.getBid());
                commandsArgs[6] = goodPrice.getSource();
                commandsArgs[7] = Double.toString(wallShiftTreshold);
                commandsArgs[8] = Double.toString(spread);

                String error = "";
                //Attach price list
                Object[] attachments = new Object[1];
                attachments[0] = currentPriceList;
                String serverTime = TimeUtils.GetUTCdatetimeAsString();
                String token = this.token;

                //String[] commandsArgs, String error, Object[] attachments, long serverTimestamp, String token

                ShiftOrdersMessage sm = new ShiftOrdersMessage(commandsArgs, error, attachments, serverTime, token);

                String messageString = sm.toString();
                messageString = Utils.minify(messageString, false);
                LOG.info("[" + currency.getCode() + "] publish on port " + socketPort + ": " + messageString);
                lastMessageSent = messageString;
                lastPriceSent = currentGoodPrice;

                //now send message to all clients
                pricePublisher.send(messageString, 0); //0 => no multimessage
            }

        } else {
            countConsecutiveFailures++;
            //Send an error
            String error = StreamMessageFacade.ERROR_CANT_FETCH + "Stream server cannot obtain a reliable price update for [" + currency.getCode() + "]. Consecutive failures :  " + countConsecutiveFailures;
            LOG.error(error);
            //Attach details of feeds received
            Object[] attachments = new Object[2];
            attachments[0] = currentPriceList;
            attachments[1] = countConsecutiveFailures;

            String serverTime = TimeUtils.GetUTCdatetimeAsString();
            ShiftOrdersMessage sm = new ShiftOrdersMessage(new String[9], error, attachments, serverTime, token);
            String messageString = sm.toString();
            LOG.info("[" + currency.getCode() + "] publish on port " + socketPort + ": " + messageString);

            pricePublisher.send(messageString, 0); //0 => no multimessage

            //temporarily Reduce interval, in case of fiat
            this.fetchInterval = Settings.FEEDFETCH_DEFAULT_INTERVAL;

        }

    }


    /**
     * determine whether a push is triggered
     *
     * @param lastPriceFetched
     * @param lastPriceSent
     * @return
     */
    private boolean isTriggered(double lastPriceFetched, double lastPriceSent) {
        //last was not sent before, so true always
        if (lastPriceSent == -1)
            return true;

        double movedPercent = Utils.percentagePriceVariation(lastPriceSent, lastPriceFetched);
        LOG.debug("[" + currency.getCode() + "] priced moved by " + Utils.formatNumber(movedPercent, 4) + " %");
        if (movedPercent >= wallShiftTreshold) {
            return true;
        } else
            return false;
    }


    private BidAskPair calcDefaultSpreadUSD(double midprice) {
        double halfSpread = midprice * (spread / 100) / 2;
        double bid = Utils.round(midprice - halfSpread);
        double ask = Utils.round(midprice + halfSpread);
        BidAskPair ba = new BidAskPair(bid, ask);
        return ba;
    }

    private BidAskPair calcDefaultSpreadPEG(double midprice) {
        double pegmid = 1 / midprice;
        double halfSpread = pegmid * (spread / 100) / 2;
        double bid = Utils.round(pegmid - halfSpread);
        double ask = Utils.round(pegmid + halfSpread);
        BidAskPair ba = new BidAskPair(bid, ask);
        return ba;
    }


    //Getters and setters

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getSocketPort() {
        return socketPort;
    }

    public void setSocketPort(int socketPort) {
        this.socketPort = socketPort;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public PriceFeedManager getPricefeedManager() {
        return pricefeedManager;
    }

    public void setPricefeedManager(PriceFeedManager pricefeedManager) {
        this.pricefeedManager = pricefeedManager;
    }

    @Override
    public String toString() {
        return "StreamerCurrencyTracker{" +
                "currency=" + currency +
                ", pair=" + pair +
                ", socketPort=" + socketPort +
                ", token='" + token + '\'' +
                ", fetchInterval=" + fetchInterval +
                ", spread=" + spread +
                ", wallShiftTreshold=" + wallShiftTreshold +
                ", firstSocketPort=" + firstSocketPort +
                '}';
    }
}
