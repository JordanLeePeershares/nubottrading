/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package com.nubits.nubotstream;

public class StreamMessageFacade {
    public static final String COMMAND_SHIFT_WALLS = "shiftWalls";
    public static final String COMMAND_INIT_SOCKET = "initSocket";

    public static final String COMMAND_TERMINATE_CONNECTION = "connectionTerminated";

    //errors descriptions start with
    public static final String ERROR_CANT_FETCH = "Cannot fetch. ";

}
