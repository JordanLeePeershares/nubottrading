/*
 * Copyright (C) 2015 Nu Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package functions;


import com.nubits.nubot.exchanges.ExchangeFacade;
import com.nubits.nubot.global.Settings;
import com.nubits.nubot.options.NuBotConfigException;
import com.nubits.nubot.options.NuBotOptions;
import com.nubits.nubot.options.NuBotOptionsDefault;
import com.nubits.nubot.options.ParseOptions;
import com.nubits.nubot.utils.FilesystemUtils;
import com.nubits.nubot.utils.JSONUtils;
import junit.framework.TestCase;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestOptions extends TestCase {

    private static String testconfigFile = "latest/test.json";
    private static String testconfig = Settings.TESTS_CONFIG_PATH + "/" + testconfigFile;

    @Override
    public void setUp() {

    }

    @Test
    public void testConfigExists() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        final String wdir = FilesystemUtils.getBotAbsolutePath();

        File f = new File(testconfig);
        assertTrue(f.exists());
    }

    @Test
    public void testLoadconfig() {
        try {
            JSONObject inputJSON = ParseOptions.parseSingleJsonFile(testconfig);
            assertTrue(inputJSON.keySet().size() > 0);
        } catch (ParseException e) {
            // e.printStackTrace();
        }
    }

    @Test
    public void testJson() {
        try {
            JSONObject inputJSON = ParseOptions.parseSingleJsonFile(testconfig);

            assertTrue(JSONUtils.containsIgnoreCase(inputJSON, "exchangename"));
        } catch (ParseException e) {
            // e.printStackTrace();
        }
    }

    @Test
    public void testLoadConfigJson() {
        // used file
        // {"options":
        // {
        // "dualside": true,
        // "multiple-custodians":false,
        // "submit-liquidity":true,
        // "executeorders":false,
        // "verbose":false,
        // "gitter":true,
        // "mail-notifications":false,
        // "mail-recipient":"xxx@xxx.xxx",
        // "emergency-timeout":60,
        // "keep-proceeds":0,
        // "max-sell-order-volume" : 0,
        // "max-buy-order-volume" : 0,
        // "priceincrement": 0.1,
        // }
        // }

        try {
            // System.out.println(System.getProperty("));
            // Global.options =
            JSONObject j = ParseOptions
                    .parseSingleJsonFile(testconfig);

            assertTrue(j.get("verbosity").toString().equalsIgnoreCase("high"));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLoadConfig() {
        try {
            NuBotOptions nuo = ParseOptions
                    .parseOptionsSingle(testconfig, false);

            assertTrue(nuo != null);

            assertTrue(nuo.exchangeName.equals("poloniex"));

            //assertTrue(nuo.getPair() != null);

            //assertTrue(nuo.getSecondaryPegOptions() != null);
            //.getSpread())

        } catch (NuBotConfigException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLoadComplete() {

        String testconfig = Settings.TESTS_CONFIG_PATH + "/" + testconfigFile;
        boolean catched = false;

        try {
            JSONObject inputJSON = ParseOptions.parseSingleJsonFile(testconfig);
            //assertTrue(inputJSON.containsKey("options"));
            //JSONObject optionsJSON = ParseOptions.getOptionsKey(inputJSON);
            assertTrue(JSONUtils.containsIgnoreCase(inputJSON, "exchangename"));
        } catch (Exception e) {

        }
        assertTrue(!catched);
    }

    @Test
    public void testLoadOptions() {

        String testconfig = Settings.TESTS_CONFIG_PATH + "/" + testconfigFile;

        boolean catched = false;
        try {
            NuBotOptions opt = ParseOptions.parseOptionsSingle(testconfig, false);

            assertTrue(opt.isSubmitLiquidity() == false);

        } catch (NuBotConfigException e) {
            System.out.println("could not parse config");
            System.out.println(e);
            catched = true;
        }

        assertTrue(!catched);

    }

    @Test
    public void testLoadOptionsAll() {

        String testconfig = Settings.TESTS_CONFIG_PATH + "/" + testconfigFile;

        boolean catched = false;
        NuBotOptions opt = null;
        try {
            opt = ParseOptions.parseOptionsSingle(testconfig, false);

        } catch (NuBotConfigException e) {
            System.out.println("error " + e);
            catched = true;
        }

        assertTrue(!catched);


        assertTrue(opt.getPair() != null);
        //assertTrue(opt.getPair().equals("nbt_btc"));


    }


    @Test
    public void testToJson() {

        NuBotOptions opt = NuBotOptionsDefault.defaultFactory();
        assertTrue(opt != null);

        String jsonString = NuBotOptions.optionsToJSONString(opt);

        assertTrue(jsonString.length() > 0);

        JSONParser jsonparser = new JSONParser();
        JSONObject optionJson = null;
        try {
            optionJson = (JSONObject) (jsonparser.parse(jsonString));
        } catch (Exception e) {

        }

        System.out.println(optionJson);

        for (int i = 0; i < ParseOptions.allkeys.length; i++) {
            String f = ParseOptions.allkeys[i];
            System.out.println(f);
            //assertTrue(optionJson.containsKey(check));
            assertTrue(JSONUtils.containsIgnoreCase(optionJson, f));
        }


    }

    @Test
    public void testRoundTrip() {


        String jsonString = "{\n" +
                "  \"exchangename\":\"Poloniex\",\n" +
                "  \"apiKey\": \"def\",\n" +
                "  \"apiSecret\": \"abc\",\n" +
                "  \"executeorders\":true,\n" +
                "  \"txfee\": 0.0,\n" +
                "  \"pair\":\"nbt_btc\",\n" +
                "  \"submitliquidity\":false,\n" +
                "  \"nubitaddress\": \"xxx\",\n" +
                "  \"nudip\": \"127.0.0.1\",\n" +
                "  \"nudport\": 9091,\n" +
                "  \"rpcpass\": \"xxx\",\n" +
                "  \"rpcuser\": \"xxx\",\n" +
                "  \"mainfeed\":\"blockchain\",\n" +
                "  \"backupfeeds\": [\"coinbase\", \"btce\"],\n" +
                "  \"wallchangeThreshold\": 0.1,\n" +
                "  \"dualside\": true,\n" +
                "  \"multipleoperators\":false,\n" +
                "  \"verbosity\":\"low\",\n" +
                "  \"gitter\":true,\n" +
                "  \"mailnotifications\":\"ALL\",\n" +
                "  \"mailrecipient\":\"test@gmail.com\",\n" +
                "  \"emergencytimeout\":60,\n" +
                "  \"keepproceeds\":0,\n" +
                "  \"bookSellwall\" : 10.0,\n" +
                "  \"bookBuywall\" : 10.0,\n" +
                "  \"bookDisabletier2\" : false,\n" +
                "  \"priceincrement\": 0.1,\n" +
                "  \"bypassStreaming\":false,\n" +
                "  \"streamingServer\":\"stream.tradingbot.nu:1010\",\n" +
                "  \"wallchangeThreshold\": 0.1,\n" +
                "  \"bookSellInterval\": 0.01,\n" +
                "  \"bookSellOffset\": 0.001,\n" +
                "  \"bookSellType\": \"log\",\n" +
                "  \"bookSellSteepness\": \"mid\",\n" +
                "  \"bookSellMaxVolumeCumulative\": 0,\n" +
                "  \"bookBuyMaxVolumeCumulative\": 0,\n" +
                "  \"bookBuyInterval\": 0.01,\n" +
                "  \"bookBuyOffset\": 0.001,\n" +
                "  \"bookBuyType\": \"lin\",\n" +
                "  \"bookBuySteepness\": \"mid\",\n" +
                "  \"webport\": 8889\n" +
                "}\n";

        JSONParser jsonparser = new JSONParser();
        JSONObject optionJson = null;
        try {
            optionJson = (JSONObject) (jsonparser.parse(jsonString));
        } catch (Exception e) {

        }
        NuBotOptions opt = null;
        try {
            opt = ParseOptions.parseOptionsFromJson(optionJson, false);
        } catch (Exception e) {
            fail(e.toString());
        }

        assertTrue(opt.getExchangeName().equals("Poloniex"));
        assertTrue(opt.getApiKey().equals("def"));
        assertTrue(opt.getApiSecret().equals("abc"));
        assertTrue(opt.isExecuteOrders() == true);
        assertTrue(opt.getTxFee() == 0.0);
        assertTrue(opt.isSubmitLiquidity() == false);
        assertTrue(opt.isBypassStreaming() == false);
        assertTrue(opt.streamingserver.equals("stream.tradingbot.nu:1010"));
        assertTrue(opt.nubitAddress.equals("xxx"));
        assertTrue(opt.rpcUser.equals("xxx"));
        assertTrue(opt.mainFeed.equals("blockchain"));
        assertTrue(opt.isBookDisabletier2() == false);
        assertTrue(opt.isDualSide() == true);
        assertTrue(opt.verbosity != "");
        assertTrue(opt.isGitter() == true);
        assertTrue(opt.getSendMailsLevel().equals("ALL"));
        assertTrue(opt.getMailRecipient().equals("test@gmail.com"));
        assertTrue(opt.getEmergencyTimeout() == 60);
        assertTrue(opt.getBookBuywall() == 10.0);
        assertTrue(opt.getBookSellwall() == 10.0);
        assertTrue(opt.getPriceIncrement() == 0.1);
        assertTrue(opt.getWallchangeThreshold() == 0.1);
        assertTrue(opt.getBackupFeeds().get(0).equals("coinbase"));
        assertTrue(opt.getBackupFeeds().get(1).equals("btce"));
        assertTrue(opt.getWebport() == 8889);

        assertTrue(opt.getBookSellInterval() == 0.01);
        assertTrue(opt.getBookSellOffset() == 0.001);

        assertTrue(opt.getBookBuyInterval() == 0.01);
        assertTrue(opt.getBookBuyOffset() == 0.001);

        assertTrue(opt.getBookBuyMaxVolumeCumulative() == 0);
        assertTrue(opt.getBookSellMaxVolumeCumulative() == 0);


        assertTrue(opt.getBookSellType().equals("log"));
        assertTrue(opt.getBookSellSteepness().equalsIgnoreCase("mid"));
        assertTrue(opt.getBookBuyType().equals("lin"));
        assertTrue(opt.getBookBuySteepness().equalsIgnoreCase("mid"));


    }

    @Test
    public void testFromFile() {

        NuBotOptions newopt = null;
        try {
            newopt = ParseOptions.parseOptionsSingle(testconfig, false);
        } catch (Exception e) {
            fail(e.toString());
        }

        assertTrue(newopt.getExchangeName().equalsIgnoreCase(ExchangeFacade.POLONIEX));

    }

}