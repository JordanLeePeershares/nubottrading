/* main.js
handles
- queries to the server of status
- navigation
- get and post of config
- start/stop Bot functionality
*/

function getBaseUrl() {
    return "http://" + location.host;
}

var baseurl = getBaseUrl();
var orderEndPoint = "orders";
var balanceEndPoint = "balances";
var poolEndPoint = "alpstats";


var hook = true; //flag that prevents/allow the alert on page change to show up

var refreshStatusInterval = 3 * 1000; //ms

/*
 No matter how fast we set the refreshOrders,
 it will be capped by Server
 in Settings.ORDER_MIN_UPDATE and BALANCE_MIN_UPDATE
*/
var refreshOrders = 8 * 1000;
var refreshBalances = 10 * 1000;

var refreshPoolTimeout = 30 * 1000;

var refreshTablesInterval = 500;
var refreshLogInterval = 300;

var pageName = "dashboard";
var debug = true;

var logLine = 0;
var requestedStop = false;

var laddaToggleBtn; //ladda button for starting and stopping the bot

var laddaSavecfgBtn; //ladda button for saving the config
var laddaClearcfgBtn; //ladda button for clearing config
var laddaPreviewBtn; //ladda button for preview orderbook

var progressPB = 0; //current status of progressbar (min 0 max 1)
var incrementPB;
var currentAnimID;
var animatingButton = false;

var serverDown = false;
var mode = ""; // halted, running, starting, halting

/*
 * Alert (once) if running an old version of NuBot
 */
var alertShown = false;




$(document).ready(function() {

    updateConfigFile();

    /*Read get parameters from URL
    example :
    ?alert=true&alertmsg=somealert
    */
    var alertz = $.getQuery('alert');
    if (alertz == "true") {
        var msg = decodeURIComponent($.getQuery('alertmsg'));
        msg = replaceAll("+", " ", msg);
        alertMessage(msg);
    }

    $('#togglebot').click(function() {

        if (mode == "running") {
            stopBot();
        }

        if (mode == "halted") {
            startBot();
        }
    });

}); //end document.ready function


function handleFailServer(moreinfoTitle, moreinfoBody) {
    if (!serverDown) {
        mode = "halted";
        serverDown = true;
        console.log(moreinfoTitle + " " + moreinfoBody);
        $('#maincontainer').html("<h1>NuBot engine is down. Relaunch it and refresh this page</h1> " +
            "<h5>More info: " + moreinfoTitle + " </h5>" +
            "<pre>" + moreinfoBody + "</pre>");
    }

}

function clearTables() {
    $("#ordertable").find("tr:gt(0)").remove();
    $("#balancetable").find("tr:gt(0)").remove();
}

function isBotActive() {
    if (mode == "halted")
        return false;
    else
        return true;
}

function setStateRunning() {
    if (mode != "running") {
        //console.log("set running");
        $('#togglebot-text').html(" Stop Bot");
        $('#togglebot-text').addClass("glyphicon-off");
        $('#togglebot-text').removeClass("glyphicon-play");

        $("#togglebot").attr("data-color", "red");

        $('#logarea').removeClass("stopped-logarea").addClass("running-logarea");

        stopAnimation();
        setNavBarActive();

        mode = "running";
        updatePageTitle();
    }

}

function setStateHalted() {
    if (mode != "halted") {
        //console.log("set halted");
        $('#togglebot-text').html(" Start Bot");
        $('#togglebot-text').addClass("glyphicon-play");
        $('#togglebot-text').removeClass("glyphicon-off");

        $("#togglebot").attr("data-color", "blue");

        setTimeout(clearTables, refreshTablesInterval);

        $('#logarea').removeClass("running-logarea").addClass("stopped-logarea");
        stopAnimation();
        setNavBarActive();
        mode = "halted";
        updatePageTitle();
    }
}

function setStateStarting() {
    if (mode != "starting") {
        //console.log("set starting");
        // Start loading button
        incrementPB = 0.006; //determines speed of progressbar
        laddaToggleBtn.ladda('start');
        currentAnimID = setInterval(animateProgressBar, 50, true);
        $('#togglebot-text').html(" Starting Bot");

        setNavBarInactive();
        mode = "starting";
        updatePageTitle();
    }
}

function setStateHalting() {
    if (mode != "halting") {
        //console.log("set halting");
        incrementPB = 0.006; //determines speed of progressbar
        laddaToggleBtn.ladda('start');
        currentAnimID = setInterval(animateProgressBar, 50, false);
        $('#togglebot-text').html(" Stopping Bot");

        setNavBarInactive();
        mode = "halting";
        updatePageTitle();
    }
}

function updatePageTitle(running) {
    if (mode != "" && mode != null)
        document.title = 'NuBot GUI - ' + mode;
    else {
        if (running)
            document.title = 'NuBot GUI - running';
        else
            document.title = 'NuBot GUI - halted';
    }


}

function startBot() {
    if (confirm("Are you sure you want to start the bot?")) {
        if (debug) console.log("calling start on server");

        setStateStarting();

        var jsondata = JSON.stringify({
            "operation": "start"
        });

        $.ajax(baseurl + "/startstop", {
            data: jsondata,
            dataType: "json",
            type: 'POST',
            success: function(data) {
                //console.log("starting bot ok " + data);
                var success = data["success"];

                if (success) {

                } else {
                    alert(data["error"]);
                    stopProgressBarAnimation(false);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("startBot - error posting to server " + textStatus + " " + errorThrown);
            }
        });
    }
}

function stopBot() {
    if (debug) console.log("calling stop on server");
    if (confirm("Are you sure you want to stop the bot?")) {

        setStateHalting();

        var jsondata = JSON.stringify({
            "operation": "stop"
        });

        $.ajax(baseurl + "/startstop", {
            data: jsondata,
            dataType: "json",
            type: 'POST',
            success: function(data) {
                var success = data["success"];
                //console.log("stopped bot. success " + success);
                var cbtn = $('#togglebot');
                if (success) {
                    //on success of post change the color of the button
                    $('#duration').html("");
                    $('#sessioid').html("");
                } else {
                    alert(data["error"]);
                    stopProgressBarAnimation(false);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("stopBot - error posting to server " + textStatus + " " + errorThrown);
            }
        });
    }
}

function setNavBarInactive() {
    $('#dashboard-nav').html('<a>Dashboard</a>');
    $('#config-nav').html('<a>Configuration</a');
    $('#pool-nav').html('<a>Liquidity Pool</a');
    $('#docu-nav').html('<a>Documentation</a>');
    $('#disclaimer-nav').html('<a>Disclaimer</a>');
}

function setNavBarActive() {
    $('#dashboard-nav').html("<a href='#' onclick=\"changePage('dashboard')\">Dashboard</a>");
    $('#config-nav').html("<a href='#' onclick=\"changePage('config')\">Configuration</a>");
    $('#pool-nav').html("<a href='#' onclick=\"changePage('pool')\">Liquidity Pool</a>");
    $('#docu-nav').html("<a href='#' onclick=\"changePage('docu')\">Documentation</a>");
    $('#disclaimer-nav').html("<a href='#' onclick=\"changePage('disclaimer')\">Disclaimer</a>");
}

function updateNavbar(page) {
    if (mode == "halting") {
        alert("Wait for Bot to halt");
        return;
    }

    if (mode == "starting") {
        alert("Wait for Bot to start");
        return;
    }

    $('#dashboard-nav').removeClass('active');
    $('#config-nav').removeClass('active');
    $('#docu-nav').removeClass('active');

    switch (page) {
        case "dashboard":
            $('#dashboard-nav').addClass('active')
            break;
        case "config":
            $('#config-nav').addClass('active')
            break;
        case "pool":
            $('#pool-nav').addClass('active')
            break;
        case "docu":
            $('#docu-nav').addClass('active')
            break;
        case "disclaimer":
            $('#disclaimer-nav').addClass('active')
            break;
    }
}

function updateConfigFile() {
    $.ajax({
            type: "GET",
            dataType: "json",
            url: baseurl + "/configfile"
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            faildetails = "jqXHR:" + JSON.stringify(jqXHR); + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
            handleFailServer("Error while loading updateConfigFile : /configfile ", faildetails);
        })
        .done(function(data) {
            //console.log("config data: " + data);
            $('#configfile').html("<code>~/" + data["configfile"] + "</code>");

        });
}


var first = true;


function refreshPoolStats() {
    if (isBotActive()) {

        $.ajax({
                type: "GET",
                dataType: "json",
                url: baseurl + "/" + poolEndPoint
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                faildetails = "jqXHR:" + JSON.stringify(jqXHR) + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
                handleFailServer("Error while loading refreshPoolStats() : /" + poolEndPoint, faildetails);
            })
            .done(function(data) {
                updatePoolView(data);
            });
    }

    setTimeout(refreshPoolStats, refreshPoolTimeout);
}

function updatePoolView(data) {
    //console.log(JSON.stringify(data)); //TODO remove

    if (data["connected"] == true && data.hasOwnProperty("connected")) {

        $("#poolstats").show();

        $('#pool-status').html('<h3>Connected to <code>' + data["serverAddress"] + '</code></h3>');

        $("#roundreward").attr("value", data["roundReward"]);
        $("#totalreward").attr("value", data["totalReward"]);
        $("#pooluserid").attr("value", data["poolUserID"]);
        $("#nextpayoutdate").attr("value", data["nextPayoutDate"]);

        $("#lastpayoutdate").attr("value", data["lastPayoutDate"]);
        $("#askpayoutrank1").attr("value", data["askPayoutRank1"]);
        $("#askpayoutrank2").attr("value", data["askPayoutRank2"]);
        $("#bidpayoutrank1").attr("value", data["bidPayoutRank1"]);
        $("#bidpayoutrank2").attr("value", data["bidPayoutRank2"]);
        $("#serveruptime").attr("value", data["serverUpTime"]);

        $('#rawjsonpooldata').html(data["rawHTMLfooter"]);

    } else {
        $('#pool-status').html(" <h2>Offline</h2><div class='alert alert-warning' role='alert'>NuBot not connected to a liquidity pool.</div>");
        $("#poolstats").hide();
    }

}


function updateBalances() {
    //console.log("updatebalance");
    if (isBotActive()) {
        //console.log("updatebalance QUERY. mode="+mode);

        $.ajax({
                type: "GET",
                dataType: "json",
                url: baseurl + "/" + balanceEndPoint
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                faildetails = "jqXHR:" + JSON.stringify(jqXHR) + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
                handleFailServer("Error while loading updateBalances() : /" + balanceEndPoint, faildetails);
            })
            .done(function(data) { //For a sample data see [1] at the bottom of this file :
                //console.log(JSON.stringify(data));
                // prevent update at shutdown

                if (data.hasOwnProperty("pegBalance")) {

                    $("#balancetable").find("tr:gt(0)").remove();

                    var pegTotal = data["pegBalance"]["balanceTotal"];
                    var pegT1 = data["pegBalance"]["balanceT1"];
                    var pegT2 = data["pegBalance"]["balanceT2"];
                    var pegCurrencyCode = data["pegBalance"]["currencyCode"];

                    var pegHTMLrow = '<tr>' +
                        '<td>' + pegCurrencyCode + '</td>' +
                        '<td align=\'right\'>' + pegTotal + '</td>' +
                        '<td align=\'right\'>' + pegT1 + '</td>' +
                        '<td align=\'right\'>' + pegT2 + '</td>' +
                        '</tr>';

                    $("#balancetable").find('tbody').after(pegHTMLrow);

                    var nbtTotal = data["nbtBalance"]["balanceTotal"];
                    var nbtT1 = data["nbtBalance"]["balanceT1"];
                    var nbtT2 = data["nbtBalance"]["balanceT2"];
                    var nbtCurrencyCode = data["nbtBalance"]["currencyCode"];

                    var nbtHTMLrow = '<tr>' +
                        '<td>' + nbtCurrencyCode + '</td>' +
                        '<td align=\'right\'>' + nbtTotal + '</td>' +
                        '<td align=\'right\'>' + nbtT1 + '</td>' +
                        '<td align=\'right\'>' + nbtT2 + '</td>' +
                        '</tr>';

                    $("#balancetable").find('tbody').after(nbtHTMLrow);
                }
            });

    }
    //pipeline the call
    var updatetime = refreshBalances;
    if (first)
        updatetime += 0.5 * refreshBalances;

    setTimeout(updateBalances, updatetime);
    first = false;
}

function updateOrders() {
    //console.log("updateorders");

    if (isBotActive()) {
        //console.log("updateorders QUERY mode="+mode);

        $.ajax({
                type: "GET",
                dataType: "json",
                url: baseurl + "/" + orderEndPoint
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                faildetails = "jqXHR:" + JSON.stringify(jqXHR) + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
                handleFailServer("Error while loading updateOrders() : /" + orderEndPoint, faildetails);
            })
            .done(function(data) {

                //console.log("buys: " + data["buys"]);
                //console.log("BuyCurrency: " + data["BuyCurrency"]);
                //console.log("SellCurrency: " + data["SellCurrency"]);

                if (data.hasOwnProperty("orders")) {

                    $("#ordertable").find("tr:gt(0)").remove();

                    var orders = data["orders"];
                    //updateFavico(orders.length);
                    //console.log("update badge + "+orders.length);
                    for (var i = 0; i < orders.length; i++) {
                        var order = orders[i];
                        var type = order["type"];
                        var qty = order["amount"]["quantity"];
                        var price = order["price"]["quantity"];
                        var rowhtml = '<tr><td>' + type + '</td><td align=\'right\'>' + qty + '</td><td align=\'right\'>' + price + '</td></tr>';
                        $("#ordertable").find('tbody').after(rowhtml);
                    }
                }

            });
    }
    setTimeout(updateOrders, refreshOrders);
}

function updateStatus(pageName) {
    $.ajax({
            type: "GET",
            dataType: "json",
            url: baseurl + "/opstatus"
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            faildetails = "jqXHR:" + JSON.stringify(jqXHR) + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
            handleFailServer("Error while loading updateStatus() : /opstatus", faildetails);
        })
        .done(function(data) {
            //console.log("status data: " + JSON.parse(JSON.stringify(data)));
            //console.log("status : " + data["status"]);


            //we polled the server and now set the client status
            //we set the state depending on the status with the setState functions
            var newmode = data["status"];

            //console.log("newmode " + newmode + " oldmode " + mode);

            //Perform page-specific operations
            if (pageName == "dashboard") {
                $('#market').html("<code>" + data["market"] + "</code>");

                $('#botstatus').html("<code>" + data["status"] + "</code>");

                if (data["sessionstart"])
                    $('#sessionstart').html("<code>" + data["sessionstart"] + "</code>");
                else
                    $('#sessionstart').html("");


                if (data["duration"])
                    $('#duration').html("<code>" + data["duration"] + "</code>");
                else
                    $('#duration').html("");

                if (data["sessionid"])
                    $('#sessionid').html("<code>" + data["sessionid"] + "</code>");
                else
                    $('#sessionid').html("");

                if (newmode == "running") {
                    setStateRunning();
                }

                if (newmode == "starting" && mode != "starting") {
                    setStateStarting();
                }

                if (newmode == "halting" && mode != "halting") {
                    setStateHalting();
                }

                if (newmode == "halted" && mode != "halted") {
                    setStateHalted();
                }

            } else if (pageName == "config") {
                var disableButtons = newmode != "halted"; //allow changes to config only with bot halted
                updateConfigElements(disableButtons);

            }

            mode = newmode;

        });

    setTimeout(updateStatus, refreshStatusInterval, "dashboard");
}

function autoScroll() {
    //autoscrolling. not used - how to make log readable also?
    var psconsole = $('#logarea');
    if (psconsole.length)
        psconsole.scrollTop(psconsole[0].scrollHeight - psconsole.height());

}

function updateLog() {

    var url = baseurl + "/logdump";

    //console.log("url  " + url);
    $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("error loading log");
            faildetails = "jqXHR:" + JSON.stringify(jqXHR) + "<br>" + "textStatus:" + textStatus + "<br>" + "errorThrown:" + errorThrown;
            handleFailServer("Error while loading log : /logdump ", faildetails);
        })
        .done(function(data) {
            $('#logarea').val(data["log"]);
        });

    logLine++;


    if ($('#autoscrollCheckbox').prop('checked'))
        autoScroll();

    setTimeout(updateLog, refreshLogInterval);

}

function loadconfig() {

    var url = baseurl + "/config";

    $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
        })
        .done(function(data) {
            //console.log(JSON.stringify(data));
            $("#exchangename").attr("value", data["exchangeName"]);
            $("#apikey").attr("value", data["apiKey"]);
            $("#apisecret").attr("value", data["apiSecret"]);
            $("#txfee").attr("value", data["txFee"]);
            $("#pair").attr("value", data["pair"]);
            $("#dualside").attr("checked", data["dualSide"]);
            $("#multipleoperators").attr("checked", data["multipleOperators"]);
            $("#executeorders").attr("checked", data["executeOrders"]);
            $("#verbosity").attr("value", data["verbosity"]);
            $("#gitter").attr("checked", data["gitter"]);
            $("#bypassStreaming").attr("checked", data["bypassStreaming"]);
            $("#streamingserver").attr("value", data["streamingserver"]);
            $("#mailnotifications").attr("value", data["mailnotifications"]);
            $("#mailrecipient").attr("value", data["mailRecipient"]);
            $("#emergencytimeout").attr("value", data["emergencyTimeout"]);
            $("#keepproceeds").attr("value", data["keepProceeds"]);

            $("#booksellwall").attr("value", data["bookSellwall"]);
            $("#bookbuywall").attr("value", data["bookBuywall"]);

            $("#bookbuytype").attr("value", data["bookBuyType"]);
            $("#bookselltype").attr("value", data["bookSellType"]);
            $("#bookbuysteepness").attr("value", data["bookBuySteepness"]);
            $("#booksellsteepness").attr("value", data["bookSellSteepness"]);
            $("#bookbuyinterval").attr("value", data["bookBuyInterval"]);
            $("#booksellinterval").attr("value", data["bookSellInterval"]);
            $("#bookbuyoffset").attr("value", data["bookBuyOffset"]);
            $("#bookselloffset").attr("value", data["bookSellOffset"]);

            $("#booksellmaxvolumecumulative").attr("value", data["bookSellMaxVolumeCumulative"]);
            $("#bookbuymaxvolumecumulative").attr("value", data["bookBuyMaxVolumeCumulative"]);


            $("#bookDisabletier2").attr("checked", data["bookDisabletier2"]);
            $("#priceincrement").attr("value", data["priceIncrement"]);
            $("#submitliquidity").attr("checked", data["submitLiquidity"]);
            $("#nubitaddress").attr("value", data["nubitAddress"]);
            $("#nudip").attr("value", data["nudIp"]);
            $("#nudport").attr("value", data["nudPort"]);
            $("#rpcpass").attr("value", data["rpcPass"]);
            $("#rpcuser").attr("value", data["rpcUser"]);
            $("#wallchangethreshold").attr("value", data["wallchangeThreshold"]);
            $("#mainfeed").attr("value", data["mainFeed"]);
            $("#backupfeeds").attr("value", data["backupFeeds"]);
            $("#webport").attr("value", data["webport"]);

            $("#poolModeActive").attr("checked", data["poolModeActive"]);
            $("#poolURI").attr("value", data["poolURI"]);
            $("#poolPayoutAddress").attr("value", data["poolPayoutAddress"]);
            $("#poolSubmitInterval").attr("value", data["poolSubmitInterval"]);

        });
}

function getJsonOptions() {
    var exchangename = $("#exchangename").attr("value");
    var apikey = $("#apikey").attr("value");
    var apisecret = $("#apisecret").attr("value");
    var txfee = $("#txfee").attr("value");
    var pair = $("#pair").attr("value");
    var dualside = $("#dualside").prop("checked");
    var multipleoperators = $("#multipleoperators").prop("checked");
    var bypassStreaming = $("#bypassStreaming").prop("checked");
    var streamingserver = $("#streamingserver").attr("value");
    var executeorders = $("#executeorders").prop("checked");
    var verbosity = $("#verbosity").attr("value");
    var gitter = $("#gitter").prop("checked");
    var mailnotifications = $("#mailnotifications").attr("value");
    var mailrecipient = $("#mailrecipient").attr("value");
    var emergencytimeout = $("#emergencytimeout").attr("value");
    var keepproceeds = $("#keepproceeds").attr("value");
    var booksellwall = $("#booksellwall").attr("value");
    var bookbuywall = $("#bookbuywall").attr("value");
    var bookbuytype = $("#bookbuytype").attr("value");
    var bookbuysteepness = $("#bookbuysteepness").attr("value");
    var bookbuyoffset = $("#bookbuyoffset").attr("value");
    var bookbuyinterval = $("#bookbuyinterval").attr("value");
    var bookbuymaxvolumecumulative = $("#bookbuymaxvolumecumulative").attr("value");
    var bookselltype = $("#bookselltype").attr("value");
    var booksellsteepness = $("#booksellsteepness").attr("value");
    var bookselloffset = $("#bookselloffset").attr("value");
    var booksellinterval = $("#booksellinterval").attr("value");
    var booksellmaxvolumecumulative = $("#booksellmaxvolumecumulative").attr("value");


    var bookdisabletier2 = $("#bookDisabletier2").prop("checked");

    var poolmodeactive = $("#poolModeActive").prop("checked");
    var pooluri = $("#poolURI").attr("value");
    var poolpayoutaddress = $("#poolPayoutAddress").attr("value");
    var poolsubmitinterval = $("#poolSubmitInterval").attr("value");


    var priceincrement = $("#priceincrement").attr("value");
    var submitliquidity = $("#submitliquidity").prop("checked");
    var nubitaddress = $("#nubitaddress").attr("value");
    var nudip = $("#nudip").attr("value");
    var nudport = $("#nudport").attr("value");
    var rpcpass = $("#rpcpass").attr("value");
    var rpcuser = $("#rpcuser").attr("value");
    var wallchangethreshold = $("#wallchangethreshold").attr("value");
    var webport = $("#webport").attr("value");
    var mainfeed = $("#mainfeed").attr("value");



    var backupfeedsString = $("#backupfeeds").attr("value");
    var backupfeedsarray = backupfeedsString.split(',');

    var jsondata = JSON.stringify({
        "exchangename": exchangename,
        "apikey": apikey,
        "apisecret": apisecret,
        "txfee": txfee,
        "pair": pair,
        "dualside": dualside,
        "multipleoperators": multipleoperators,
        "executeorders": executeorders,
        "verbosity": verbosity,
        "bypassStreaming": bypassStreaming,
        "streamingserver": streamingserver,
        "gitter": gitter,
        "mailrecipient": mailrecipient,
        "mailnotifications": mailnotifications,
        "emergencytimeout": emergencytimeout,
        "keepproceeds": keepproceeds,
        "booksellwall": booksellwall,
        "bookbuywall": bookbuywall,
        "bookbuytype": bookbuytype,
        "bookbuysteepness": bookbuysteepness,
        "bookbuyoffset": bookbuyoffset,
        "bookbuyinterval": bookbuyinterval,
        "bookbuymaxvolumecumulative": bookbuymaxvolumecumulative,
        "booksellmaxvolumecumulative": booksellmaxvolumecumulative,
        "bookselltype": bookselltype,
        "booksellsteepness": booksellsteepness,
        "bookselloffset": bookselloffset,
        "booksellinterval": booksellinterval,
        "bookdisabletier2": bookdisabletier2,
        "poolmodeactive": poolmodeactive,
        "pooluri": pooluri,
        "poolpayoutaddress": poolpayoutaddress,
        "poolsubmitinterval": poolsubmitinterval,
        "priceincrement": priceincrement,
        "submitliquidity": submitliquidity,
        "nubitaddress": nubitaddress,
        "nudip": nudip,
        "nudport": nudport,
        "rpcpass": rpcpass,
        "rpcuser": rpcuser,
        "wallchangethreshold": wallchangethreshold,
        "webport": webport,
        "mainfeed": mainfeed,
        "backupfeeds": backupfeedsarray

    });

    return jsondata;
}

function postall() {
    var posturl = baseurl + "/config";
    laddaSavecfgBtn.ladda('start');
    //get all vars
    var jsondata = getJsonOptions();
    //could check on the client side if valid post

    makePostConfig(posturl, jsondata);
}

function makePostConfig(url, jsondata) {
    //make HTTP post to server

    $.ajax(url, {
        data: jsondata,
        dataType: "json",
        type: 'POST',
        success: function(data) {

            //console.log("got data " + data);

            // server returns custom error message
            var success = data["success"];
            //console.log("success " + success);
            var errormsg = data["error"];
            //console.log("error " + errormsg);

            if (success) {
                //stop loading after 1000 msec
                setTimeout(function() {
                    laddaSavecfgBtn.ladda('stop');
                }, 1000);
            } else {
                alert(errormsg);
                //Stop loading right away
                laddaSavecfgBtn.ladda('stop');
            }
        },

        error: function(xhr, textStatus, errorThrown) {
            alert("makePostConfig - error posting to server " + textStatus + " " + errorThrown);
            alert(xhr.responseText);
        }
    });
}

function postreset() {
    if (confirm("The bot will use a clean config saved in config/nubot-config.json. Are you sure?")) {
        laddaClearcfgBtn.ladda('start');
        var posturl = baseurl + "/configreset";

        $.ajax(posturl, {

            contentType: 'application/json',
            type: 'POST',
            success: function(data) {

                //on success of post change the color of the button
                var rbtn = $('#resetbutton');


                //stop loading after 1000 msec
                setTimeout(function() {
                    laddaClearcfgBtn.ladda('stop');
                }, 1000);


                //reset values from server
                updateConfigFile();
                loadconfig();
            },

            error: function(xhr, textStatus, errorThrown) {

                laddaClearcfgBtn.ladda('stop');

                alert("PostReset - error posting to server " + textStatus + " " + errorThrown);
                alert(xhr.responseText);
            }
        });
    }
}

function toggleChart() {
    $("#orderbook-preview-container").toggle(500);
    var $el = $("#togglepreview");
    $el.find('span').toggleClass('glyphicon-triangle-top glyphicon-triangle-bottom');
    $el.toggleClass('showChart');
    if ($el.find('span').hasClass('glyphicon-triangle-top')) {
        $('html, body').animate({
            scrollTop: $(document).height()
        }, 'slow');
    }
}

function previewOrderBook() {
    laddaPreviewBtn.ladda('start');
    var posturl = baseurl + "/previeworderbook";

    var jsondata = getJsonOptions();

    $("#orderbook-preview-container").show();
    $("#togglepreview").show();
    $("#togglepreview").find('span').removeClass('glyphicon-triangle-bottom');
    $("#togglepreview").find('span').addClass('glyphicon-triangle-top');

    loadOrderbookChart([0.9, 0.95, 1, 1.05, 1.1], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0],'NBT');
    setTimeout(function() {
        $('html, body').animate({
            scrollTop: $(document).height()
        }, 'slow');
    }, 150);
    $.ajax(posturl, {
        data: jsondata,
        dataType: "json",
        type: 'POST',
        success: function(data) {

            // server returns custom error message
            var success = data["success"];
            //console.log("success " + success);
            var errormsg = data["error"];
            //console.log("error " + errormsg);

            if (success) {

                loadOrderbookChart(data["orderbook"]["xArray"], data["orderbook"]["asks"], data["orderbook"]["bids"],data["volumeCurrency"]);
                //stop loading after 1000 msec
                setTimeout(function() {
                    laddaPreviewBtn.ladda('stop');
                }, 600);

            } else {

                $("#orderbook-preview-container").hide(50);
                alert(errormsg);
                //Stop loading right away
                laddaPreviewBtn.ladda('stop');
            }
        },

        error: function(xhr, textStatus, errorThrown) {
            alert("makePostConfig - error posting to server " + textStatus + " " + errorThrown);
            alert(xhr.responseText);
        }
    });
}


function updateIframe(pageName) {
    $('#docu-iframe').attr('src', 'docs/' + pageName + '.html');
}

function replaceZeroesWithNull(inp) {
    if (inp) {
        for (i = 0; i < inp.length; i++) {
            if (inp[i] == 0)
                inp[i] = null;
        }
        return inp;
    }
}

function isOdd(num) {
    return num % 2;
}

function loadOrderbookChart(x, asks, bids,volumeCurrency) {

    asks = replaceZeroesWithNull(asks);
    bids = replaceZeroesWithNull(bids);
    //console.log(JSON.stringify(x));

    asksName = 'Asks';
    bidsName = 'Bids';
    if(volumeCurrency!="NBT")
    {
        asksName += '[swapped]';
        bidsName += '[swapped]';

    }
    var tickPos = [0.5, 1, 1.5];

    var xBids = [
        [0.5, 0],
        [1.5, 0]
    ];
    var xAsks = [
        [0.5, 0],
        [1.5, 0]
    ];

    if (x && x.length > 4 && !isOdd(x.length)) {

        tickPos = new Array(5);
        tickPos[0] = x[0].toFixed(2);
        tickPos[1] = x[x.length / 2].toFixed(2);
        tickPos[2] = 1;
        tickPos[3] = x[(x.length / 2) + 2].toFixed(2);
        tickPos[4] = x[x.length - 1].toFixed(2);

        //console.log(JSON.stringify(tickPos));
    }



    if (asks) {
        xAsks = new Array(x.length);
        for (var i = 0; i < x.length; i++) {
            var tempArray = new Array(2);
            tempArray[0] = x[i];
            tempArray[1] = asks[i];

            xAsks[i] = tempArray;
        }
        //console.log(JSON.stringify(xAsks));

    }

    if (bids) {
        xBids = new Array(x.length);
        for (var i = 0; i < x.length; i++) {
            var tempArray = new Array(2);

            tempArray[0] = x[i];
            tempArray[1] = bids[i];

            xBids[i] = tempArray;
        }
        //console.log(JSON.stringify(xBids));

    }


    $('#orderbook-preview-div').highcharts({
        chart: {
            type: 'area',
            spacingBottom: 30
        },
        title: {
            text: 'Orderbook Preview'
        },
        subtitle: {
            text: 'Simulated view'
        },
        xAxis: {
            title: {
                text: 'Order price ($)'
            },
            tickmarkPlacement: 'on',
            tickPositions: tickPos
        },
        yAxis: {
            floor: 0,
            title: {
                text: 'Order Volume ('+volumeCurrency+')'
            },
            labels: {
                formatter: function() {
                    return this.value;
                }
            }
        },
        tooltip: {
            formatter: function() {
                var actionS = "Sell";
                if (this.series.name == "Bids") {
                    actionS = "Buy"
                }
                return '<b>' + actionS + '</b> ' +
                    this.y + ' '+volumeCurrency+' @ ' + this.x + " $";
            }
        },
        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: asksName,
            data: xAsks
        }, {
            name: bidsName,
            data: xBids
        }]
    });
}

function startupPage(page_name) {
    pageName = page_name;

    switch (page_name) {
        case "dashboard":
            updateNavbar("dashboard");
            // Create a new instance of ladda for the specified button
            laddaToggleBtn = $('#togglebot').ladda();

            updateStatus("dashboard");
            updateLog();

            updateBalances();
            updateOrders();
            break;
        case "config":
            updateNavbar("config");

            laddaSavecfgBtn = $('#saveconfigbutton').ladda();
            laddaClearcfgBtn = $('#resetbutton').ladda();
            laddaPreviewBtn = $('#previewbook').ladda();

            updateStatus("config");
            loadOrderbookChart();
            loadconfig();
            break;
        case "pool":
            updateNavbar("pool");

            refreshPoolStats();
            break;
        case "docu":
            updateNavbar("docu");
            break;
        case "disclaimer":
            updateNavbar("disclaimer");
            break;

    }
}

function changePage(page_name) {
    hook = false; //this flag will prevent the alert to show up on page change
    var newUrl = window.location.href; // default to same
    switch (page_name) {
        case "dashboard":
            newUrl = '/';
            break;
        case "config":
            newUrl = '/configui';
            break;
        case "pool":
            newUrl = '/pool';
            break;
        case "docu":
            newUrl = '/docu';
            break;
        case "disclaimer":
            newUrl = '/disclaimer';
            break;
    }
    document.location.href = baseurl + newUrl;
}

function animateProgressBar(toggle) {

    progressPB += incrementPB;
    if (progressPB >= 1) {
        stopProgressBarAnimation(toggle);
    } else {
        animatingButton = true;
        laddaToggleBtn.ladda('setProgress', progressPB);
    }
}

function stopAnimation() {
    clearInterval(currentAnimID);
    progressPB = 0;
    laddaToggleBtn.ladda('stop');
    animatingButton = false;
}

function stopProgressBarAnimation(toggle) {
    stopAnimation();
}

function updateConfigElements(running) {
    $('#saveconfigbutton').prop('disabled', running);
    $('#resetbutton').prop('disabled', running);
    $('#previewbook').prop('disabled', running);

    updatePageTitle(running);
}

function stopServer() {
    if (confirm("Are you sure you want to shutdown the server?")) {
        var url = baseurl + "/stopserver";

        //console.log("url  " + url);
        $.ajax({
                type: "GET",
                dataType: "json",
                url: url,
            })
            .fail(function() {
                hook = false;
                alert("Server stopped.");
                location.reload();

            })
            .done(function(data) {
                //It cannot succed
            });
    }
}

// Parse URL Queries Method
(function($) {
    $.getQuery = function(query) {
        query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp(expr);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
            return decodeURIComponent(results[1].replace(/\+/g, " "));
        } else {
            return false;
        }
    };
})(jQuery);


window.onbeforeunload = function(e) {
    if (isBotActive() && hook) {
        e = e || window.event;
        var confirmMessage = "The bot is still running."
            // For IE and Firefox prior to version 4
        if (e) {
            e.returnValue = confirmMessage;
        }
        // For Safari
        return confirmMessage;
    } else if (!serverDown && hook) {
        e = e || window.event;
        var confirmMessage = "Closing this window will not stop the server. Use the red button if you want to shut down server before leaving this page."
            // For IE and Firefox prior to version 4
        if (e) {
            e.returnValue = confirmMessage;
        }
        // For Safari
        return confirmMessage;
    }

};

function alertMessage(msg) {
    if (!alertShown) {
        alert(msg);
        alertShown = true;
    }
}

function replaceAll(find, replace, string) {
    return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

//from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}


/*

    1. Sample Data -------------------

    {
      "pegBalance": {
        "balanceTotal": "0.02201121",
        "balanceT1": "0",
        "balanceT2": "0",
        "currencyCode": "BTC"
      },
      "buys": 0,
      "sells": 1,
      "nbtBalance": {
        "balanceTotal": "9.53076606",
        "balanceT1": "0",
        "balanceT2": "0",
        "currencyCode": "NBT"
      },
      "orders": [
        {
          "id": "46533",
          "insertedDate": "Apr 23, 2015 12:13:37 PM",
          "type": "SELL",
          "pair": {
            "orderCurrency": {
              "fiat": false,
              "code": "NBT",
              "extendedName": "NuBit"
            },
            "paymentCurrency": {
              "fiat": false,
              "code": "BTC",
              "extendedName": "Bitcoin"
            }
          },
          "amount": {
            "quantity": 2.5,
            "currency": {
              "fiat": false,
              "code": "NBT",
              "extendedName": "NuBit"
            }
          },
          "price": {
            "quantity": 0.00439,
            "currency": {
              "fiat": false,
              "code": "BTC",
              "extendedName": "Bitcoin"
            }
          },
          "completed": true
        }
      ]
    }
 <end[1]> -------------
 */
