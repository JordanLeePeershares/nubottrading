syntax: keytool -importcert -file certificate.cer -keystore keystore.jks -alias "Alias"

Example:

keytool -importcert -file kraken.cer -keystore nubot_keystore.jks -alias “kraken-oct-2015”


certificate password : nub0tSSL


